/*
 * 文件名：Application.java
 * 版权：Copyright by layne
 * 描述：
 * 修改人：layne
 * 修改时间：2019年5月22日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
