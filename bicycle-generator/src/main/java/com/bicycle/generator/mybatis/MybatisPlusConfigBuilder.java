/*
 * 文件名：MybatisPlusConfigBuilder.java
 * 版权：Copyright by 深圳市优联物联网有限公司
 * 描述：
 * 修改人：win10
 * 修改时间：2019年5月22日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.generator.mybatis;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MybatisPlusConfigBuilder {

	/**
	 * 生成文件的输出目录【默认 D 盘根目录】
	 */
	private String outputDir = "D://";

	/**
	 * 开发人员
	 */
	private String author = "layne";

	/**
	 * 开启 swagger2 模式
	 */
	private boolean swagger2 = false;

	/**
	 * 【实体】是否为lombok模型（默认 false）<br>
	 * <a href="https://projectlombok.org/">document</a>
	 */
	private boolean entityLombokModel = false;

	/**
	 * 驱动连接的URL
	 */
	private String url;
	/**
	 * 驱动名称
	 */
	private String driverName;
	/**
	 * 数据库连接用户名
	 */
	private String username;
	/**
	 * 数据库连接密码
	 */
	private String password;

	/**
	 * 父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
	 */
	private String parent = "com.baomidou";
	/**
	 * 父包模块名
	 */
	private String moduleName = null;

	/**
	 * 需要包含的表名，允许正则表达式（与exclude二选一配置）
	 */
	private String[] include = null;
	/**
	 * 需要排除的表名，允许正则表达式
	 */
	private String[] exclude = null;

	// public String[] getInclude() {
	// 	return include;
	// }
	//
	// public void setInclude(String... include) {
	// 	this.include = include;
	// }
	//
	// public String[] getExclude() {
	// 	return exclude;
	// }
	//
	// public void setExclude(String... exclude) {
	// 	this.exclude = exclude;
	// }

}
