/*
 * 文件名：MyBatisPlusConfig.java
 * 版权：Copyright by layne
 * 描述：
 * 修改人：layne
 * 修改时间：2019年5月6日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package ${project.package.base}.${project.moduleName}.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;

@MapperScan("${project.package.base}.${project.moduleName}.mapper")
@Configuration
public class MyBatisPlusConfig {

	/**
	 * mybatis-plus分页插件
	 * 
	 * @return
	 * @see
	 */
	@Bean
	public PaginationInterceptor paginationInterceptor() {
		return new PaginationInterceptor();
	}
}
