package ${project.package.base}.${project.moduleName}.entity.base;

import java.util.Collection;
<#if project.lombok>
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
</#if>

<#if project.lombok>
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
</#if>
@SuppressWarnings("rawtypes")
public class DataGrid extends Message {

	/** 总记录数 **/
	private long totalCount;
	/** 数据集 **/
	private Collection list;
	<#if project.lombok>
	<#else>
	public DataGrid() {
		super();
	}
	
	public DataGrid(long totalCount, Collection list) {
		super();
		this.totalCount = totalCount;
		this.list = list;
	}

	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	public Collection getList() {
		return list;
	}
	public void setList(Collection list) {
		this.list = list;
	}
	</#if>
}
