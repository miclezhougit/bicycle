package ${package.Controller};

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

<#if swagger2>
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
</#if>

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.Entity}.base.DataGrid;
import ${package.Entity}.base.Message;
import ${package.Entity}.${table.entityName};
import ${package.Service}.${table.serviceName};

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if swagger2>
@Api(tags = "${entity}控制")
</#if>
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>

	private static Logger LOGGER = LoggerFactory.getLogger(${table.controllerName}.class);
	@Resource
	private ${table.serviceName} ${table.serviceName?uncap_first};

	<#if swagger2>
	@ApiOperation(value = "分页查询信息")
	</#if>
	@RequestMapping(value = "getDataGrid", method = { RequestMethod.GET })
	public Object getDataGrid(${table.entityName} ${table.entityName?uncap_first}, Page<${table.entityName}> page) {
		DataGrid dataGrid = new DataGrid();
		try {
			QueryWrapper<${table.entityName}> queryWrapper = new QueryWrapper<${table.entityName}>();
			queryWrapper.setEntity(${table.entityName?uncap_first});
			IPage<${table.entityName}> iPage = ${table.serviceName?uncap_first}.page(page, queryWrapper);
			dataGrid.setTotalCount(iPage.getTotal());
			dataGrid.setList(iPage.getRecords());
		} catch (Exception e) {
			LOGGER.error("getDataGrid method error : " + e);
			dataGrid.setCode(500);
			dataGrid.setMessage("getDataGrid error!");
		}
		return dataGrid;
	}

	<#if swagger2>
	@ApiOperation(value = "插入信息")
	</#if>
	@RequestMapping(value = "insert", method = { RequestMethod.POST })
	public Object insert(${table.entityName} ${table.entityName?uncap_first}) {
		Message message = new Message();
		try {
			${table.serviceName?uncap_first}.save(${table.entityName?uncap_first});
		} catch (Exception e) {
			LOGGER.error("insert method error :", e);
			message.setCode(500);
			message.setMessage("insert error!");
		}
		return message;
	}
	
	<#if swagger2>
	@ApiOperation(value = "删除信息")
	</#if>
	@RequestMapping(value = "delete", method = { RequestMethod.GET })
	public Object delete(${table.entityName} ${table.entityName?uncap_first}) {
		Message message = new Message();
		try {
			${table.serviceName?uncap_first}.removeById(<#list table.fields as field><#if field.keyFlag>${table.entityName?uncap_first}.get${field.propertyName?cap_first}()</#if></#list>);
		} catch (Exception e) {
			LOGGER.error("delete method error :", e);
			message.setCode(500);
			message.setMessage("delete error!");
		}
		return message;
	}

	<#if swagger2>
	@ApiOperation(value = "更新信息")
	</#if>
	@RequestMapping(value = "update", method = { RequestMethod.POST })
	public Object update(${table.entityName} ${table.entityName?uncap_first}) {
		Message message = new Message();
		try {
			${table.serviceName?uncap_first}.updateById(${table.entityName?uncap_first});
		} catch (Exception e) {
			LOGGER.error("update method error :", e);
			message.setCode(500);
			message.setMessage("update error!");
		}
		return message;
	}

	<#if swagger2>
	@ApiOperation(value = "查询信息")
	</#if>
	@RequestMapping(value = "query", method = { RequestMethod.GET })
	public Object query(${table.entityName} ${table.entityName?uncap_first}) {
		Message message = new Message();
		try {
			QueryWrapper<${table.entityName}> queryWrapper = new QueryWrapper<${table.entityName}>();
			queryWrapper.setEntity(${table.entityName?uncap_first});
			message.setResult(${table.serviceName?uncap_first}.list(queryWrapper));
		} catch (Exception e) {
			LOGGER.error("query method error :", e);
			message.setCode(500);
			message.setMessage("query error!");
		}
		return message;
	}

	<#if swagger2>
	@ApiOperation(value = "根据主键查询信息")
	</#if>
	@RequestMapping(value = "queryById", method = { RequestMethod.GET })
	public Object queryById(${table.entityName} ${table.entityName?uncap_first}) {
		Message message = new Message();
		try {
			message.setResult(${table.serviceName?uncap_first}.getById(<#list table.fields as field><#if field.keyFlag>${table.entityName?uncap_first}.get${field.propertyName?cap_first}()</#if></#list>));
		} catch (Exception e) {
			LOGGER.error("queryById method error :", e);
			message.setCode(500);
			message.setMessage("queryById error!");
		}
		return message;
	}
}
</#if>
