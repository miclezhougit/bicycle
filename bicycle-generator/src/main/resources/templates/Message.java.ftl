package ${project.package.base}.${project.moduleName}.entity.base;

<#if project.lombok>
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
</#if>
public class Message {
	/** 错误码 **/
	private int code = 200;
	/** 返回消息 **/
	private String message = "success";
	/** 返回结果 **/
	private Object result;

	<#if project.lombok>
	<#else>
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	</#if>
}