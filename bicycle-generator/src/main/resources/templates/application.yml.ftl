spring:
  profiles: 
    active: dev
  datasource:
    url: ${project.datasource.url}
    username: ${project.datasource.username}
    password: ${project.datasource.password}
    driver-class-name: ${project.datasource.driverClassName}
    type: com.alibaba.druid.pool.DruidDataSource
    initialSize: 5
    minIdle: 5
    maxActive: 20
    maxWait: 60000
    timeBetweenEvictionRunsMillis: 60000
    minEvictableIdleTimeMillis: 300000
    validationQuery: SELECT 1 FROM DUAL
    testWhileIdle: true
    testOnBorrow: false
    testOnReturn: false
    poolPreparedStatements: true
    maxPoolPreparedStatementPerConnectionSize: 20
    filters: stat,wall,log4j
    connectionProperties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000
    useGlobalDataSourceStat: true
server:
  port: ${project.port}
  servlet:
    context-path: /${project.name}
mybatis-plus:
  typeAliasesPackage: ${project.package.base}.${project.moduleName}
  mapperLocations: classpath:/mapper/**/*Mapper.xml
logging:
  level:
    ${project.package.base}.${project.moduleName}.mapper: DEBUG
    <#if project.swagger>
    io.swagger.models.parameters.AbstractSerializableParameter: ERROR
    </#if>
