package com.bicycle.platform.exception;

import com.bicycle.common.entity.base.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

/**
 * @author : layne
 * @Date: 2020/8/9
 * @Time: 16:49
 * Description:
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {


    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public Message BusinessExceptionHandler(BusinessException e, HttpServletResponse response) {
        Message message = new Message();
        message.setCode(e.getErrorCode());
        message.setMessage(e.getErrorMsg());
        log.error(e.getErrorMsg(), e);
        return message;
    }


    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Message exceptionHandler(Exception e, HttpServletResponse response) {
        Message message = new Message();
        message.setCode(500);
        message.setMessage("数据加载中，请稍后重试！");
        log.error(e.getMessage(), e);
        return message;
    }
}
