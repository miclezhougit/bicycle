/*
 * 文件名：Constants.java 版权：Copyright by 联通系统集成有限公司 描述： 修改人：焦凯旋 修改时间：2019年6月22日 跟踪单号： 修改单号： 修改内容：
 */

package com.bicycle.platform.constant;



public class Constants {

	/**
	 * 登录用户session
	 */
	public static String BICYCLE_SESSION_LOGIN_USER = "bicycle_session_login_user";
	/**
	 * 验证码session
	 */
	public static String BICYCLE_SESSION_KAPTCHA = "bicycle_session_kaptcha";
	// 文件上传路径
//	public final static String UPLOAD_PATH = Properties.getString("file.upload.path");
	// 文件下载路径
//	public final static String DOWNLOAD_PATH = Properties.getString("file.download.path");
	// 文件占位符
	public final static String FILE_PLACEHOLDER = "/";

	/**
	 * 用户与token关系结构
	 *		key		|	value
	 *		用户账号	|	token
	 *		token	|	登录用户信息
	 * 举例：
	 *		key										|	value
	 *		bicycle_account_login_user_admin			|	15636068027194221
	 *		bicycle_token_login_user_15636068027194221	|	{"account":"admin","createTime":1561095497000,"department":"市交通部门",
	 *													"email":"admin@qq.com","hasDel":0,"loginTime":1563606802672,
	 *													"mobile":"13266771234","page":0,"pageNo":0,"pageSize":0,"roleId":1,
	 *													"roleLevel":1,"roleName":"超级管理员","rows":0,"sysUserId":1,
	 *													"token":"15636068027194221","updateTime":1561095688000,
	 *													"userName":"超级管理员","userStatus":1}
	 * 
	 */
	
	/**
	 * 登录用户token
	 */
	public static String BICYCLE_TOKEN_LOGIN_USER = "bicycle_token_login_user_";
	// 请求头文件token key
	public static String BICYCLE_TOKEN_KEY = "bicycle_platform_token";
	// 登录用户
	public static String BICYCLE_ACCOUNT_LOGIN_USER = "bicycle_account_login_user_";
	/**
	 * 登录用户token session
	 */
	public static String BICYCLE_SESSION_LOGIN_TOKEN = "bicycle_session_login_token";
}
