
package com.bicycle.platform.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleCommonRegion;
import com.bicycle.service.service.IBicycleCommonFileService;
import com.bicycle.service.service.IBicycleCommonRegionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;


@Api(tags = "公共服务相关功能")
@Slf4j
@Controller
@RequestMapping("/common")
public class CommonController extends BaseController {
    @Autowired
    private IBicycleCommonRegionService commonRegionService;
    @Autowired
    private IBicycleCommonFileService fileService;


    /**
     * 获取行政区街道列表
     *
     * @param bicycleCommonRegion
     * @return
     * @see
     */
    @ApiOperation("获取行政区街道列表")
    @RequestMapping(value = "/selectRegion", method = {RequestMethod.POST})
    @ResponseBody
    public Object regionList(@RequestBody BicycleCommonRegion bicycleCommonRegion) {
        Message message = new Message();
        message.setResult(commonRegionService.list(new QueryWrapper<BicycleCommonRegion>(bicycleCommonRegion)));
        return message;
    }


    /**
     * 根据经纬度反查行政区街道信息
     *
     * @param lat 纬度
     * @param lng 经度
     * @return
     * @see
     */
    @ApiOperation("根据经纬度反查行政区街道信息")
    @RequestMapping(value = "/selectRegionByPoint", method = {RequestMethod.POST})
    @ResponseBody
    public Object selectRegionByPoint(@RequestBody JSONObject jsonObject) {
        Message message = new Message();

        String lat = jsonObject.getString("lat");
        String lng = jsonObject.getString("lng");
        message = commonRegionService.selectBicycleCommonRegionByPoint(lat, lng);

        return message;
    }


}
