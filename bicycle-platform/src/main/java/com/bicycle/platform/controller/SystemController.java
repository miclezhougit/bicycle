
package com.bicycle.platform.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bicycle.common.entity.base.DataGrid;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.*;
import com.bicycle.common.helper.*;
import com.bicycle.service.dto.*;
import com.bicycle.service.vo.BicycleSystemMenuTreeVo;
import com.bicycle.service.vo.BicycleSystemUserLoginVo;
import com.bicycle.service.vo.BicycleSystemUserVo;
import com.bicycle.platform.constant.Constants;
import com.bicycle.platform.service.BicycleSystemMenuService;
import com.bicycle.platform.service.BicycleSystemUserService;
import com.bicycle.service.entity.*;
import com.bicycle.service.service.*;
import com.google.code.kaptcha.impl.DefaultKaptcha;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.*;

/**
 *
 */
@Api(tags = "系统用户信息相关功能")
@Slf4j
@Controller
@RequestMapping("/system")
public class SystemController extends BaseController {

    @Autowired
    private BicycleSystemUserService userService;
    @Autowired
    private IBicycleSystemUserService systemUserService;
    @Autowired
    private IBicycleSystemOperationLogService systemOperationLogService;
    @Autowired
    private IBicycleSystemLoginErrorService loginErrorService;
    @Autowired
    private IBicycleSystemRoleService roleService;
    @Autowired
    private IBicycleSystemOperationLogService operationLogService;
    @Autowired
    private IBicycleSystemUserRoleService userRoleService;
    @Autowired
    private DefaultKaptcha defaultKaptcha;
    @Autowired
    private BicycleSystemMenuService menuService;
    @Autowired
    private IBicycleSequenceUserService processUserService;
    @Autowired
    private RedisStringHelper redisStringHelper;
    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;

    @Autowired
    private Environment environment;


    /**
     * 新增用户信息
     *
     * @param bicycleSystemUserDto
     * @return
     * @see
     */
    @ApiOperation("新增系统用户")
    @PostMapping(value = "/insertSysUser")
    @ResponseBody
    public Message insertSysUser(@RequestBody BicycleSystemUserDto bicycleSystemUserDto, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSystemUserDto.setCreateBy(userLoginResp.getSysUserId());
        bicycleSystemUserDto.setCreateName(userLoginResp.getUserName());
        message = userService.insertSysUser(bicycleSystemUserDto);
        return message;
    }

    /**
     * 修改用户信息
     *
     * @param bicycleSystemUserDto 用户信息
     * @param httpServletRequest   requens
     * @return
     * @see
     */
    @ApiOperation("修改系统用户")
    @PostMapping(value = "/updateSysUser")
    @ResponseBody
    public Message updateSysUser(@RequestBody BicycleSystemUserDto bicycleSystemUserDto, HttpServletRequest httpServletRequest) {
        Message message = new Message();

        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSystemUserDto.setUpdateBy(userLoginResp.getSysUserId());
        bicycleSystemUserDto.setUpdateName(userLoginResp.getUserName());
        message = userService.updateSysUser(bicycleSystemUserDto);

        if (bicycleSystemUserDto.getUserStatus() != 1 && message.getCode() == CommonStatusCodeEnum.CODE_200.getKey()) {
            redisStringHelper.del(Constants.BICYCLE_ACCOUNT_LOGIN_USER + bicycleSystemUserDto.getAccount());
            // 当前登录用户退出登录
            if (bicycleSystemUserDto.getSysUserId() == userLoginResp.getSysUserId().intValue()) {
                message.setCode(PermissionBusinessCodeEnum.CODE_INFO_LOGIN_EXPIRE_ERROR.getKey());
                message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_STOP_ERROR.getValue());
                return message;
            }
        }
        return message;
    }


    /**
     * 修改用户信息
     *
     * @param bicycleSystemUser  用户信息
     * @param httpServletRequest requens
     * @return
     * @see
     */
    @ApiOperation("删除系统用户")
    @PostMapping(value = "/deleteSysUser")
    @ResponseBody
    public Message deleteSysUser(@RequestBody BicycleSystemUser bicycleSystemUser, HttpServletRequest httpServletRequest) {
        Message message = new Message();

        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        BicycleSystemUser systemUser = systemUserService.getById(bicycleSystemUser.getSysUserId());
        systemUser.setUpdateBy(userLoginResp.getSysUserId());
        systemUser.setUpdateName(userLoginResp.getUserName());
        systemUser.setHasDel(HasDelEnum.HAS_DEL_TRUE.getKey());
        systemUserService.updateById(systemUser);

        redisStringHelper.del(Constants.BICYCLE_ACCOUNT_LOGIN_USER + systemUser.getAccount());
        // 当前登录用户退出登录
        if (systemUser.getSysUserId() == userLoginResp.getSysUserId().intValue()) {
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_LOGIN_EXPIRE_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_STOP_ERROR.getValue());
            return message;
        }
        return message;
    }

    /**
     * 修改个人用户信息
     *
     * @param bicycleSystemUser
     * @return
     * @see
     */
    @ApiOperation("修改当前用户信息")
    @PostMapping(value = "/updateLoginSysUser")
    @ResponseBody
    public Message updateLoginSysUser(@RequestBody BicycleSystemUser bicycleSystemUser, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        BicycleSystemUserDto bicycleSystemUserDto = new BicycleSystemUserDto();
        bicycleSystemUserDto.setUpdateBy(userLoginResp.getSysUserId());
        bicycleSystemUserDto.setUpdateName(userLoginResp.getUserName());
        bicycleSystemUserDto.setSysUserId(bicycleSystemUser.getSysUserId());
        bicycleSystemUserDto.setMobile(bicycleSystemUser.getMobile());
        bicycleSystemUserDto.setEmail(bicycleSystemUser.getEmail());
        message = userService.updateSysUser(bicycleSystemUserDto);
        return message;
    }

    /**
     * 分页查询用户
     *
     * @param bicycleSystemUserGridDto
     * @return
     * @see
     */
    @ApiOperation("分页查询系统用户")
    @PostMapping(value = "/selectSysUserGrid")
    @ResponseBody
    public DataGrid<BicycleSystemUserVo> selectSysUserGrid(@RequestBody BicycleSystemUserGridDto bicycleSystemUserGridDto, HttpServletRequest httpServletRequest) {
        DataGrid dataGrid = new DataGrid();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSystemUserGridDto.setRegionId(userLoginResp.getRegionId());
        bicycleSystemUserGridDto.setUserType(userLoginResp.getUserType());
        dataGrid = userService.querySystemUserByPage(bicycleSystemUserGridDto);
        return dataGrid;
    }


    /**
     * 分页查询操作日志
     *
     * @param bicycleSystemOperationLogGridDto
     * @return
     * @see
     */
    @ApiOperation("分页查询操作日志")
    @PostMapping(value = "/selectLogGrid")
    @ResponseBody
    public DataGrid<BicycleSystemOperationLog> selectLogGrid(@RequestBody BicycleSystemOperationLogGridDto bicycleSystemOperationLogGridDto) {
        DataGrid dataGrid = new DataGrid();
        QueryWrapper<BicycleSystemOperationLog> queryWrapper = new QueryWrapper<BicycleSystemOperationLog>();
        BicycleSystemOperationLog qBicycleSystemOperationLog = new BicycleSystemOperationLog();
        BeanUtil.copyProperties(bicycleSystemOperationLogGridDto, qBicycleSystemOperationLog);
        if (StringUtils.isNotBlank(bicycleSystemOperationLogGridDto.getAccount())) {
            queryWrapper.like("account", bicycleSystemOperationLogGridDto.getAccount());
            qBicycleSystemOperationLog.setAccount(null);
        }
        IPage<BicycleSystemOperationLog> systemOperationLogIPage = systemOperationLogService.page(new Page<BicycleSystemOperationLog>(bicycleSystemOperationLogGridDto.getPage(), bicycleSystemOperationLogGridDto.getRows()), queryWrapper);
        dataGrid.setTotal(systemOperationLogIPage.getTotal());
        dataGrid.setRows(systemOperationLogIPage.getRecords());
        return dataGrid;
    }

    /**
     * 系统用户登录
     *
     * @param bicycleSystemUserLoginDto
     * @param request
     * @return
     * @see
     */
    @ApiOperation("系统用户登陆")
    @PostMapping(value = "/login")
    @ResponseBody
    public Message<BicycleSystemUserLoginVo> login(@RequestBody BicycleSystemUserLoginDto bicycleSystemUserLoginDto, HttpServletRequest request) throws Exception {
        Message message = new Message();

        // api 验证开关
        int apiValidateSwitch = StringUtils.isNoneBlank(environment.getProperty("api.validate.switch")) ? Integer.parseInt(environment.getProperty("api.validate.switch")) : 1;
        if (apiValidateSwitch == 1) {
            StringBuffer buffer = new StringBuffer();
            InputStream inputStream = request.getInputStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
            String line;
            while ((line = in.readLine()) != null)
                buffer.append(line + "\n");
            Map<String, Object> parameters = new HashMap<String, Object>();
            JSONObject jsonObject = JSON.parseObject(buffer.toString());
            for (String key : jsonObject.keySet()) {
                parameters.put(key, jsonObject.getString(key));
            }
            // 校验签名
            if (!SignatureHelper.isSignatureValid(parameters, environment.getProperty("api.secret"))) {
                message.setCode(CommonBusinessCodeEnum.CODE_SIGN_ERROR.getKey());
                message.setMessage(CommonBusinessCodeEnum.CODE_SIGN_ERROR.getValue());
                return message;
            }
        }

        String sessionCode = (String) request.getSession().getAttribute(Constants.BICYCLE_SESSION_KAPTCHA);
        String code = bicycleSystemUserLoginDto.getCode();
        // 判断账户密码错误次数是否达到当日上限
        BicycleSystemLoginError qBicycleSystemLoginError = new BicycleSystemLoginError();
        qBicycleSystemLoginError.setAccount(bicycleSystemUserLoginDto.getAccount());
        qBicycleSystemLoginError.setLoginDate(new Date());
        BicycleSystemLoginError bicycleSystemLoginError = loginErrorService.getOne(new QueryWrapper<BicycleSystemLoginError>(qBicycleSystemLoginError));
        if (bicycleSystemLoginError != null && bicycleSystemLoginError.getErrorNum() >= 5) {
            // 当日输入密码错误次数已到上限
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_PASSWORD_NUM_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_PASSWORD_NUM_ERROR.getValue());
            return message;
        }
        if (StringUtils.isBlank(code)) {
            // 验证码错误
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_KAPTCHA_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_KAPTCHA_ERROR.getValue());
            return message;
        }
        String defaultCode = StringUtils.isBlank(environment.getProperty("platform.login.kaptcha")) ? "djzx" : environment.getProperty("platform.login.kaptcha");
        if (!StringUtils.equalsIgnoreCase(defaultCode, code) && !StringUtils.equalsIgnoreCase(sessionCode, code)) {
            // 验证码错误
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_KAPTCHA_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_KAPTCHA_ERROR.getValue());
            return message;
        }

        BicycleSystemUser systemUser = systemUserService.getOne(new QueryWrapper<BicycleSystemUser>(BicycleSystemUser.builder()
                .account(bicycleSystemUserLoginDto.getAccount())
                .hasDel(HasDelEnum.HAS_DEL_FALSE.getKey())
                .build()));
        // 账号不存在
        if (systemUser == null) {
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_NONEXISTS.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_NONEXISTS.getValue());
            return message;
        }
        // 密码错误
        if (!StringUtils.equals(systemUser.getPassword(), bicycleSystemUserLoginDto.getPassword())) {
            if (bicycleSystemLoginError == null) {
                bicycleSystemLoginError = new BicycleSystemLoginError();
                bicycleSystemLoginError.setAccount(bicycleSystemUserLoginDto.getAccount());
                bicycleSystemLoginError.setLoginIp(IPAddressHelper.getIpAddress(request));
                bicycleSystemLoginError.setErrorNum(1);
                bicycleSystemLoginError.setLoginDate(new Date());
                loginErrorService.save(bicycleSystemLoginError);
            } else {
                bicycleSystemLoginError.setUpdateTime(new Date());
                bicycleSystemLoginError.setLoginIp(IPAddressHelper.getIpAddress(request));
                bicycleSystemLoginError.setErrorNum(bicycleSystemLoginError.getErrorNum() + 1);
                loginErrorService.updateById(bicycleSystemLoginError);
            }
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_PASSWORD_ERROR.getKey());
            message.setMessage(MessageFormat.format(PermissionBusinessCodeEnum.CODE_INFO_PASSWORD_ERROR.getValue(), 5 - bicycleSystemLoginError.getErrorNum()));
            return message;
        }
        if (systemUser.getUserStatus() == SysUserStatusEnum.USER_STATUS_ABOLISH.getKey().intValue()) {
            // 账号被废除
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_STATUS_ABOLISH.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_STATUS_ABOLISH.getValue());
            return message;
        }
        if (systemUser.getUserStatus() != SysUserStatusEnum.USER_STATUS_NORMAL.getKey().intValue()) {
            // 账号状态不可用
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_STATUS_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_STATUS_ERROR.getValue());
            return message;
        }
        BicycleSystemUserRole bicycleSystemUserRole = userRoleService.getOne(new QueryWrapper<BicycleSystemUserRole>(BicycleSystemUserRole.builder().sysUserId(systemUser.getSysUserId()).build()));
        if (bicycleSystemUserRole == null) {
            // 账号未分配角色
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_ROLE_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_ROLE_ERROR.getValue());
            return message;
        }
        BicycleSystemRole bicycleSystemRole = roleService.getById(bicycleSystemUserRole.getRoleId());
        if (bicycleSystemRole.getRoleStatus() != RoleStatusEnum.ROLE_STATUS_NORMAL.getKey().intValue()) {
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_ROLE_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_ACCOUNT_ROLE_ERROR.getValue());
            return message;
        }
        systemUser.setLoginTime(new Date());
        systemUserService.updateById(systemUser);


        BicycleSystemUserLoginVo bicycleSystemUserLoginVo = new BicycleSystemUserLoginVo();
        BeanUtil.copyProperties(bicycleSystemRole, bicycleSystemUserLoginVo);
        BeanUtil.copyProperties(systemUser, bicycleSystemUserLoginVo);
        bicycleSystemUserLoginVo.setPassword(null);

        // 用户登录最大控制值
        int maxLoginUser = StringUtils.isNotEmpty(environment.getProperty("platform.login.maxTotal")) ? Integer.parseInt(environment.getProperty("platform.login.maxTotal")) : 600;

        Set<String> keys = redisStringHelper.keys(Constants.BICYCLE_ACCOUNT_LOGIN_USER + "*");
        if (keys != null && keys.size() >= maxLoginUser) {
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_LOGIN_COUNT_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_LOGIN_COUNT_ERROR.getValue());
            return message;
        }

        // session失效时间
        int tokenExpires = StringUtils.isNotEmpty(environment.getProperty("session.expires.time")) ? Integer.parseInt(environment.getProperty("session.expires.time")) : 1800;

        String token = GenratorNoHelper.generateTokenNo();
        bicycleSystemUserLoginVo.setToken(token);
        // 存取token信息
        redisStringHelper.set(Constants.BICYCLE_TOKEN_LOGIN_USER + token, JSON.toJSONString(bicycleSystemUserLoginVo));
        redisStringHelper.expire(Constants.BICYCLE_TOKEN_LOGIN_USER + token, tokenExpires);

        // 将登录用户绑定token
        redisStringHelper.set(Constants.BICYCLE_ACCOUNT_LOGIN_USER + bicycleSystemUserLoginDto.getAccount(), token);
        redisStringHelper.expire(Constants.BICYCLE_ACCOUNT_LOGIN_USER + bicycleSystemUserLoginDto.getAccount(), tokenExpires);

        request.getSession().setAttribute(Constants.BICYCLE_SESSION_LOGIN_TOKEN, token);
        request.getSession().setMaxInactiveInterval(-1);

        BicycleSystemOperationLog bicycleSystemOperationLog = new BicycleSystemOperationLog();
        bicycleSystemOperationLog.setAccount(bicycleSystemUserLoginVo.getAccount());
        bicycleSystemOperationLog.setSysUserId(bicycleSystemUserLoginVo.getSysUserId());
        bicycleSystemOperationLog.setUrl(request.getServletPath());
        bicycleSystemOperationLog.setIp(IPAddressHelper.getIpAddress(request));
        bicycleSystemOperationLog.setMenu("登录");
        bicycleSystemOperationLog.setCreateTime(new Date());
        bicycleSystemOperationLog.setOperateDesc("登录");
        bicycleSystemOperationLog.setDataStruts(JSON.toJSONString(bicycleSystemUserLoginVo));
        operationLogService.save(bicycleSystemOperationLog);

        // 登录成功 清楚错误次数
        if (bicycleSystemLoginError != null) {
            bicycleSystemLoginError.setErrorNum(0);
            bicycleSystemLoginError.setUpdateTime(new Date());
            loginErrorService.updateById(bicycleSystemLoginError);
        }
        message.setResult(bicycleSystemUserLoginVo);

        return message;
    }

    /**
     * 退出登陆
     *
     * @param request
     * @return
     * @see
     */
    @ApiOperation("注销系统用户")
    @PostMapping(value = "/logout")
    @ResponseBody
    public Object logout(HttpServletRequest request) {
        Message message = new Message();
        // 退出登录，清楚当前用户以及token
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(request);
        redisStringHelper.del(Constants.BICYCLE_TOKEN_LOGIN_USER + userLoginResp.getToken());
        redisStringHelper.del(Constants.BICYCLE_ACCOUNT_LOGIN_USER + userLoginResp.getAccount());

        BicycleSystemOperationLog bicycleSystemOperationLog = new BicycleSystemOperationLog();
        bicycleSystemOperationLog.setAccount(userLoginResp.getAccount());
        bicycleSystemOperationLog.setSysUserId(userLoginResp.getSysUserId());
        bicycleSystemOperationLog.setUrl(request.getServletPath());
        bicycleSystemOperationLog.setIp(IPAddressHelper.getIpAddress(request));
        bicycleSystemOperationLog.setMenu("注销");
        bicycleSystemOperationLog.setCreateTime(new Date());
        bicycleSystemOperationLog.setOperateDesc("注销");
        bicycleSystemOperationLog.setDataStruts(null);
        operationLogService.save(bicycleSystemOperationLog);
        request.getSession().invalidate();
        return message;
    }

    /**
     * 获取验证码
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @throws Exception
     * @see
     */
    @ApiOperation("获取登陆验证码")
    @GetMapping(value = "/code")
    public void code(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        byte[] captchaChallengeAsJpeg = null;
        ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
        try {
            // 生产验证码字符串并保存到session中
            String createText = defaultKaptcha.createText();
            httpServletRequest.getSession().setAttribute(Constants.BICYCLE_SESSION_KAPTCHA, createText);
            // 使用生产的验证码字符串返回一个BufferedImage对象并转为byte写入到byte数组中
            BufferedImage challenge = defaultKaptcha.createImage(createText);
            ImageIO.write(challenge, "jpg", jpegOutputStream);
        } catch (IllegalArgumentException e) {
            httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        // 定义response输出类型为image/jpeg类型，使用response输出流输出图片的byte数组
        captchaChallengeAsJpeg = jpegOutputStream.toByteArray();
        httpServletResponse.setHeader("Cache-Control", "no-store");
        httpServletResponse.setHeader("Pragma", "no-cache");
        httpServletResponse.setDateHeader("Expires", 0);
        httpServletResponse.setContentType("image/jpeg");
        ServletOutputStream responseOutputStream = httpServletResponse.getOutputStream();
        responseOutputStream.write(captchaChallengeAsJpeg);
        responseOutputStream.flush();
        responseOutputStream.close();
    }

    /**
     * 根据用户主键获取菜单树
     *
     * @param bicycleSystemUser
     * @return
     * @see
     */
    @ApiOperation("根据用户主键获取菜单树")
    @PostMapping(value = "/selectUserMenu")
    @ResponseBody
    public Message<List<BicycleSystemMenuTreeVo>> selectUserMenu(@RequestBody BicycleSystemUser bicycleSystemUser) {
        Message message = new Message();

        BicycleSystemUserRole qBicycleSystemUserRole = new BicycleSystemUserRole();
        qBicycleSystemUserRole.setSysUserId(bicycleSystemUser.getSysUserId());
        BicycleSystemUserRole bicycleSystemUserRole = userRoleService.getOne(new QueryWrapper<BicycleSystemUserRole>(qBicycleSystemUserRole));
        List<BicycleSystemMenuTreeVo> menuTreeResps = menuService.selectSystemMenuTreeByRole(bicycleSystemUserRole.getRoleId());
        message.setResult(menuTreeResps);

        return message;
    }

    /**
     * 插入移动端企业用户
     *
     * @param bicycleSequenceUser
     * @param httpServletRequest
     * @return
     * @see
     */
    @ApiOperation("新增移动端企业用户")
    @PostMapping(value = "/insertEnterpriseUser")
    @ResponseBody
    public Message insertEnterpriseUser(@RequestBody BicycleSequenceUser bicycleSequenceUser, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSequenceUser.setCreateBy(userLoginResp.getSysUserId());
        bicycleSequenceUser.setCreateName(userLoginResp.getUserName());
        bicycleSequenceUser.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        bicycleSequenceUser.setUserType(1);
        message = processUserService.insertSequenceProcessUser(bicycleSequenceUser);
        return message;
    }

    /**
     * 修改移动端企业用户信息
     *
     * @param bicycleSequenceUser
     * @param httpServletRequest
     * @return
     * @see
     */
    @ApiOperation("修改移动端企业用户")
    @PostMapping(value = "/updateEnterpriseUser")
    @ResponseBody
    public Message updateEnterpriseUser(@RequestBody BicycleSequenceUser bicycleSequenceUser, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSequenceUser.setUpdateBy(userLoginResp.getSysUserId());
        bicycleSequenceUser.setUpdateName(userLoginResp.getUserName());
        message = processUserService.updateSequenceProcessUser(bicycleSequenceUser);
        return message;
    }


    @ApiOperation("删除移动端企业用户")
    @PostMapping(value = "/deleteEnterpriseUser")
    @ResponseBody
    public Object deleteEnterpriseUser(@RequestBody BicycleSequenceUser bicycleSequenceUser, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSequenceUser sequenceUser = processUserService.getById(bicycleSequenceUser.getUserId());
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        sequenceUser.setUpdateBy(userLoginResp.getSysUserId());
        sequenceUser.setUpdateName(userLoginResp.getUserName());
        sequenceUser.setHasDel(HasDelEnum.HAS_DEL_TRUE.getKey());
        processUserService.updateById(sequenceUser);
        return message;
    }

    /**
     * 分页查询移动端用户
     *
     * @param bicycleSequenceUser
     * @param httpServletRequest
     * @return
     * @see
     */
    @ApiOperation("分页查询移动端企业用户")
    @PostMapping(value = "/selectEnterpriseUserGrid")
    @ResponseBody
    public Object selectEnterpriseUserGrid(@RequestBody BicycleSequenceUser bicycleSequenceUser, HttpServletRequest httpServletRequest) {
        DataGrid dataGrid = new DataGrid();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSequenceUser.setDistrictId(userLoginResp.getRegionId());
        bicycleSequenceUser.setUserType(1);
        IPage<BicycleSequenceUser> bicycleSequenceUserIPage = processUserService.page(new Page<BicycleSequenceUser>(bicycleSequenceUser.getPage(), bicycleSequenceUser.getRows()), new QueryWrapper<BicycleSequenceUser>(bicycleSequenceUser));
        dataGrid.setTotal(bicycleSequenceUserIPage.getTotal());
        dataGrid.setRows(bicycleSequenceUserIPage.getRecords());
        return dataGrid;
    }

    /**
     * 插入移动端政府用户
     *
     * @param bicycleSequenceUser
     * @param httpServletRequest
     * @return
     * @see
     */
    @ApiOperation("插入移动端政府用户")
    @PostMapping(value = "/insertGovUser")
    @ResponseBody
    public Object insertGovUser(@RequestBody BicycleSequenceUser bicycleSequenceUser, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSequenceUser.setCreateBy(userLoginResp.getSysUserId());
        bicycleSequenceUser.setCreateName(userLoginResp.getUserName());
        bicycleSequenceUser.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        bicycleSequenceUser.setUserType(2);
        message = processUserService.insertSequenceProcessUser(bicycleSequenceUser);
        return message;
    }

    /**
     * 修改移动端政府用户
     *
     * @param bicycleSequenceUser
     * @param httpServletRequest
     * @return
     * @see
     */
    @ApiOperation("修改移动端政府用户")
    @PostMapping(value = "/updateGovUser")
    @ResponseBody
    public Object updateGovUser(@RequestBody BicycleSequenceUser bicycleSequenceUser, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSequenceUser.setUpdateBy(userLoginResp.getSysUserId());
        bicycleSequenceUser.setUpdateName(userLoginResp.getUserName());
        message = processUserService.updateSequenceProcessUser(bicycleSequenceUser);
        return message;
    }

    @ApiOperation("删除移动端政府用户")
    @PostMapping(value = "/deleteGovUser")
    @ResponseBody
    public Object deleteGovUser(@RequestBody BicycleSequenceUser bicycleSequenceUser, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSequenceUser sequenceUser = processUserService.getById(bicycleSequenceUser.getUserId());
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        sequenceUser.setUpdateBy(userLoginResp.getSysUserId());
        sequenceUser.setUpdateName(userLoginResp.getUserName());
        sequenceUser.setHasDel(HasDelEnum.HAS_DEL_TRUE.getKey());
        processUserService.updateById(sequenceUser);
        return message;
    }

    /**
     * 分页查询移动端政府用户
     *
     * @param bicycleSequenceUser
     * @param httpServletRequest
     * @return
     * @see
     */
    @ApiOperation("分页查询移动端政府用户")
    @PostMapping(value = "/selectGovUserGrid")
    @ResponseBody
    public DataGrid<BicycleSequenceUser> selectGovUserGrid(@RequestBody BicycleSequenceUser bicycleSequenceUser, HttpServletRequest httpServletRequest) {
        DataGrid dataGrid = new DataGrid();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSequenceUser.setDistrictId(userLoginResp.getRegionId());
        bicycleSequenceUser.setUserType(2);
        IPage<BicycleSequenceUser> bicycleSequenceUserIPage = processUserService.page(new Page<BicycleSequenceUser>(bicycleSequenceUser.getPage(), bicycleSequenceUser.getRows()), new QueryWrapper<BicycleSequenceUser>(bicycleSequenceUser));
        dataGrid.setTotal(bicycleSequenceUserIPage.getTotal());
        dataGrid.setRows(bicycleSequenceUserIPage.getRecords());
        return dataGrid;
    }

    /**
     * 修改系统用户密码
     *
     * @param bicycleSystemUserPasswordDto
     * @param httpServletRequest
     * @return
     */
    @ApiOperation("修改系统用户密码")
    @PostMapping(value = "/updateSysUserPassword")
    @ResponseBody
    public Object updateSysUserPassword(@RequestBody BicycleSystemUserPasswordDto bicycleSystemUserPasswordDto, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSystemUserPasswordDto.setUpdateBy(userLoginResp.getSysUserId());
        bicycleSystemUserPasswordDto.setUpdateName(userLoginResp.getUserName());
        message = userService.updateSysUserPassword(bicycleSystemUserPasswordDto);
        return message;
    }

    @ApiOperation("获取系统接口列表")
    @RequestMapping(value = "/requestUrl", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Object requestUrl(@RequestBody BicycleSystemUser bicycleSystemUser, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        List<String> mappingList = new ArrayList<>();
        Map<RequestMappingInfo, HandlerMethod> handlerMethods = requestMappingHandlerMapping.getHandlerMethods();
        handlerMethods.forEach((mappingInfo, method) -> {
            log.error("mappingInfo:{},method{}", mappingInfo.getPatternsCondition(), method.getMethod().getName());
            String mapping = String.valueOf(mappingInfo.getPatternsCondition());
            mappingList.add(mapping.substring(1, mapping.length() - 1));
        });
        message.setResult(mappingList);

        return message;
    }

    /**
     * 获取菜单树
     *
     * @param httpServletRequest
     * @return
     * @see
     */
    @ApiOperation("获取菜单树")
    @PostMapping(value = "/selectSystemMenuTree")
    @ResponseBody
    public Message<List<BicycleSystemMenuTreeVo>> selectSystemMenuTree(HttpServletRequest httpServletRequest) {
        Message message = new Message();
        List<BicycleSystemMenuTreeVo> menuTreeResps = menuService.selectSystemMenuTree();
        message.setResult(menuTreeResps);
        return message;
    }

}
