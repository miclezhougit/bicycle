package com.bicycle.platform.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bicycle.common.entity.base.DataGrid;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.CommonStatusCodeEnum;
import com.bicycle.common.enums.HasDelEnum;
import com.bicycle.common.enums.RoleStatusEnum;
import com.bicycle.service.dto.BicycleSystemRoleMenuDto;
import com.bicycle.service.entity.BicycleSystemRoleMenu;
import com.bicycle.service.service.IBicycleSystemRoleMenuService;
import com.bicycle.service.service.IBicycleSystemRoleService;
import com.bicycle.service.vo.BicycleSystemUserLoginVo;
import com.bicycle.platform.service.BicycleSystemRoleService;
import com.bicycle.service.entity.BicycleSystemRole;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/10
 * @Time: 21:59
 * Description:
 */
@Api(tags = "系统角色信息相关功能")
@Slf4j
@RestController
@RequestMapping("/system")
public class RoleController extends BaseController {

    @Autowired
    private BicycleSystemRoleService systemRoleService;

    @Autowired
    private IBicycleSystemRoleService roleService;
    @Autowired
    private IBicycleSystemRoleMenuService roleMenuService;


    /**
     * 查询角色信息列表
     *
     * @param bicycleSystemRole
     * @return
     * @see
     */
    @ApiOperation("查询角色信息列表")
    @PostMapping(value = "/selectSysRoleList")
    @ResponseBody
    public Message<List<BicycleSystemRole>> selectSysRoleList(@RequestBody BicycleSystemRole bicycleSystemRole, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleSystemRole.setRoleType(userLoginResp.getRoleType());
        bicycleSystemRole.setRegionId(userLoginResp.getRegionId());
        bicycleSystemRole.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        message = systemRoleService.selectSystemRoleList(bicycleSystemRole);
        return message;
    }


    /**
     * 查询角色列表
     *
     * @param systemRole
     * @return
     * @see
     */
    @ApiOperation("分页查询角色列表")
    @RequestMapping(value = "/selectRoleGrid", method = {RequestMethod.POST})
    @ResponseBody
    public DataGrid<BicycleSystemRole> selectRoleGrid(HttpServletRequest httpServletRequest, @RequestBody BicycleSystemRole systemRole) {
        DataGrid dataGrid = new DataGrid();
        BicycleSystemRole qSystemRole = BicycleSystemRole.builder().hasDel(HasDelEnum.HAS_DEL_FALSE.getKey()).roleName(systemRole.getRoleName()).agencyId(systemRole.getAgencyId())
                .roleStatus(systemRole.getRoleStatus()).roleType(systemRole.getRoleType()).build();
        qSystemRole.setPage(systemRole.getPage());
        qSystemRole.setRows(systemRole.getRows());
        IPage<BicycleSystemRole> page = roleService.page(new Page<BicycleSystemRole>(systemRole.getPage(), systemRole.getRows()), new QueryWrapper<BicycleSystemRole>(qSystemRole));
        dataGrid.setTotal(page.getTotal());
        dataGrid.setRows(page.getRecords());
        return dataGrid;
    }

    /**
     * 禁用指定角色
     *
     * @param bicycleSystemRole
     * @return
     * @see
     */
    @ApiOperation("禁用指定角色")
    @RequestMapping(value = "/disableRoleByRoleId", method = {RequestMethod.POST})
    @ResponseBody
    public Message disableRoleByRoleId(HttpServletRequest request, @RequestBody BicycleSystemRole bicycleSystemRole) {
        Message message = new Message();
        BicycleSystemUserLoginVo loginResp = super.loginUserInfo(request);
        BicycleSystemRole systemRole = roleService.getById(bicycleSystemRole.getRoleId());
        systemRole.setUpdateBy(loginResp.getSysUserId());
        systemRole.setUpdateName(loginResp.getUserName());
        systemRole.setUpdateTime(new Date());
        systemRole.setRoleStatus(RoleStatusEnum.ROLE_STATUS_PAUSE.getKey());
        roleService.updateById(systemRole);
        return message;
    }

    /**
     * 启用指定角色
     *
     * @param bicycleSystemRole
     * @return
     * @see
     */
    @ApiOperation("启用指定角色")
    @RequestMapping(value = "/enableRoleByRoleId", method = {RequestMethod.POST})
    @ResponseBody
    public Message enableRoleByRoleId(HttpServletRequest request, @RequestBody BicycleSystemRole bicycleSystemRole) {
        Message message = new Message();

        BicycleSystemUserLoginVo loginResp = super.loginUserInfo(request);
        bicycleSystemRole.setUpdateBy(loginResp.getSysUserId());
        bicycleSystemRole.setUpdateName(loginResp.getUserName());
        bicycleSystemRole.setUpdateTime(new Date());
        bicycleSystemRole.setRoleStatus(RoleStatusEnum.ROLE_STATUS_NORMAL.getKey());
        roleService.updateById(bicycleSystemRole);

        return message;
    }

    /**
     * 删除角色
     *
     * @param bicycleSystemRole
     * @return
     * @see
     */
    @ApiOperation("删除角色")
    @RequestMapping(value = "/delRoleByRoleId", method = {RequestMethod.POST})
    @ResponseBody
    public Message delRoleByRoleId(HttpServletRequest request, @RequestBody BicycleSystemRole bicycleSystemRole) {
        Message message = new Message();

        BicycleSystemUserLoginVo loginResp = super.loginUserInfo(request);
        bicycleSystemRole.setUpdateBy(loginResp.getSysUserId());
        bicycleSystemRole.setUpdateName(loginResp.getUserName());
        bicycleSystemRole.setUpdateTime(new Date());
        bicycleSystemRole.setHasDel(HasDelEnum.HAS_DEL_TRUE.getKey());
        roleService.updateById(bicycleSystemRole);

        return message;
    }

    /**
     * 新增角色
     *
     * @param bicycleSystemRole
     * @return
     * @see
     */
    @ApiOperation("新增角色")
    @RequestMapping(value = "/insertRole", method = {RequestMethod.POST})
    @ResponseBody
    public Message insertRole(HttpServletRequest request, @RequestBody BicycleSystemRole bicycleSystemRole) {
        Message message = new Message();
        BicycleSystemUserLoginVo loginResp = super.loginUserInfo(request);
        bicycleSystemRole.setCreateBy(loginResp.getSysUserId());
        bicycleSystemRole.setCreateName(loginResp.getUserName());
        bicycleSystemRole.setCreateTime(new Date());
        bicycleSystemRole.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        roleService.save(bicycleSystemRole);
        return message;
    }

    /**
     * 新增角色对应的菜单
     *
     * @param bicycleSystemRoleMenuDto
     * @return
     * @see
     */
    @ApiOperation("新增角色对应的菜单")
    @RequestMapping(value = "/insertRoleMenu", method = {RequestMethod.POST})
    @ResponseBody
    public Message insertRoleMenu(HttpServletRequest request, @RequestBody BicycleSystemRoleMenuDto bicycleSystemRoleMenuDto) {
        Message message = new Message();
        BicycleSystemUserLoginVo loginResp = super.loginUserInfo(request);
        bicycleSystemRoleMenuDto.setCreateBy(loginResp.getSysUserId());
        bicycleSystemRoleMenuDto.setCreateName(loginResp.getUserName());
        bicycleSystemRoleMenuDto.setCreateTime(new Date());
        message = roleMenuService.insertBicycleSystemRoleMenu(bicycleSystemRoleMenuDto);
        return message;
    }

    /**
     * 根据角色ID获取角色对应的菜单
     *
     * @param request
     * @param bicycleSystemRoleMenuDto
     * @return
     * @see
     */
    @ApiOperation("根据角色ID获取角色对应的菜单")
    @RequestMapping(value = "/selectRoleMenu", method = {RequestMethod.POST})
    @ResponseBody
    public Message<List<BicycleSystemRoleMenu>> selectRoleMenu(HttpServletRequest request, @RequestBody BicycleSystemRoleMenuDto bicycleSystemRoleMenuDto) {
        Message message = new Message();

        BicycleSystemRoleMenu qRoleMenu = new BicycleSystemRoleMenu();
        qRoleMenu.setRoleId(bicycleSystemRoleMenuDto.getRoleId());
        message.setResult(roleMenuService.list(new QueryWrapper<BicycleSystemRoleMenu>(qRoleMenu)));

        return message;
    }
}
