
package com.bicycle.platform.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bicycle.common.entity.base.DataGrid;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.AreaStatusEnum;
import com.bicycle.common.enums.EnterpriseAreaTypeEnum;
import com.bicycle.common.enums.GovAreaAttributeEnum;
import com.bicycle.common.enums.HasDelEnum;
import com.bicycle.common.helper.JedisClusterHelper;
import com.bicycle.common.helper.RedisStringHelper;
import com.bicycle.service.cache.DataCacheOprator;
import com.bicycle.service.dto.BicycleFacilitiesMapDto;
import com.bicycle.service.entity.BicycleFacilitiesEnterpriseArea;
import com.bicycle.service.entity.BicycleFacilitiesEnterpriseEquipment;
import com.bicycle.service.entity.BicycleFacilitiesGovArea;
import com.bicycle.service.service.IBicycleFacilitiesEnterpriseAreaService;
import com.bicycle.service.service.IBicycleFacilitiesEnterpriseEquipmentService;
import com.bicycle.service.service.IBicycleFacilitiesGovAreaService;
import com.bicycle.service.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 基础设施相关controller
 */
@Api(tags = "基础设施相关功能")
@Slf4j
@RestController
@RequestMapping("/facilities")
public class FacilitiesController extends BaseController {

    @Autowired
    private IBicycleFacilitiesEnterpriseAreaService facilitiesEnterpriseAreaService;
    @Autowired
    private IBicycleFacilitiesEnterpriseEquipmentService facilitiesEnterpriseEquipmentService;
    //    @Autowired
//    private FacilitiesLabelService rtcFacilitiesLabelService;
    @Autowired
    private RedisStringHelper redisStringHelper;

    @Autowired
    private IBicycleFacilitiesGovAreaService facilitiesGovAreaService;


    /**
     * 基础设施管理-电子围栏-分页查询围栏信息
     *
     * @param bicycleFacilitiesEnterpriseArea
     * @param httpServletRequest
     * @return
     */
    @ApiOperation("分页查询围栏信息")
    @RequestMapping(value = "/selectFenceGrid", method = {RequestMethod.POST})
    public DataGrid<BicycleFacilitiesEnterpriseEquipmentVo> selectFenceGrid(@RequestBody BicycleFacilitiesEnterpriseArea bicycleFacilitiesEnterpriseArea, HttpServletRequest httpServletRequest) {
        DataGrid dataGrid = new DataGrid();

        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        if (userLoginResp.getRegionId() != null) {
            bicycleFacilitiesEnterpriseArea.setDistrict(userLoginResp.getRegionId());
        }
        QueryWrapper<BicycleFacilitiesEnterpriseArea> queryWrapper = new QueryWrapper<BicycleFacilitiesEnterpriseArea>();

        bicycleFacilitiesEnterpriseArea.setAreaType(1);
        bicycleFacilitiesEnterpriseArea.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        if (StringUtils.isNotBlank(bicycleFacilitiesEnterpriseArea.getAreaNo())) {
            queryWrapper.like("area_no", bicycleFacilitiesEnterpriseArea.getAreaNo());
            bicycleFacilitiesEnterpriseArea.setAreaNo(null);
        }
        if (StringUtils.isNotBlank(bicycleFacilitiesEnterpriseArea.getAreaName())) {
            queryWrapper.like("area_name", bicycleFacilitiesEnterpriseArea.getAreaName());
            bicycleFacilitiesEnterpriseArea.setAreaName(null);
        }
        IPage<BicycleFacilitiesEnterpriseArea> page = facilitiesEnterpriseAreaService.page(new Page<BicycleFacilitiesEnterpriseArea>(bicycleFacilitiesEnterpriseArea.getPage(), bicycleFacilitiesEnterpriseArea.getRows()), queryWrapper);

        List<BicycleFacilitiesEnterpriseAreaVo> bicycleFacilitiesEnterpriseAreaVoList = new ArrayList<BicycleFacilitiesEnterpriseAreaVo>();
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        page.getRecords().forEach(facilitiesEnterpriseArea -> {
            BicycleFacilitiesEnterpriseAreaVo bicycleFacilitiesEnterpriseAreaVo = new BicycleFacilitiesEnterpriseAreaVo();
            BeanUtil.copyProperties(facilitiesEnterpriseArea, bicycleFacilitiesEnterpriseAreaVo);
            bicycleFacilitiesEnterpriseAreaVo.setDistrictName(facilitiesEnterpriseArea.getDistrict() == null ? null
                    : DataCacheOprator.getInstance().getRegion(facilitiesEnterpriseArea.getDistrict()).getRegionName());
            bicycleFacilitiesEnterpriseAreaVo.setStreetName(facilitiesEnterpriseArea.getStreet() == null ? null
                    : DataCacheOprator.getInstance().getRegion(facilitiesEnterpriseArea.getStreet()).getRegionName());
            bicycleFacilitiesEnterpriseAreaVoList.add(bicycleFacilitiesEnterpriseAreaVo);
        });

        dataGrid.setTotal(page.getTotal());
        dataGrid.setRows(bicycleFacilitiesEnterpriseAreaVoList);
        return dataGrid;
    }

    /**
     * 基础设施管理-智能车桩-分页查询站点信息
     *
     * @param bicycleFacilitiesEnterpriseArea
     * @param httpServletRequest
     * @return
     */
    @ApiOperation("分页查询站点信息")
    @RequestMapping(value = "/selectParkGrid", method = {RequestMethod.POST})
    public DataGrid<BicycleFacilitiesEnterpriseEquipmentVo> selectParkGrid(@RequestBody BicycleFacilitiesEnterpriseArea bicycleFacilitiesEnterpriseArea, HttpServletRequest httpServletRequest) {
        DataGrid dataGrid = new DataGrid();

        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        if (userLoginResp.getRegionId() != null) {
            bicycleFacilitiesEnterpriseArea.setDistrict(userLoginResp.getRegionId());
        }
        bicycleFacilitiesEnterpriseArea.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        bicycleFacilitiesEnterpriseArea.setAreaType(0);
        QueryWrapper<BicycleFacilitiesEnterpriseArea> queryWrapper = new QueryWrapper<BicycleFacilitiesEnterpriseArea>();

        if (StringUtils.isNotBlank(bicycleFacilitiesEnterpriseArea.getAreaNo())) {
            queryWrapper.like("area_no", bicycleFacilitiesEnterpriseArea.getAreaNo());
            bicycleFacilitiesEnterpriseArea.setAreaNo(null);
        }
        if (StringUtils.isNotBlank(bicycleFacilitiesEnterpriseArea.getAreaName())) {
            queryWrapper.like("area_name", bicycleFacilitiesEnterpriseArea.getAreaName());
            bicycleFacilitiesEnterpriseArea.setAreaName(null);
        }
        IPage<BicycleFacilitiesEnterpriseArea> page = facilitiesEnterpriseAreaService.page(new Page<BicycleFacilitiesEnterpriseArea>(bicycleFacilitiesEnterpriseArea.getPage(), bicycleFacilitiesEnterpriseArea.getRows()), queryWrapper);

        List<BicycleFacilitiesEnterpriseAreaVo> bicycleFacilitiesEnterpriseAreaVoList = new ArrayList<BicycleFacilitiesEnterpriseAreaVo>();
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        page.getRecords().forEach(facilitiesEnterpriseArea -> {
            BicycleFacilitiesEnterpriseAreaVo bicycleFacilitiesEnterpriseAreaVo = new BicycleFacilitiesEnterpriseAreaVo();
            BeanUtil.copyProperties(facilitiesEnterpriseArea, bicycleFacilitiesEnterpriseAreaVo);
            bicycleFacilitiesEnterpriseAreaVo.setDistrictName(facilitiesEnterpriseArea.getDistrict() == null ? null
                    : DataCacheOprator.getInstance().getRegion(facilitiesEnterpriseArea.getDistrict()).getRegionName());
            bicycleFacilitiesEnterpriseAreaVo.setStreetName(facilitiesEnterpriseArea.getStreet() == null ? null
                    : DataCacheOprator.getInstance().getRegion(facilitiesEnterpriseArea.getStreet()).getRegionName());
            bicycleFacilitiesEnterpriseAreaVoList.add(bicycleFacilitiesEnterpriseAreaVo);
        });

        dataGrid.setTotal(page.getTotal());
        dataGrid.setRows(bicycleFacilitiesEnterpriseAreaVoList);
        return dataGrid;
    }

    /**
     * 分页查询企业区域信息
     *
     * @param bicycleFacilitiesEnterpriseArea
     * @return
     * @see
     */
    @ApiOperation("分页查询区域信息")
    @RequestMapping(value = "/selectAreaGrid", method = {RequestMethod.POST})
    public DataGrid<BicycleFacilitiesEnterpriseEquipmentVo> selectAreaGrid(@RequestBody BicycleFacilitiesEnterpriseArea bicycleFacilitiesEnterpriseArea, HttpServletRequest httpServletRequest) {
        DataGrid dataGrid = new DataGrid();

        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        if (userLoginResp.getRegionId() != null) {
            bicycleFacilitiesEnterpriseArea.setDistrict(userLoginResp.getRegionId());
        }
        bicycleFacilitiesEnterpriseArea.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());

        QueryWrapper<BicycleFacilitiesEnterpriseArea> queryWrapper = new QueryWrapper<BicycleFacilitiesEnterpriseArea>();

        if (StringUtils.isNotBlank(bicycleFacilitiesEnterpriseArea.getAreaNo())) {
            queryWrapper.like("area_no", bicycleFacilitiesEnterpriseArea.getAreaNo());
            bicycleFacilitiesEnterpriseArea.setAreaNo(null);
        }
        if (StringUtils.isNotBlank(bicycleFacilitiesEnterpriseArea.getAreaName())) {
            queryWrapper.like("area_name", bicycleFacilitiesEnterpriseArea.getAreaName());
            bicycleFacilitiesEnterpriseArea.setAreaName(null);
        }
        IPage<BicycleFacilitiesEnterpriseArea> page = facilitiesEnterpriseAreaService.page(new Page<BicycleFacilitiesEnterpriseArea>(bicycleFacilitiesEnterpriseArea.getPage(), bicycleFacilitiesEnterpriseArea.getRows()), queryWrapper);

        List<BicycleFacilitiesEnterpriseAreaVo> bicycleFacilitiesEnterpriseAreaVoList = new ArrayList<BicycleFacilitiesEnterpriseAreaVo>();
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        page.getRecords().forEach(facilitiesEnterpriseArea -> {
            BicycleFacilitiesEnterpriseAreaVo bicycleFacilitiesEnterpriseAreaVo = new BicycleFacilitiesEnterpriseAreaVo();
            BeanUtil.copyProperties(facilitiesEnterpriseArea, bicycleFacilitiesEnterpriseAreaVo);
            bicycleFacilitiesEnterpriseAreaVo.setDistrictName(facilitiesEnterpriseArea.getDistrict() == null ? null
                    : DataCacheOprator.getInstance().getRegion(facilitiesEnterpriseArea.getDistrict()).getRegionName());
            bicycleFacilitiesEnterpriseAreaVo.setStreetName(facilitiesEnterpriseArea.getStreet() == null ? null
                    : DataCacheOprator.getInstance().getRegion(facilitiesEnterpriseArea.getStreet()).getRegionName());
            bicycleFacilitiesEnterpriseAreaVoList.add(bicycleFacilitiesEnterpriseAreaVo);
        });
        dataGrid.setTotal(page.getTotal());
        dataGrid.setRows(bicycleFacilitiesEnterpriseAreaVoList);
        return dataGrid;
    }

    /**
     * 分页查询设备信息列表
     *
     * @param bicycleFacilitiesEnterpriseEquipment
     * @return
     * @see
     */
    @ApiOperation("分页查询设备信息列表")
    @RequestMapping(value = "/selectEquipmentGrid", method = {RequestMethod.POST})
    public DataGrid<BicycleFacilitiesEnterpriseEquipmentVo> selectEquipmentGrid(@RequestBody BicycleFacilitiesEnterpriseEquipment bicycleFacilitiesEnterpriseEquipment) {
        DataGrid dataGrid = new DataGrid();
        QueryWrapper<BicycleFacilitiesEnterpriseEquipment> queryWrapper = new QueryWrapper<BicycleFacilitiesEnterpriseEquipment>();
        IPage<BicycleFacilitiesEnterpriseEquipment> page = facilitiesEnterpriseEquipmentService.page(new Page<BicycleFacilitiesEnterpriseEquipment>(bicycleFacilitiesEnterpriseEquipment.getPage(), bicycleFacilitiesEnterpriseEquipment.getRows()), queryWrapper);
        List<BicycleFacilitiesEnterpriseEquipmentVo> bicycleFacilitiesEnterpriseEquipmentVoList = new ArrayList<BicycleFacilitiesEnterpriseEquipmentVo>();
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        page.getRecords().forEach(facilitiesEnterpriseEquipment -> {
            BicycleFacilitiesEnterpriseEquipmentVo bicycleFacilitiesEnterpriseEquipmentVo = new BicycleFacilitiesEnterpriseEquipmentVo();
            BeanUtil.copyProperties(facilitiesEnterpriseEquipment, bicycleFacilitiesEnterpriseEquipmentVo);
            bicycleFacilitiesEnterpriseEquipmentVo.setDistrictName(facilitiesEnterpriseEquipment.getDistrict() == null ? null
                    : DataCacheOprator.getInstance().getRegion(facilitiesEnterpriseEquipment.getDistrict()).getRegionName());
            bicycleFacilitiesEnterpriseEquipmentVo.setStreetName(facilitiesEnterpriseEquipment.getStreet() == null ? null
                    : DataCacheOprator.getInstance().getRegion(facilitiesEnterpriseEquipment.getStreet()).getRegionName());
            bicycleFacilitiesEnterpriseEquipmentVoList.add(bicycleFacilitiesEnterpriseEquipmentVo);
        });
        dataGrid.setTotal(page.getTotal());
        dataGrid.setRows(bicycleFacilitiesEnterpriseEquipmentVoList);
        return dataGrid;
    }

    /**
     * 基础设施-电子标签-分页查询电子标签列表
     *
     * @param rtcFacilitiesLabel
     * @return
     * @see
     */
//    @RequestMapping(value = "/selectLabelGrid", method = {RequestMethod.GET, RequestMethod.POST})
//    @ResponseBody
//    public Object selectLabelGrid(@RequestBody FacilitiesLabel rtcFacilitiesLabel) {
//        DataGrid dataGrid = new DataGrid();
//        try {
//            rtcFacilitiesLabel.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
//            dataGrid = rtcFacilitiesLabelService.queryFacilitiesLabelByPage(rtcFacilitiesLabel);
//        } catch (ServiceException e) {
//            log.error("selectLabelGrid error:", e);
//            dataGrid.setCode(e.getErrorCode());
//            dataGrid.setMessage(e.getErrorMsg());
//        }
//        return dataGrid;
//    }

    /**
     * 基础设施-设施一张图-根据行政区街道获取区域列表
     *
     * @param bicycleFacilitiesEnterpriseArea
     * @return
     * @see
     */
    @ApiOperation("根据行政区街道获取区域列表")
    @RequestMapping(value = "/selectFacilitiesMapList", method = {RequestMethod.POST})
    public Message<List<BicycleFacilitiesEnterpriseArea>> selectFacilitiesMapList(@RequestBody BicycleFacilitiesEnterpriseArea bicycleFacilitiesEnterpriseArea,
                                                                                  HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        if (userLoginResp.getRegionId() != null) {
            bicycleFacilitiesEnterpriseArea.setDistrict(userLoginResp.getRegionId());
        }
        bicycleFacilitiesEnterpriseArea.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        message.setResult(facilitiesEnterpriseAreaService.list(new QueryWrapper<BicycleFacilitiesEnterpriseArea>(bicycleFacilitiesEnterpriseArea)));
        return message;
    }

    /**
     * 基础设施-设施一张图-根据行政区街道获取区域数量
     *
     * @param bicycleFacilitiesEnterpriseArea
     * @return
     * @see
     */
    @ApiOperation("根据行政区街道获取区域数量")
    @RequestMapping(value = "/selectFacilitiesMapCount", method = {RequestMethod.POST})
    public Message<BicycleFacilitiesEnterpriseAreaCountVo> selectFacilitiesMapCount(@RequestBody BicycleFacilitiesEnterpriseArea bicycleFacilitiesEnterpriseArea) {
        Message message = new Message();

        bicycleFacilitiesEnterpriseArea.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        BicycleFacilitiesEnterpriseAreaCountVo facilitiesEnterpriseAreaCountResp = new BicycleFacilitiesEnterpriseAreaCountVo();
        // 总数量
        facilitiesEnterpriseAreaCountResp
                .setTotalCount(facilitiesEnterpriseAreaService.count(new QueryWrapper<BicycleFacilitiesEnterpriseArea>(bicycleFacilitiesEnterpriseArea)));
        // 锁桩站点数量
        bicycleFacilitiesEnterpriseArea.setAreaType(EnterpriseAreaTypeEnum.AREA_TYPE_0.getKey());
        facilitiesEnterpriseAreaCountResp
                .setSiteCount(facilitiesEnterpriseAreaService.count(new QueryWrapper<BicycleFacilitiesEnterpriseArea>(bicycleFacilitiesEnterpriseArea)));
        // 围栏数量
        bicycleFacilitiesEnterpriseArea.setAreaType(EnterpriseAreaTypeEnum.AREA_TYPE_1.getKey());
        facilitiesEnterpriseAreaCountResp
                .setRailCount(facilitiesEnterpriseAreaService.count(new QueryWrapper<BicycleFacilitiesEnterpriseArea>(bicycleFacilitiesEnterpriseArea)));

        bicycleFacilitiesEnterpriseArea.setAreaType(EnterpriseAreaTypeEnum.AREA_TYPE_0.getKey());
        List<BicycleFacilitiesEnterpriseArea> facilitiesEnterpriseAreas = facilitiesEnterpriseAreaService.list(new QueryWrapper<BicycleFacilitiesEnterpriseArea>(bicycleFacilitiesEnterpriseArea));
        // 车桩数量
        int rackCount = 0;
        for (BicycleFacilitiesEnterpriseArea facilitiesEnterpriseArea : facilitiesEnterpriseAreas) {
            rackCount += facilitiesEnterpriseArea.getVolume();
        }
        facilitiesEnterpriseAreaCountResp.setRackCount(rackCount);
        message.setResult(facilitiesEnterpriseAreaCountResp);
        return message;
    }

    /**
     * 根据主键ID获取区域详情
     *
     * @param bicycleFacilitiesEnterpriseArea
     * @return
     * @see
     */
    @ApiOperation("根据主键ID获取区域详情")
    @RequestMapping(value = "/selectFacilitiesMapDetail", method = {RequestMethod.POST})
    public Message<BicycleFacilitiesEnterpriseArea> selectFacilitiesMapDetail(@RequestBody BicycleFacilitiesEnterpriseArea bicycleFacilitiesEnterpriseArea) {
        Message message = new Message();
        message.setResult(facilitiesEnterpriseAreaService.getById(bicycleFacilitiesEnterpriseArea.getId()));
        return message;
    }

    /**
     * 基础设施-设施一张图-获取各区域区域数量
     *
     * @param
     * @return
     * @see
     */
    @ApiOperation("获取区域区域数量")
    @RequestMapping(value = "/selectFacilitiesMapChart", method = {RequestMethod.POST})
    public Message<BicycleFacilitiesMapChartVo> selectFacilitiesMapChart() {
        Message message = new Message();
        message = facilitiesEnterpriseAreaService.selectFacilitiesMapChart();
        return message;
    }


    @ApiOperation("设施一张图")
    @RequestMapping(value = "/selectFacilitiesMap", method = {RequestMethod.POST})
    public Message<List<BicycleFacilitiesMapVo>> selectFacilitiesMap(@RequestBody BicycleFacilitiesMapDto bicycleFacilitiesMapDto) {
        Message message = new Message();
        List<BicycleFacilitiesMapVo> bicycleFacilitiesMapVoList = new ArrayList<>();

        if (bicycleFacilitiesMapDto.getType() != null) {
            if (bicycleFacilitiesMapDto.getType() == 0) {
                //停放区
                List<BicycleFacilitiesGovArea> govAreaList = facilitiesGovAreaService.list(new QueryWrapper<BicycleFacilitiesGovArea>(BicycleFacilitiesGovArea.builder().hasDel(HasDelEnum.HAS_DEL_FALSE.getKey()).areaStatus(AreaStatusEnum.AREA_STATUS_0.getKey()).areaAttribute(GovAreaAttributeEnum.AREA_ATTRI_PARK.getKey()).build()));
                govAreaList.forEach(govArea -> {
                    BicycleFacilitiesMapVo bicycleFacilitiesMapVo = new BicycleFacilitiesMapVo();
                    bicycleFacilitiesMapVo.setAreaPoints(govArea.getAreaPoints());
                    bicycleFacilitiesMapVo.setLat(govArea.getLat());
                    bicycleFacilitiesMapVo.setLng(govArea.getLng());
                    bicycleFacilitiesMapVo.setType(0);
                    bicycleFacilitiesMapVoList.add(bicycleFacilitiesMapVo);
                });
            } else if (bicycleFacilitiesMapDto.getType() == 1) {
                //监控区
                List<BicycleFacilitiesGovArea> govAreaList = facilitiesGovAreaService.list(new QueryWrapper<BicycleFacilitiesGovArea>(BicycleFacilitiesGovArea.builder().hasDel(HasDelEnum.HAS_DEL_FALSE.getKey()).areaStatus(AreaStatusEnum.AREA_STATUS_0.getKey()).areaAttribute(GovAreaAttributeEnum.AREA_ATTRI_MONITOR.getKey()).build()));
                govAreaList.forEach(govArea -> {
                    BicycleFacilitiesMapVo bicycleFacilitiesMapVo = new BicycleFacilitiesMapVo();
                    bicycleFacilitiesMapVo.setAreaPoints(govArea.getAreaPoints());
                    bicycleFacilitiesMapVo.setLat(govArea.getLat());
                    bicycleFacilitiesMapVo.setLng(govArea.getLng());
                    bicycleFacilitiesMapVo.setType(1);
                    bicycleFacilitiesMapVoList.add(bicycleFacilitiesMapVo);
                });
            } else if (bicycleFacilitiesMapDto.getType() == 3) {
                //禁停区
                List<BicycleFacilitiesGovArea> govAreaList = facilitiesGovAreaService.list(new QueryWrapper<BicycleFacilitiesGovArea>(BicycleFacilitiesGovArea.builder().hasDel(HasDelEnum.HAS_DEL_FALSE.getKey()).areaStatus(AreaStatusEnum.AREA_STATUS_0.getKey()).areaAttribute(GovAreaAttributeEnum.AREA_ATTRI_ILLEGAL_PARK.getKey()).build()));
                govAreaList.forEach(govArea -> {
                    BicycleFacilitiesMapVo bicycleFacilitiesMapVo = new BicycleFacilitiesMapVo();
                    bicycleFacilitiesMapVo.setAreaPoints(govArea.getAreaPoints());
                    bicycleFacilitiesMapVo.setLat(govArea.getLat());
                    bicycleFacilitiesMapVo.setLng(govArea.getLng());
                    bicycleFacilitiesMapVo.setType(3);
                    bicycleFacilitiesMapVoList.add(bicycleFacilitiesMapVo);
                });
            } else {
                //电子停放区
                List<BicycleFacilitiesEnterpriseArea> enterpriseAreaList = facilitiesEnterpriseAreaService.list(new QueryWrapper<>(BicycleFacilitiesEnterpriseArea.builder().hasDel(HasDelEnum.HAS_DEL_FALSE.getKey()).areaStatus(AreaStatusEnum.AREA_STATUS_0.getKey()).areaType(EnterpriseAreaTypeEnum.AREA_TYPE_1.getKey()).build()));
                enterpriseAreaList.forEach(enterpriseArea -> {
                    BicycleFacilitiesMapVo bicycleFacilitiesMapVo = new BicycleFacilitiesMapVo();
                    bicycleFacilitiesMapVo.setAreaPoints(enterpriseArea.getAreaPoints());
                    bicycleFacilitiesMapVo.setLat(enterpriseArea.getLat());
                    bicycleFacilitiesMapVo.setLng(enterpriseArea.getLng());
                    bicycleFacilitiesMapVo.setType(2);
                    bicycleFacilitiesMapVoList.add(bicycleFacilitiesMapVo);
                });
            }
        } else {
            List<BicycleFacilitiesEnterpriseArea> enterpriseAreaList = facilitiesEnterpriseAreaService.list(new QueryWrapper<>(BicycleFacilitiesEnterpriseArea.builder().hasDel(HasDelEnum.HAS_DEL_FALSE.getKey()).areaStatus(AreaStatusEnum.AREA_STATUS_0.getKey()).areaType(EnterpriseAreaTypeEnum.AREA_TYPE_1.getKey()).build()));
            enterpriseAreaList.forEach(enterpriseArea -> {
                BicycleFacilitiesMapVo bicycleFacilitiesMapVo = new BicycleFacilitiesMapVo();
                bicycleFacilitiesMapVo.setAreaPoints(enterpriseArea.getAreaPoints());
                bicycleFacilitiesMapVo.setLat(enterpriseArea.getLat());
                bicycleFacilitiesMapVo.setLng(enterpriseArea.getLng());
                bicycleFacilitiesMapVo.setType(2);
                bicycleFacilitiesMapVoList.add(bicycleFacilitiesMapVo);
            });
            List<BicycleFacilitiesGovArea> govAreaList = facilitiesGovAreaService.list(new QueryWrapper<BicycleFacilitiesGovArea>(BicycleFacilitiesGovArea.builder().hasDel(HasDelEnum.HAS_DEL_FALSE.getKey()).areaStatus(AreaStatusEnum.AREA_STATUS_0.getKey()).build()));
            govAreaList.forEach(govArea -> {
                BicycleFacilitiesMapVo bicycleFacilitiesMapVo = new BicycleFacilitiesMapVo();
                bicycleFacilitiesMapVo.setAreaPoints(govArea.getAreaPoints());
                bicycleFacilitiesMapVo.setLat(govArea.getLat());
                bicycleFacilitiesMapVo.setLng(govArea.getLng());
                if (govArea.getAreaAttribute() == GovAreaAttributeEnum.AREA_ATTRI_MONITOR.getKey().intValue()) {
                    bicycleFacilitiesMapVo.setType(1);
                } else if (govArea.getAreaAttribute() == GovAreaAttributeEnum.AREA_ATTRI_PARK.getKey().intValue()) {
                    bicycleFacilitiesMapVo.setType(0);
                } else if (govArea.getAreaAttribute() == GovAreaAttributeEnum.AREA_ATTRI_ILLEGAL_PARK.getKey().intValue()) {
                    bicycleFacilitiesMapVo.setType(3);
                }
                bicycleFacilitiesMapVoList.add(bicycleFacilitiesMapVo);
            });
        }
        message.setResult(bicycleFacilitiesMapVoList);
        return message;
    }
}
