package com.bicycle.platform.controller;

import com.bicycle.common.entity.base.Message;
import com.bicycle.common.helper.RedisStringHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author : layne
 * @Date: 2020/8/11
 * @Time: 15:22
 * Description:
 */
@Api(tags = "redis功能")
@Slf4j
@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    private RedisStringHelper redisStringHelper;


    @ApiOperation("set")
    @GetMapping(value = "/set")
    @ResponseBody
    public Message set(String key, String value) {
        Message message = new Message();
        redisStringHelper.set(key, value);
        return message;
    }

    @ApiOperation("get")
    @GetMapping(value = "/get")
    @ResponseBody
    public Message get(String key) {
        Message message = new Message();
        message.setResult(redisStringHelper.get(key));
        return message;
    }
}
