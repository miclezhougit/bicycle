/*
 * 文件名：EnterpriseController.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月29日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.platform.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.service.service.IBicycleCommonEnterpriseService;
import com.bicycle.service.vo.BicycleSystemUserLoginVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


@Api(tags = "企业服务相关功能")
@Slf4j
@RestController
@RequestMapping("/enterprise")
public class EnterpriseController extends BaseController {

    @Autowired
    private IBicycleCommonEnterpriseService enterpriseService;

    /**
     * 新增企业信息
     *
     * @param bicycleCommonEnterprise
     * @param httpServletRequest
     * @return
     * @see
     */
    @ApiOperation("新增企业信息")
    @RequestMapping(value = "/insertEnterprise", method = { RequestMethod.POST})
    public Message insertEnterprise(@RequestBody BicycleCommonEnterprise bicycleCommonEnterprise, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        BicycleSystemUserLoginVo userLoginResp = this.loginUserInfo(httpServletRequest);
        bicycleCommonEnterprise.setCreateBy(userLoginResp.getSysUserId());
        bicycleCommonEnterprise.setCreateName(userLoginResp.getUserName());
        bicycleCommonEnterprise.setCreateTime(new Date());
        message = enterpriseService.insertEnterprise(bicycleCommonEnterprise);
        return message;
    }

    /**
     * 获取企业list
     *
     * @param bicycleCommonEnterprise
     * @param httpServletRequest
     * @return
     * @see
     */
    @ApiOperation("获取企业信息列表")
    @RequestMapping(value = "/selectEnterpriseList", method = { RequestMethod.POST})
    public Object selectEnterpriseList(@RequestBody BicycleCommonEnterprise bicycleCommonEnterprise, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        message.setResult(enterpriseService.list(new QueryWrapper<BicycleCommonEnterprise>(bicycleCommonEnterprise)));
        return message;
    }


}
