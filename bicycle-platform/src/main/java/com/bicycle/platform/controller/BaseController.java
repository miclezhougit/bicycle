/*
 * 文件名：BaseController.java 版权：Copyright by 联通系统集成有限公司 描述： 修改人：焦凯旋 修改时间：2019年6月22日 跟踪单号： 修改单号： 修改内容：
 */

package com.bicycle.platform.controller;

import com.alibaba.fastjson.JSON;
import com.bicycle.common.helper.RedisStringHelper;
import com.bicycle.service.vo.BicycleSystemUserLoginVo;
import com.bicycle.platform.constant.Constants;
import com.bicycle.service.entity.BicycleCommonFile;
import com.bicycle.service.service.IBicycleCommonFileService;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Collection;
import java.util.Date;

public class BaseController {
    @Autowired
    private IBicycleCommonFileService bicycleCommonFileService;
    @Autowired
    private RedisStringHelper redisStringHelper;

    /**
     * 获取登录用户信息
     *
     * @param httpServletRequest
     * @return
     * @see
     */
    public BicycleSystemUserLoginVo loginUserInfo(HttpServletRequest httpServletRequest) {
		return JSON.parseObject(redisStringHelper.get(Constants.BICYCLE_TOKEN_LOGIN_USER + (String) httpServletRequest.getSession().getAttribute(Constants.BICYCLE_SESSION_LOGIN_TOKEN)),
				BicycleSystemUserLoginVo.class);
        /*BicycleSystemUserLoginVo userLoginResp = new BicycleSystemUserLoginVo();
        userLoginResp.setRegionId(1);
        userLoginResp.setSysUserId(3);
        userLoginResp.setAccount("admin");
        userLoginResp.setUserName("admin");
        return userLoginResp;*/
    }

    /**
     * 上传文件
     *
     * @param file         文件实体
     * @param uploadPath   写文件路径
     * @param downloadPath 文件下载路径
     * @param fileName     文件名
     * @return
     * @throws Exception
     * @see
     */
    public BicycleCommonFile fileUpload(CommonsMultipartFile file, String uploadPath, String downloadPath, String fileName) throws Exception {
        return this.fileUpload(file, uploadPath, downloadPath, fileName, false);
    }

    /**
     * 上传文件
     *
     * @param file         文件实体
     * @param uploadPath   写文件路径
     * @param downloadPath 文件下载路径
     * @param fileName     文件名
     * @param isModifyName 是否更改文件名
     * @return
     * @throws Exception
     * @see
     */
    public BicycleCommonFile fileUpload(CommonsMultipartFile file, String uploadPath, String downloadPath, String fileName, boolean isModifyName) throws Exception {
		BicycleCommonFile bicycleCommonFile = new BicycleCommonFile();
		bicycleCommonFile.setCreateTime(new Date());

		bicycleCommonFile.setFileSize((int) file.getSize());

        String date = DateFormatUtils.format(new Date(), "yyyyMMdd");

        // 定义文件保存路径
        File saveFile = new File(uploadPath + File.separator + date + File.separator);
        // 如果文件地址为空时创建路径
        if (!saveFile.exists()) {
            saveFile.mkdirs();
        }

        FileItem fileItem = file.getFileItem();

        // 获取文件的后缀
        String suffix = fileItem.getName().substring(fileItem.getName().lastIndexOf("."));
		bicycleCommonFile.setFileSuffix(suffix);
        if (StringUtils.isBlank(fileName)) {
			bicycleCommonFile.setFileName(fileItem.getName().substring(0, fileItem.getName().lastIndexOf(".")));
        } else {
			bicycleCommonFile.setFileName(fileName.substring(0, fileName.lastIndexOf(".")));
        }
        // 更改文件名称
        if (isModifyName) {
            // 文件名称由时间构成
            String name = String.valueOf(System.currentTimeMillis());
			bicycleCommonFile.setFileName(name);
        }
		bicycleCommonFile.setRelativePath(File.separator + date + File.separator + bicycleCommonFile.getFileName() + bicycleCommonFile.getFileSuffix());
		bicycleCommonFile.setDownloadPath(downloadPath);
		bicycleCommonFile.setAbsolutePath(uploadPath);
        // 构件完整文件路径名称
        String pathname = uploadPath + bicycleCommonFile.getRelativePath();
        // 转移文件到指定地址
        fileItem.write(new File(pathname));
		bicycleCommonFileService.save(bicycleCommonFile);
        return bicycleCommonFile;
    }

    public <T> boolean isNullOrSizeIndexNull(Collection<T> c) {
        if (c == null || c.isEmpty()) {
            return true;
        }
        if (c.size() == 0) {
            return true;
        }
        Object[] o = c.toArray();
        if (o.length == 0) {
            return true;
        }
        if (o[0] == null) {
            return true;
        }
        return false;
    }
}
