package com.bicycle.platform.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.helper.RedisStringHelper;
import com.bicycle.service.cache.DataCacheOprator;
import com.bicycle.service.dto.BicycleStatisGlobalDto;
import com.bicycle.service.entity.BicycleStatisGlobal;
import com.bicycle.service.service.IBicycleStatisGlobalService;
import com.bicycle.service.vo.BicycleStatisGlobalVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/15
 * @Time: 15:21
 * Description:
 */
@Api(tags = "统计分析相关功能")
@Slf4j
@RestController
@RequestMapping("/statis")
public class StatisController extends BaseController {

    @Autowired
    private IBicycleStatisGlobalService bicycleStatisGlobalService;
    @Autowired
    private RedisStringHelper redisStringHelper;


    @ApiOperation("查询车辆总数统计信息")
    @PostMapping(value = "/selectBikeList")
    @ResponseBody
    public Message<List<BicycleStatisGlobalVo>> selectBikeList(@RequestBody BicycleStatisGlobalDto bicycleStatisGlobalDto, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        QueryWrapper<BicycleStatisGlobal> queryWrapper = new QueryWrapper<BicycleStatisGlobal>();
        BicycleStatisGlobal qBicycleStatisGlobal = new BicycleStatisGlobal();
        BeanUtil.copyProperties(bicycleStatisGlobalDto, qBicycleStatisGlobal);
        queryWrapper.setEntity(qBicycleStatisGlobal);
        if (bicycleStatisGlobalDto.getBeginDate() != null && bicycleStatisGlobalDto.getEndDate() != null) {
            queryWrapper.between("statis_date", DateUtil.beginOfDay(bicycleStatisGlobalDto.getBeginDate()), DateUtil.endOfDay(bicycleStatisGlobalDto.getEndDate()));
        }
        queryWrapper.orderByAsc("statis_date", "statis_hour", "enterprise_no");
        List<BicycleStatisGlobal> bicycleStatisGlobalList = bicycleStatisGlobalService.list(queryWrapper);
        List<DateTime> dateTimes = bicycleStatisGlobalDto.getStatisType() == 2 ?
                DateUtil.rangeToList(DateUtil.beginOfDay(bicycleStatisGlobalDto.getBeginDate()), DateUtil.endOfDay(bicycleStatisGlobalDto.getEndDate()), DateField.HOUR_OF_DAY) :
                DateUtil.rangeToList(bicycleStatisGlobalDto.getBeginDate(), bicycleStatisGlobalDto.getEndDate(), DateField.DAY_OF_MONTH);
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);

        List<BicycleStatisGlobalVo> bicycleStatisGlobalVoList = new ArrayList<BicycleStatisGlobalVo>();
        dateTimes.forEach(dateTime -> {
            BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
            List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
            if (bicycleStatisGlobalDto.getStatisType() == 2) {
                bicycleStatisGlobalVo.setStatisHour(String.valueOf(DateUtil.hour(dateTime, true)));
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.hour(dateTime, true) == Integer.parseInt(bicycleStatisGlobal.getStatisHour())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getBikeCount());
                            enterpriseVoList.add(enterpriseVo);
                        }

                    }
                });
            } else {
                bicycleStatisGlobalVo.setStatisDate(dateTime);
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getBikeCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
            }
            bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
            bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
        });
        message.setResult(bicycleStatisGlobalVoList);
        return message;
    }

    @ApiOperation("查询超量告警统计信息")
    @PostMapping(value = "/selectExcessList")
    @ResponseBody
    public Message<List<BicycleStatisGlobalVo>> selectExcessList(@RequestBody BicycleStatisGlobalDto bicycleStatisGlobalDto, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        QueryWrapper<BicycleStatisGlobal> queryWrapper = new QueryWrapper<BicycleStatisGlobal>();
        BicycleStatisGlobal qBicycleStatisGlobal = new BicycleStatisGlobal();
        BeanUtil.copyProperties(bicycleStatisGlobalDto, qBicycleStatisGlobal);
        queryWrapper.setEntity(qBicycleStatisGlobal);
        if (bicycleStatisGlobalDto.getBeginDate() != null && bicycleStatisGlobalDto.getEndDate() != null) {
            queryWrapper.between("statis_date", DateUtil.beginOfDay(bicycleStatisGlobalDto.getBeginDate()), DateUtil.endOfDay(bicycleStatisGlobalDto.getEndDate()));
        }
        queryWrapper.orderByAsc("statis_date", "statis_hour", "enterprise_no");
        List<BicycleStatisGlobal> bicycleStatisGlobalList = bicycleStatisGlobalService.list(queryWrapper);
        List<DateTime> dateTimes = bicycleStatisGlobalDto.getStatisType() == 2 ?
                DateUtil.rangeToList(DateUtil.beginOfDay(bicycleStatisGlobalDto.getBeginDate()), DateUtil.endOfDay(bicycleStatisGlobalDto.getEndDate()), DateField.HOUR_OF_DAY) :
                DateUtil.rangeToList(bicycleStatisGlobalDto.getBeginDate(), bicycleStatisGlobalDto.getEndDate(), DateField.DAY_OF_MONTH);
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);

        List<BicycleStatisGlobalVo> bicycleStatisGlobalVoList = new ArrayList<BicycleStatisGlobalVo>();
        dateTimes.forEach(dateTime -> {
            BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
            List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
            if (bicycleStatisGlobalDto.getStatisType() == 2) {
                bicycleStatisGlobalVo.setStatisHour(String.valueOf(DateUtil.hour(dateTime, true)));
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.hour(dateTime, true) == Integer.parseInt(bicycleStatisGlobal.getStatisHour())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getExcessCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
            } else {
                bicycleStatisGlobalVo.setStatisDate(dateTime);
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getExcessCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
            }
            bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
            bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
        });
        message.setResult(bicycleStatisGlobalVoList);
        return message;
    }

    @ApiOperation("查询违停案件统计信息")
    @PostMapping(value = "/selectIllegalParkList")
    @ResponseBody
    public Message<List<BicycleStatisGlobalVo>> selectIllegalParkList(@RequestBody BicycleStatisGlobalDto bicycleStatisGlobalDto, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        QueryWrapper<BicycleStatisGlobal> queryWrapper = new QueryWrapper<BicycleStatisGlobal>();
        BicycleStatisGlobal qBicycleStatisGlobal = new BicycleStatisGlobal();
        BeanUtil.copyProperties(bicycleStatisGlobalDto, qBicycleStatisGlobal);
        queryWrapper.setEntity(qBicycleStatisGlobal);
        if (bicycleStatisGlobalDto.getBeginDate() != null && bicycleStatisGlobalDto.getEndDate() != null) {
            queryWrapper.between("statis_date", DateUtil.beginOfDay(bicycleStatisGlobalDto.getBeginDate()), DateUtil.endOfDay(bicycleStatisGlobalDto.getEndDate()));
        }
        queryWrapper.orderByAsc("statis_date", "statis_hour", "enterprise_no");
        List<BicycleStatisGlobal> bicycleStatisGlobalList = bicycleStatisGlobalService.list(queryWrapper);
        List<DateTime> dateTimes = bicycleStatisGlobalDto.getStatisType() == 2 ?
                DateUtil.rangeToList(DateUtil.beginOfDay(bicycleStatisGlobalDto.getBeginDate()), DateUtil.endOfDay(bicycleStatisGlobalDto.getEndDate()), DateField.HOUR_OF_DAY) :
                DateUtil.rangeToList(bicycleStatisGlobalDto.getBeginDate(), bicycleStatisGlobalDto.getEndDate(), DateField.DAY_OF_MONTH);
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);

        List<BicycleStatisGlobalVo> bicycleStatisGlobalVoList = new ArrayList<BicycleStatisGlobalVo>();
        dateTimes.forEach(dateTime -> {
            BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
            List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
            if (bicycleStatisGlobalDto.getStatisType() == 2) {
                bicycleStatisGlobalVo.setStatisHour(String.valueOf(DateUtil.hour(dateTime, true)));
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.hour(dateTime, true) == Integer.parseInt(bicycleStatisGlobal.getStatisHour())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getIllegalParkCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
            } else {
                bicycleStatisGlobalVo.setStatisDate(dateTime);
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getIllegalParkCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
            }
            bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
            bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
        });
        message.setResult(bicycleStatisGlobalVoList);
        return message;
    }

    @ApiOperation("查询执法案件统计信息")
    @PostMapping(value = "/selectEnforceCaseList")
    @ResponseBody
    public Message<List<BicycleStatisGlobalVo>> selectgetEnforceCaseList(@RequestBody BicycleStatisGlobalDto bicycleStatisGlobalDto, HttpServletRequest httpServletRequest) {
        Message message = new Message();
        QueryWrapper<BicycleStatisGlobal> queryWrapper = new QueryWrapper<BicycleStatisGlobal>();
        BicycleStatisGlobal qBicycleStatisGlobal = new BicycleStatisGlobal();
        BeanUtil.copyProperties(bicycleStatisGlobalDto, qBicycleStatisGlobal);
        queryWrapper.setEntity(qBicycleStatisGlobal);
        if (bicycleStatisGlobalDto.getBeginDate() != null && bicycleStatisGlobalDto.getEndDate() != null) {
            queryWrapper.between("statis_date", DateUtil.beginOfDay(bicycleStatisGlobalDto.getBeginDate()), DateUtil.endOfDay(bicycleStatisGlobalDto.getEndDate()));
        }
        queryWrapper.orderByAsc("statis_date", "statis_hour", "enterprise_no");
        List<BicycleStatisGlobal> bicycleStatisGlobalList = bicycleStatisGlobalService.list(queryWrapper);
        List<DateTime> dateTimes = bicycleStatisGlobalDto.getStatisType() == 2 ?
                DateUtil.rangeToList(DateUtil.beginOfDay(bicycleStatisGlobalDto.getBeginDate()), DateUtil.endOfDay(bicycleStatisGlobalDto.getEndDate()), DateField.HOUR_OF_DAY) :
                DateUtil.rangeToList(bicycleStatisGlobalDto.getBeginDate(), bicycleStatisGlobalDto.getEndDate(), DateField.DAY_OF_MONTH);
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);

        List<BicycleStatisGlobalVo> bicycleStatisGlobalVoList = new ArrayList<BicycleStatisGlobalVo>();
        dateTimes.forEach(dateTime -> {
            BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
            List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
            if (bicycleStatisGlobalDto.getStatisType() == 2) {
                bicycleStatisGlobalVo.setStatisHour(String.valueOf(DateUtil.hour(dateTime, true)));
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.hour(dateTime, true) == Integer.parseInt(bicycleStatisGlobal.getStatisHour())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getEnforceCaseCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
            } else {
                bicycleStatisGlobalVo.setStatisDate(dateTime);
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getEnforceCaseCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
            }


            bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
            bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
        });
        message.setResult(bicycleStatisGlobalVoList);
        return message;
    }

    public static void main(String[] args) {
        List<DateTime> dateTimes = DateUtil.rangeToList(DateUtil.beginOfDay(new Date()), DateUtil.endOfDay(new Date()), DateField.HOUR_OF_DAY);
        dateTimes.forEach(dateTime ->{
            System.out.println(dateTime);
        });
    }


}
