package com.bicycle.platform.controller;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.helper.RedisStringHelper;
import com.bicycle.service.cache.DataCacheOprator;
import com.bicycle.service.config.MyBatisPlusConfig;
import com.bicycle.service.dto.BicycleStatisGlobalDto;
import com.bicycle.service.entity.*;
import com.bicycle.service.service.*;
import com.bicycle.service.vo.BicycleIndexStatisVo;
import com.bicycle.service.vo.BicycleStatisGlobalVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/15
 * @Time: 17:10
 * Description:
 */
@Api(tags = "首页相关功能")
@Slf4j
@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private IBicycleStatisGlobalService bicycleStatisGlobalService;
    @Autowired
    private IBicycleDataBicycleSiteService bicycleSiteService;
    @Autowired
    private IBicycleDataOrderInfoService orderInfoService;
    @Autowired
    private IBicycleSequenceReportDetailService reportDetailService;
    @Autowired
    private IBicycleFacilitiesGovAreaService facilitiesGovAreaService;
    @Autowired
    private IBicycleBasicsNoRecordService basicsNoRecordService;
    @Autowired
    private IBicycleBasicsBicycleService basicsBicycleService;
    @Autowired
    private RedisStringHelper redisStringHelper;
    @Autowired
    private Environment environment;


    @ApiOperation("查询当日数量")
    @GetMapping(value = "/initStatis")
    public Message<BicycleIndexStatisVo> initStatis() {
        Message message = new Message();

        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        List<BicycleCommonEnterprise> enterpriseList = DataCacheOprator.getInstance().getEnterpriseList();
        Integer bicycleCount = 0;

        for (BicycleCommonEnterprise enterprise : enterpriseList){
            QueryWrapper<BicycleDataBicycleSite> queryWrapper = new QueryWrapper<BicycleDataBicycleSite>();
            queryWrapper.groupBy("bicycle_no");
            queryWrapper.eq("com_id", enterprise.getEnterpriseNo());
            MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(new Date(), "yyyyMMdd"));
//        int bicycleCount = bicycleSiteService.count(queryWrapper);
            List<BicycleDataBicycleSite> bicycleSites = bicycleSiteService.list(queryWrapper);
            bicycleCount += bicycleSites == null ? 0 : bicycleSites.size();
        }

        QueryWrapper<BicycleDataOrderInfo> orderInfoQueryWrapper = new QueryWrapper<BicycleDataOrderInfo>();
        orderInfoQueryWrapper.between("end_time", DateUtil.beginOfDay(new Date()), DateUtil.endOfDay(new Date()));
        MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(new Date(), "yyyyMM"));
        int orderCount = orderInfoService.count(orderInfoQueryWrapper);
//        List<BicycleDataOrderInfo> orderInfos = orderInfoService.list(orderInfoQueryWrapper);
//        int orderCount = orderInfos == null ? 0 : orderInfos.size();

        QueryWrapper<BicycleSequenceReportDetail> reportDetailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
        reportDetailQueryWrapper.between("report_time", DateUtil.beginOfDay(new Date()), DateUtil.endOfDay(new Date()));
        reportDetailQueryWrapper.like("report_type", 5);
        int excessCount = reportDetailService.count(reportDetailQueryWrapper);

        QueryWrapper<BicycleSequenceReportDetail> sequenceReportDetailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
        sequenceReportDetailQueryWrapper.between("report_time", DateUtil.beginOfDay(new Date()), DateUtil.endOfDay(new Date()));
        sequenceReportDetailQueryWrapper.like("report_type", 1);
        int illegalParkCount = reportDetailService.count(sequenceReportDetailQueryWrapper);
        int bicycleParkCount = facilitiesGovAreaService.sumParkValue();
        BicycleIndexStatisVo bicycleIndexStatisVo = new BicycleIndexStatisVo();
        bicycleIndexStatisVo.setBicycleCount(bicycleCount);
        bicycleIndexStatisVo.setBicycleParkCount(bicycleParkCount);
        bicycleIndexStatisVo.setExcessCount(excessCount);
        bicycleIndexStatisVo.setIllegalParkCount(illegalParkCount);
        bicycleIndexStatisVo.setOrderCount(orderCount);
        message.setResult(bicycleIndexStatisVo);
        return message;
    }

    @ApiOperation("查询各企业车辆总数")
    @GetMapping(value = "/selectEnterpriseBicycleCount")
    public Message<List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>> selectEnterpriseBicycleCount() {
        Message message = new Message();
        List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();

        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        List<BicycleCommonEnterprise> enterpriseList = DataCacheOprator.getInstance().getEnterpriseList();
        enterpriseList.forEach(enterprise -> {
            BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
            QueryWrapper<BicycleDataBicycleSite> queryWrapper = new QueryWrapper<BicycleDataBicycleSite>();
            queryWrapper.groupBy("bicycle_no");
            queryWrapper.eq("com_id", enterprise.getEnterpriseNo());
            MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(new Date(), "yyyyMMdd"));

            List<BicycleDataBicycleSite> bicycleSites = bicycleSiteService.list(queryWrapper);
            int bicycleCount = bicycleSites == null ? 0 : bicycleSites.size();

//            int bicycleCount = bicycleSiteService.count(queryWrapper);

            enterpriseVo.setStatisCount(bicycleCount);
            enterpriseVo.setEnterpriseNo(enterprise.getEnterpriseNo());
            enterpriseVo.setEnterpriseName(enterprise.getEnterpriseAlias());
            enterpriseVoList.add(enterpriseVo);
        });
        message.setResult(enterpriseVoList);
        return message;
    }

    @ApiOperation("查询各企业车辆总数趋势")
    @GetMapping(value = "/selectEnterpriseBicycleChart")
    public Message<List<BicycleStatisGlobalVo>> selectEnterpriseBicycleChart() {
        Message message = new Message();
        QueryWrapper<BicycleStatisGlobal> queryWrapper = new QueryWrapper<BicycleStatisGlobal>();
        BicycleStatisGlobal qBicycleStatisGlobal = new BicycleStatisGlobal();
        qBicycleStatisGlobal.setStatisType(1);
        queryWrapper.setEntity(qBicycleStatisGlobal);
        Date beginDate = DateUtil.beginOfDay(DateUtil.offsetDay(new Date(), -7));
        Date endDate = DateUtil.endOfDay(DateUtil.offsetDay(new Date(), -1));
        queryWrapper.between("statis_date", beginDate, endDate);

        queryWrapper.orderByAsc("statis_date", "statis_hour", "enterprise_no");
        List<BicycleStatisGlobal> bicycleStatisGlobalList = bicycleStatisGlobalService.list(queryWrapper);
        List<DateTime> dateTimes = DateUtil.rangeToList(beginDate, endDate, DateField.DAY_OF_MONTH);
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);

        List<BicycleStatisGlobalVo> bicycleStatisGlobalVoList = new ArrayList<BicycleStatisGlobalVo>();
        dateTimes.forEach(dateTime -> {
            BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
            List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
            bicycleStatisGlobalVo.setStatisDate(dateTime);
            bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                    BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                    if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                        enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                        enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                        enterpriseVo.setStatisCount(bicycleStatisGlobal.getBikeCount());
                        enterpriseVoList.add(enterpriseVo);
                    }
                }
            });
            bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
            bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
        });
        message.setResult(bicycleStatisGlobalVoList);
        return message;
    }

    @ApiOperation("查询各企业订单总数趋势")
    @GetMapping(value = "/selectEnterpriseOrderChart")
    public Message<List<BicycleStatisGlobalVo>> selectEnterpriseOrderChart() {
        Message message = new Message();
        QueryWrapper<BicycleStatisGlobal> queryWrapper = new QueryWrapper<BicycleStatisGlobal>();
        BicycleStatisGlobal qBicycleStatisGlobal = new BicycleStatisGlobal();
        qBicycleStatisGlobal.setStatisType(1);
        queryWrapper.setEntity(qBicycleStatisGlobal);
        Date beginDate = DateUtil.beginOfDay(DateUtil.offsetDay(new Date(), -7));
        Date endDate = DateUtil.endOfDay(DateUtil.offsetDay(new Date(), -1));
        queryWrapper.between("statis_date", beginDate, endDate);

        queryWrapper.orderByAsc("statis_date", "statis_hour", "enterprise_no");
        List<BicycleStatisGlobal> bicycleStatisGlobalList = bicycleStatisGlobalService.list(queryWrapper);
        List<DateTime> dateTimes = DateUtil.rangeToList(beginDate, endDate, DateField.DAY_OF_MONTH);
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);

        List<BicycleStatisGlobalVo> bicycleStatisGlobalVoList = new ArrayList<BicycleStatisGlobalVo>();
        dateTimes.forEach(dateTime -> {
            BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
            List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
            bicycleStatisGlobalVo.setStatisDate(dateTime);
            bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                    BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                    if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                        enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                        enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                        enterpriseVo.setStatisCount(bicycleStatisGlobal.getOrderCount());
                        enterpriseVoList.add(enterpriseVo);
                    }
                }
            });
            bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
            bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
        });
        message.setResult(bicycleStatisGlobalVoList);
        return message;
    }

    @ApiOperation("查询各企业违停总数趋势")
    @GetMapping(value = "/selectEnterpriseillegalParkChart")
    public Message<List<BicycleStatisGlobalVo>> selectEnterpriseillegalParkChart() {
        Message message = new Message();
        QueryWrapper<BicycleStatisGlobal> queryWrapper = new QueryWrapper<BicycleStatisGlobal>();
        BicycleStatisGlobal qBicycleStatisGlobal = new BicycleStatisGlobal();
        qBicycleStatisGlobal.setStatisType(1);
        queryWrapper.setEntity(qBicycleStatisGlobal);
        Date beginDate = DateUtil.beginOfDay(DateUtil.offsetDay(new Date(), -7));
        Date endDate = DateUtil.endOfDay(DateUtil.offsetDay(new Date(), -1));
        queryWrapper.between("statis_date", beginDate, endDate);

        queryWrapper.orderByAsc("statis_date", "statis_hour", "enterprise_no");
        List<BicycleStatisGlobal> bicycleStatisGlobalList = bicycleStatisGlobalService.list(queryWrapper);
        List<DateTime> dateTimes = DateUtil.rangeToList(beginDate, endDate, DateField.DAY_OF_MONTH);
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);

        List<BicycleStatisGlobalVo> bicycleStatisGlobalVoList = new ArrayList<BicycleStatisGlobalVo>();
        dateTimes.forEach(dateTime -> {
            BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
            List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
            bicycleStatisGlobalVo.setStatisDate(dateTime);
            bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                    BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                    if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                        enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                        enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                        enterpriseVo.setStatisCount(bicycleStatisGlobal.getIllegalParkCount());
                        enterpriseVoList.add(enterpriseVo);
                    }
                }
            });
            bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
            bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
        });
        message.setResult(bicycleStatisGlobalVoList);
        return message;
    }

    @ApiOperation("查询当日各类数据总数")
    @GetMapping(value = "/initStatisCount")
    public Message<BicycleIndexStatisVo> initStatisCount() {
        Message message = new Message();

        //车辆总数
        Integer reportLocationCount = 0;
        QueryWrapper<BicycleBasicsBicycle> queryWrapper = new QueryWrapper<BicycleBasicsBicycle>();
        if(environment.getProperty("region.district") != null) {
            queryWrapper.eq("district", environment.getProperty("region.district"));
        }
        if(environment.getProperty("region.street") != null) {
            queryWrapper.eq("street", environment.getProperty("region.street"));
        }
//        queryWrapper.eq("put_on_status",1);
        reportLocationCount = basicsBicycleService.count(queryWrapper);

        //违规停放
        QueryWrapper<BicycleBasicsBicycle> illegalParkQueryWrapper = new QueryWrapper<BicycleBasicsBicycle>();
        illegalParkQueryWrapper.eq("has_illegal_park", 1);
//        illegalParkQueryWrapper.eq("lock_status", 1);
        if(environment.getProperty("region.district") != null) {
            illegalParkQueryWrapper.eq("district", environment.getProperty("region.district"));
        }
        if(environment.getProperty("region.street") != null) {
            illegalParkQueryWrapper.eq("street", environment.getProperty("region.street"));
        }
        int illegalParkCount = basicsBicycleService.count(illegalParkQueryWrapper);

        //违规投放
        QueryWrapper<BicycleBasicsBicycle> basicsNoRecordQueryWrapper = new QueryWrapper<BicycleBasicsBicycle>();
        basicsNoRecordQueryWrapper.eq("put_on_status",0);
        if(environment.getProperty("region.district") != null) {
            basicsNoRecordQueryWrapper.eq("district", environment.getProperty("region.district"));
        }
        if(environment.getProperty("region.street") != null) {
            basicsNoRecordQueryWrapper.eq("street", environment.getProperty("region.street"));
        }
        int illegalDeliveryCount = basicsBicycleService.count(basicsNoRecordQueryWrapper);

        BicycleIndexStatisVo bicycleIndexStatisVo = new BicycleIndexStatisVo();
        bicycleIndexStatisVo.setReportLocationCount(reportLocationCount);
        bicycleIndexStatisVo.setIllegalParkCount(illegalParkCount);
        bicycleIndexStatisVo.setIllegalDeliveryCount(illegalDeliveryCount);
        message.setResult(bicycleIndexStatisVo);
        return message;
    }

    @ApiOperation("根据选择的查询数据类型获取各企业车辆占比数")
    @RequestMapping(value = "/getEnterpriseBicyclePieChart", method = RequestMethod.POST)
    public Message<List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>>
                getEnterpriseBicyclePieChart(@RequestBody BicycleStatisGlobalDto bicycleStatisGlobalDto) {
        Message message = new Message();
        List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();

        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        List<BicycleCommonEnterprise> enterpriseList = DataCacheOprator.getInstance().getEnterpriseList();
        enterpriseList.forEach(enterprise -> {
            BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
            Integer bicycleCount = 0;
            if(bicycleStatisGlobalDto.getDataType() == 2) {//违停车辆数
                QueryWrapper<BicycleBasicsBicycle> queryWrapper = new QueryWrapper<BicycleBasicsBicycle>();
                queryWrapper.eq("has_illegal_park", 1);
//                queryWrapper.eq("lock_status", 1);
                queryWrapper.eq("enterprise_no", enterprise.getEnterpriseNo());
                if(environment.getProperty("region.district") != null) {
                    queryWrapper.eq("district", environment.getProperty("region.district"));
                }
                if(environment.getProperty("region.street") != null) {
                    queryWrapper.eq("street", environment.getProperty("region.street"));
                }
                bicycleCount = basicsBicycleService.count(queryWrapper);
            } else if(bicycleStatisGlobalDto.getDataType() == 3) {//违投车辆数
                QueryWrapper<BicycleBasicsBicycle> queryWrapper = new QueryWrapper<BicycleBasicsBicycle>();
                queryWrapper.eq("put_on_status", 0);
                queryWrapper.eq("enterprise_no", enterprise.getEnterpriseNo());
                if(environment.getProperty("region.district") != null) {
                    queryWrapper.eq("district", environment.getProperty("region.district"));
                }
                if(environment.getProperty("region.street") != null) {
                    queryWrapper.eq("street", environment.getProperty("region.street"));
                }
                bicycleCount = basicsBicycleService.count(queryWrapper);
            } else { //实时车辆数
                QueryWrapper<BicycleBasicsBicycle> queryWrapper = new QueryWrapper<BicycleBasicsBicycle>();
                if(environment.getProperty("region.district") != null) {
                    queryWrapper.eq("district", environment.getProperty("region.district"));
                }
                if(environment.getProperty("region.street") != null) {
                    queryWrapper.eq("street", environment.getProperty("region.street"));
                }
//                queryWrapper.eq("put_on_status",1);
                queryWrapper.eq("enterprise_no", enterprise.getEnterpriseNo());
                bicycleCount = basicsBicycleService.count(queryWrapper);
            }
            enterpriseVo.setStatisCount(bicycleCount);
            enterpriseVo.setEnterpriseNo(enterprise.getEnterpriseNo());
            enterpriseVo.setEnterpriseName(enterprise.getEnterpriseAlias());
            enterpriseVoList.add(enterpriseVo);
        });
        message.setResult(enterpriseVoList);
        return message;
    }

    @ApiOperation("根据选择的查询数据类型获取各企业车辆总数趋势")
    @RequestMapping(value = "/getEnterpriseBicycleLineChart", method = RequestMethod.POST)
    public Message<List<BicycleStatisGlobalVo>> getEnterpriseBicycleLineChart(
            @RequestBody BicycleStatisGlobalDto bicycleStatisGlobalDto) {
        Message message = new Message();
        QueryWrapper<BicycleStatisGlobal> queryWrapper = new QueryWrapper<BicycleStatisGlobal>();
        BicycleStatisGlobal qBicycleStatisGlobal = new BicycleStatisGlobal();
        qBicycleStatisGlobal.setStatisType(1);
        queryWrapper.setEntity(qBicycleStatisGlobal);
        Date beginDate = DateUtil.beginOfDay(DateUtil.offsetDay(new Date(), -7));
        Date endDate = DateUtil.endOfDay(DateUtil.offsetDay(new Date(), -1));
        queryWrapper.between("statis_date", beginDate, endDate);
        queryWrapper.orderByAsc("statis_date", "statis_hour", "enterprise_no");
        List<BicycleStatisGlobal> bicycleStatisGlobalList = bicycleStatisGlobalService.list(queryWrapper);
        List<DateTime> dateTimes = DateUtil.rangeToList(beginDate, endDate, DateField.DAY_OF_MONTH);
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);

        //获取企业信息
        List<BicycleCommonEnterprise> bicycleCommonEnterpriseList = DataCacheOprator.getInstance().getAllEnterpriseList();

        List<BicycleStatisGlobalVo> bicycleStatisGlobalVoList = new ArrayList<BicycleStatisGlobalVo>();
        if(bicycleStatisGlobalDto.getDataType() == 2) { // 违停车辆总数趋势
            dateTimes.forEach(dateTime -> {
                BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
                List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
                bicycleStatisGlobalVo.setStatisDate(dateTime);
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getIllegalParkCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
                bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
                bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
            });
        } else if(bicycleStatisGlobalDto.getDataType() == 3) {// 违投车辆总数趋势
            dateTimes.forEach(dateTime -> {
                BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
                List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
                bicycleStatisGlobalVo.setStatisDate(dateTime);
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getIllegalDeliveryCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
                bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
                bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
            });
        } else { // 车辆总数趋势
            dateTimes.forEach(dateTime -> {
                BicycleStatisGlobalVo bicycleStatisGlobalVo = new BicycleStatisGlobalVo();
                List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
                bicycleStatisGlobalVo.setStatisDate(dateTime);
                bicycleStatisGlobalList.forEach(bicycleStatisGlobal -> {
                    if (DateUtil.isSameDay(dateTime, bicycleStatisGlobal.getStatisDate())) {
                        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                        if(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()) != null) {
                            enterpriseVo.setEnterpriseNo(bicycleStatisGlobal.getEnterpriseNo());
                            enterpriseVo.setEnterpriseName(DataCacheOprator.getInstance().getEnterprise(bicycleStatisGlobal.getEnterpriseNo()).getEnterpriseAlias());
                            enterpriseVo.setStatisCount(bicycleStatisGlobal.getBikeCount());
                            enterpriseVoList.add(enterpriseVo);
                        }
                    }
                });
                bicycleStatisGlobalVo.setColletions(bicycleStatisGlobalService.dealBicycleStatisGlobal(enterpriseVoList));
                bicycleStatisGlobalVoList.add(bicycleStatisGlobalVo);
            });
        }
        message.setResult(bicycleStatisGlobalVoList);
        return message;
    }

}
