package com.bicycle.platform.config;

import com.bicycle.platform.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/9
 * @Time: 23:25
 * Description:
 */
@Configuration
//@ConfigurationProperties(prefix = "interceptor.sign")
public class LoginInterceptorConfig implements WebMvcConfigurer {
    @Autowired
    private LoginInterceptor loginInterceptor;

    private List<String> excludePathPatterns;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(loginInterceptor);
        registration.addPathPatterns("/**");
        registration.excludePathPatterns(
                "/system/login",
                "/system/code",
                "/system/requestUrl",
                "/redis/**",
                "/static/**",
                "/template/**",
                "/index.html",
                "/bigData/**",
                "/swagger-resources/**",
                "/webjars/**",
                "/v2/**",
                "/swagger-ui.html/**"
        );
    }
}
