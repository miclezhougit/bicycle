package com.bicycle.platform.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.platform.service.BicycleSystemRoleService;
import com.bicycle.service.entity.BicycleSystemRole;
import com.bicycle.service.mapper.BicycleSystemRoleMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author : layne
 * @Date: 2020/8/10
 * @Time: 22:06
 * Description:
 */
@Service("roleService")
@Transactional(rollbackFor = Exception.class)
public class BicycleSystemRoleServiceImpl implements BicycleSystemRoleService {

    @Autowired
    private BicycleSystemRoleMapper systemRoleMapper;


    @Override
    public Message selectSystemRoleList(BicycleSystemRole bicycleSystemRole) {
        Message message = new Message();
        QueryWrapper<BicycleSystemRole> queryWrapper = new QueryWrapper<BicycleSystemRole>();
        queryWrapper.setEntity(bicycleSystemRole);
        if(StringUtils.isNotBlank(bicycleSystemRole.getRoleName())){
            queryWrapper.like("role_name",bicycleSystemRole.getRoleName());
            bicycleSystemRole.setRoleName(null);
        }
        message.setResult(systemRoleMapper.selectList(queryWrapper));
        return message;
    }
}
