package com.bicycle.platform.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleSystemRole;

/**
 * @author : layne
 * @Date: 2020/8/10
 * @Time: 22:06
 * Description:
 */
public interface BicycleSystemRoleService {

    /**
     * 获取角色信息列表
     *
     * @param bicycleSystemRole
     * @return
     */
    Message selectSystemRoleList(BicycleSystemRole bicycleSystemRole);
}
