package com.bicycle.platform.service;

import com.bicycle.common.entity.base.DataGrid;
import com.bicycle.common.entity.base.Message;
import com.bicycle.service.dto.BicycleSystemUserDto;
import com.bicycle.service.dto.BicycleSystemUserGridDto;
import com.bicycle.service.dto.BicycleSystemUserPasswordDto;


public interface BicycleSystemUserService {


    /**
     * 插入系统用户信息
     *
     * @param bicycleSystemUserDto
     * @return
     */
    Message insertSysUser(BicycleSystemUserDto bicycleSystemUserDto);

    /**
     * 修改系统用户信息
     *
     * @param bicycleSystemUserDto
     * @return
     */
    Message updateSysUser(BicycleSystemUserDto bicycleSystemUserDto);

    /**
     * 分页查询系统用户
     *
     * @param bicycleSystemUserGridDto
     * @return
     */
    DataGrid querySystemUserByPage(BicycleSystemUserGridDto bicycleSystemUserGridDto);

    /**
     * 修改系统用户密码
     *
     * @param bicycleSystemUserPasswordDto
     * @return
     */
    Message updateSysUserPassword(BicycleSystemUserPasswordDto bicycleSystemUserPasswordDto);
}
