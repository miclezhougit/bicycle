package com.bicycle.platform.service;

import com.bicycle.service.vo.BicycleSystemMenuTreeVo;

import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/10
 * @Time: 23:36
 * Description:
 */
public interface BicycleSystemMenuService {

    /**
     * 根据角色主键获取菜单树
     *
     * @param roleId
     * @return
     */
    List<BicycleSystemMenuTreeVo> selectSystemMenuTreeByRole(Integer roleId);

    /**
     * 获取菜单树
     * @return
     */
    List<BicycleSystemMenuTreeVo> selectSystemMenuTree();
}
