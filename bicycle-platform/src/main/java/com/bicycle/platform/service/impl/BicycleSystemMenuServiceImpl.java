package com.bicycle.platform.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.enums.MenuTypeEnum;
import com.bicycle.service.vo.BicycleSystemMenuTreeVo;
import com.bicycle.platform.service.BicycleSystemMenuService;
import com.bicycle.service.entity.BicycleSystemMenu;
import com.bicycle.service.mapper.BicycleSystemMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/10
 * @Time: 23:36
 * Description:
 */
@Service("menuService")
public class BicycleSystemMenuServiceImpl implements BicycleSystemMenuService {

    @Autowired
    private BicycleSystemMenuMapper bicycleSystemMenuMapper;

    @Override
    public List<BicycleSystemMenuTreeVo> selectSystemMenuTreeByRole(Integer roleId) {
        // 一级菜单
        List<BicycleSystemMenuTreeVo> menuTreeResps = new ArrayList<>();
        List<BicycleSystemMenu> systemMenus = bicycleSystemMenuMapper.selectSystemMenuListByRole(roleId);
        // 二级菜单
        List<BicycleSystemMenuTreeVo> systemMenuTreeResps = new ArrayList<BicycleSystemMenuTreeVo>();
        for (BicycleSystemMenu bicycleSystemMenu : systemMenus) {
            BicycleSystemMenuTreeVo bicycleSystemMenuTreeVo = new BicycleSystemMenuTreeVo();
            if (bicycleSystemMenu.getMenuType() == MenuTypeEnum.MENU_TYPE_MENU.getKey().intValue()) {
                BeanUtil.copyProperties(bicycleSystemMenu, bicycleSystemMenuTreeVo);
                if (bicycleSystemMenu.getParentId() == null || bicycleSystemMenu.getParentId() == 0) {
                    menuTreeResps.add(bicycleSystemMenuTreeVo);
                    continue;
                }
            }
            BeanUtil.copyProperties(bicycleSystemMenu, bicycleSystemMenuTreeVo);
            systemMenuTreeResps.add(bicycleSystemMenuTreeVo);
        }
        // 循环一级菜单
        for (BicycleSystemMenuTreeVo bicycleSystemMenuTreeVo : menuTreeResps) {
            bicycleSystemMenuTreeVo.setChildValues(selectRecursiveMenuTree(bicycleSystemMenuTreeVo.getMenuId(), systemMenuTreeResps));
        }
        return menuTreeResps;
    }

    @Override
    public List<BicycleSystemMenuTreeVo> selectSystemMenuTree() {
        // 一级菜单
        List<BicycleSystemMenuTreeVo> menuTreeResps = new ArrayList<BicycleSystemMenuTreeVo>();

        // 查询启用中的菜单
        BicycleSystemMenu qBicycleSystemMenu = new BicycleSystemMenu();
        qBicycleSystemMenu.setState(1);
        QueryWrapper<BicycleSystemMenu> queryWrapper = new QueryWrapper<BicycleSystemMenu>(qBicycleSystemMenu);
        queryWrapper.orderByAsc("sort");
        List<BicycleSystemMenu> systemMenus = this.bicycleSystemMenuMapper.selectList(queryWrapper);
        // 二级菜单
        List<BicycleSystemMenuTreeVo> systemMenuTreeResps = new ArrayList<>();
        for (BicycleSystemMenu bicycleSystemMenu : systemMenus) {
            BicycleSystemMenuTreeVo bicycleSystemMenuTreeVo = new BicycleSystemMenuTreeVo();
            if (bicycleSystemMenu.getMenuType() == MenuTypeEnum.MENU_TYPE_MENU.getKey().intValue()) {
                BeanUtil.copyProperties(bicycleSystemMenu, bicycleSystemMenuTreeVo);
                if (bicycleSystemMenu.getParentId() == null || bicycleSystemMenu.getParentId() == 0) {
                    menuTreeResps.add(bicycleSystemMenuTreeVo);
                    continue;
                }
            }
            BeanUtil.copyProperties(bicycleSystemMenu, bicycleSystemMenuTreeVo);
            systemMenuTreeResps.add(bicycleSystemMenuTreeVo);
        }
        // 循环一级菜单
        for (BicycleSystemMenuTreeVo bicycleSystemMenuTreeVo : menuTreeResps) {
            bicycleSystemMenuTreeVo.setChildValues(selectRecursiveMenuTree(bicycleSystemMenuTreeVo.getMenuId(), systemMenuTreeResps));
        }

        return menuTreeResps;
    }


    /**
     * 递归获取菜单信息
     *
     * @param parentId
     * @param systemMenuTreeResps
     * @return
     * @see
     */
    private List<BicycleSystemMenuTreeVo> selectRecursiveMenuTree(Integer parentId, List<BicycleSystemMenuTreeVo> systemMenuTreeResps) {
        List<BicycleSystemMenuTreeVo> childValues = new ArrayList<>();
        for (BicycleSystemMenuTreeVo systemMenuTreeResp : systemMenuTreeResps) {
            // 菜单
            if (systemMenuTreeResp.getParentId() != null && systemMenuTreeResp.getParentId().equals(parentId) && systemMenuTreeResp.getMenuType().intValue() == MenuTypeEnum.MENU_TYPE_MENU.getKey()) {
                childValues.add(systemMenuTreeResp);
            }
            if (systemMenuTreeResp.getParentId() != null && systemMenuTreeResp.getParentId().equals(parentId)
                    && systemMenuTreeResp.getMenuType().intValue() == MenuTypeEnum.MENU_TYPE_BUTTON.getKey()) {
                childValues.add(systemMenuTreeResp);
            }
        }
        for (BicycleSystemMenuTreeVo systemMenuTreeResp : childValues) {
            if (systemMenuTreeResp.getParentId() != null && systemMenuTreeResp.getParentId().equals(parentId)) {
                systemMenuTreeResp.setChildValues(selectRecursiveMenuTree(systemMenuTreeResp.getMenuId(), systemMenuTreeResps));
            }
        }
        return childValues;
    }
}
