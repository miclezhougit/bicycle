package com.bicycle.statis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author xuxueli 2018-10-28 00:38:13
 */
@SpringBootApplication
@ComponentScan("com.bicycle")
public class StatisApplication {
	static {
//		System.setProperty("java.security.auth.login.config", "d:/kafka_ssl/kafka_client_jaas.conf");
		System.setProperty("java.security.auth.login.config", "/app/kafka/kafka_ssl/kafka_client_jaas.conf");
	}

	public static void main(String[] args) {
        SpringApplication.run(StatisApplication.class, args);
	}

}
