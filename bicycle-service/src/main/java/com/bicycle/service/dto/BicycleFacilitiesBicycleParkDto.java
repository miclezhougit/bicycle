package com.bicycle.service.dto;

import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/16
 * @Time: 1:18
 * Description:
 */
@Data
public class BicycleFacilitiesBicycleParkDto {

    private Integer bicyclePark;
}
