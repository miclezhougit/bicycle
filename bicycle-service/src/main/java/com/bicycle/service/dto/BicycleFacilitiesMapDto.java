/*
 * 文件名：FacilitiesMapReq.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月4日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 设施一张图请求实体
 *
 * @author 焦凯旋
 * @version 2019年6月4日
 * @see BicycleFacilitiesMapDto
 * @since
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BicycleFacilitiesMapDto implements Serializable {

    /**
     * 意义，目的和功能，以及被用到的地方<br>
     */
    private static final long serialVersionUID = 1L;

    // 行政区主键
    private Integer district;

    // 街道主键
    private Integer street;

    // 数据类型 0：停放区，1：禁停区，2：锁桩站点，3：电子停放区
    @ApiModelProperty("区域属性：0：停放区，1：监控区, 2:电子围栏,3:禁停区")
    private Integer type;

}
