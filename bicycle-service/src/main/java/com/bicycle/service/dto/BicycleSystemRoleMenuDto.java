package com.bicycle.service.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class BicycleSystemRoleMenuDto implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 角色主键ID
	 */
	private Integer roleId;
	/**
	 * 菜单主键ID集合
	 */
	private List<Integer> menuIdList;
	/**
	 * 创建人
	 */
	private String createName;
	/**
	 * 创建人主键
	 */
	private Integer createBy;

	/**
	 * 创建时间
	 */
	private Date createTime;

}
