package com.bicycle.service.dto;

import com.bicycle.service.entity.BicycleSystemUser;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/10
 * @Time: 17:38
 * Description:
 */
@Data
public class BicycleSystemUserGridDto extends BicycleSystemUser {

    /**
     * 当前页
     **/
    private int page;
    /**
     * 每页条数
     **/
    private int rows;
}
