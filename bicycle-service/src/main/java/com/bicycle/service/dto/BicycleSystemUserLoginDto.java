package com.bicycle.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/10
 * @Time: 22:31
 * Description:
 */
@Data
public class BicycleSystemUserLoginDto {

    @ApiModelProperty(value = "用户账户")
    private String account;

    @ApiModelProperty(value = "登录密码")
    private String password;

    @ApiModelProperty(value = "验证码")
    private String code;
}
