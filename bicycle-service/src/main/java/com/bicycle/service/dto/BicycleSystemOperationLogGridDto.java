package com.bicycle.service.dto;

import com.bicycle.service.entity.BicycleSystemOperationLog;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/10
 * @Time: 22:20
 * Description:
 */
@Data
public class BicycleSystemOperationLogGridDto extends BicycleSystemOperationLog {

    /**
     * 当前页
     **/
    private int page;
    /**
     * 每页条数
     **/
    private int rows;
}
