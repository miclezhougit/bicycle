package com.bicycle.service.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author : layne
 * @Date: 2020/8/15
 * @Time: 15:44
 * Description:
 */
@Data
public class BicycleStatisGlobalDto {

    @ApiModelProperty(value = "统计类型：1：日期，2：小时")
    private Integer statisType;

    @ApiModelProperty(value = "企业编号")
    private String enterpriseNo;

    @ApiModelProperty(value = "开始日期",example = "2020-08-18")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date beginDate;

    @ApiModelProperty(value = "结束日期",example = "2020-08-18")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date endDate;

    @ApiModelProperty(value = "请求类型：1-实时，2-违停，3-违投")
    private Integer dataType;


}
