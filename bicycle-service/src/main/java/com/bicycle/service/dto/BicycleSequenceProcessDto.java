/*
 * 文件名：BicycleSequenceProcessDto.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月14日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.service.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class BicycleSequenceProcessDto implements Serializable {

	/**
	 * 意义，目的和功能，以及被用到的地方<br>
	 */
	private static final long serialVersionUID = 1L;

	private Integer detailId;

	/**
	 * 操作类型：1：结案，2：退回
	 */
	private Integer operateType;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 企业编号
	 */
	private String enterpriseNo;

	/**
	 * 处理人员主键
	 */
	private Integer processUserId;

	/**
	 * 处理人员姓名
	 */
	private String processUserName;
}
