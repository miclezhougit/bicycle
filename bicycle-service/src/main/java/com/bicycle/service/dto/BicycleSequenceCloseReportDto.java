
package com.bicycle.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BicycleSequenceCloseReportDto implements Serializable {
	/**
	 * 意义，目的和功能，以及被用到的地方<br>
	 */
	private static final long serialVersionUID = 1L;

	private Integer[] detailIds;
	// 处理人员主键
	private Integer processUserId;

	// 处理人员姓名
	private String processUserName;

}
