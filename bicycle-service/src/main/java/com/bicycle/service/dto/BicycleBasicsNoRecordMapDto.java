
package com.bicycle.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * @Description: 未备案车辆信息
 * @Author: zhouzm
 * @Date: 2021/7/11 20:41
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BicycleBasicsNoRecordMapDto implements Serializable {

    /**
     * 意义，目的和功能，以及被用到的地方<br>
     */
    private static final long serialVersionUID = 1L;

    // 序号ID
    private Integer id;

    // 企业简称
    private String enterpriseName;

    // 车辆总数
    private Integer totalCount = 0;

}
