package com.bicycle.service.dto;

import lombok.Data;

import java.util.Date;

/**
 * 订单信息
 *
 */
@Data
public class OrderInfo {
	// 企业标识 统一分配
	private String comId;
	// 订单编号 用户订单编号
	private String orderNo;
	// 车辆编号 车辆编号
	private String bicycleNo;
	// 下单用户 用户唯一标示
	private String userId;
	// 下单时间 下单时间
	private Date startTime;
	// 下单经度 下单经度
	private String startLng;
	// 下单纬度 下单纬度
	private String startLat;
	// 还车时间 还车时间
	private Date endTime;
	// 结束经度 结束经度
	private String endLng;
	// 结束纬度 结束纬度
	private String endLat;
	// 订单里程 订单里程数
	private double orderMile;
	// 最后更新时间 订单最后更新时间
	private Date uploadTime;
}
