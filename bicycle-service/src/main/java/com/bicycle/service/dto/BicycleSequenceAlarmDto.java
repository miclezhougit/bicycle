/*
 * 文件名：SequenceAlarmReq.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年5月31日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BicycleSequenceAlarmDto implements Serializable {

	/**
	 * 意义，目的和功能，以及被用到的地方<br>
	 */
	private static final long serialVersionUID = 1L;

	// 举报来源：1：政府巡查，2：群众举报，3：监管平台
	private Integer reportResource;

	// 区域主键
	private Integer district;

	// 状态 0：未处理 1：已退回，2：已处理，3：已结案
	private Integer reportStatus;

	// 举报类型：1：违规投放，2：乱停乱放，3：区域超量，4：损坏车辆，5：其他类型
	private Integer reportType;

}
