/*
 * 文件名：BicycleFacilitiesGovAreaPageReq.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月3日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.service.dto;

import com.bicycle.service.entity.BicycleFacilitiesGovArea;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BicycleFacilitiesGovAreaGridDto extends BicycleFacilitiesGovArea {

    /**
     * 意义，目的和功能，以及被用到的地方<br>
     */
    private static final long serialVersionUID = 1L;

    /**
     * 时间段筛选开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;
    /**
     * 时间段筛选结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 当前页
     **/
    private int page;
    /**
     * 每页条数
     **/
    private int rows;

    /**
     * 访问类型：0-获取车筐内数据，1-获取车框外数据
     **/
    private Integer dataType;

}
