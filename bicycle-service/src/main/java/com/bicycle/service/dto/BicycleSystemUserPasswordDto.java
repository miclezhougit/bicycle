package com.bicycle.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/11
 * @Time: 0:30
 * Description:
 */
@Data
public class BicycleSystemUserPasswordDto {

    @ApiModelProperty("主键")
    private Integer sysUserId;

    @ApiModelProperty("登录密码")
    private String password;

    @ApiModelProperty("登录旧密码")
    private String oldPassword;

    @ApiModelProperty(value = "修改人")
    private String updateName;

    @ApiModelProperty(value = "修改人主键")
    private Integer updateBy;
}
