package com.bicycle.service.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 行业总览统计请求公共类
 * 
 * @author 程昂
 * @version 2019年7月3日
 * @see BicycleStatisCommonDto
 * @since
 */
@Data
public class BicycleStatisCommonDto implements java.io.Serializable {

	/**
	 * 意义，目的和功能，以及被用到的地方<br>
	 */
	private static final long serialVersionUID = -6092385787102957968L;
	// 区域类型 0 行政区 1 街道
	private Integer regionType;
	// 行政区ID regionType=1时，必传
	private Integer parentId = 0;
	// 区域ID
	private Integer regionId;
	// 企业编号
	private String enterpriseNo;
	// 间隔天数
	private Integer intervalDay = 0;
	// 间隔小时
	private Integer intervalHour = 1;
	// 开始时间
	private Date beginDate;
	// 结束时间
	private Date endDate;
	// 区域类型：0：停放区，1：禁停区，2：规范停放区，3：动态调节区，4：临时管控区
	private Integer areaType;
	// 开始时间字符串
	private String beginDateStr;
	// 结束时间字符串
	private String endDateStr;
	// 行政区
	private Integer districtId;
	// 街道
	private Integer streetId;
	// 区域类型数组
	private List<Integer> areaTypeList;
}
