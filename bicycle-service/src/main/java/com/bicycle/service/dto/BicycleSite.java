package com.bicycle.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 车辆位置上报信息
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BicycleSite implements java.io.Serializable {
    /**
     * 意义，目的和功能，以及被用到的地方<br>
     */
    private static final long serialVersionUID = 1L;
    // 企业标识 统一分配
    private String comId;
    // 车辆编号 车辆编号
    private String bicycleNo;
    //所在区编号
    private String districtCode;
    // 定位时间 定位时间
    private Date gnssTime;
    // 车辆坐标经度 车辆位置经度
    private String lng;
    // 车辆坐标纬度 车辆位置纬度
    private String lat;
    // 车锁状态 0：开；1：关
    private int lockState;
    // 车辆备案状态： 0：未备案，1：已备案，2：已回收
    private int putOnState;
    // 车辆是否违停：0：否，1：是
    private int hasIllegalPark;

}
