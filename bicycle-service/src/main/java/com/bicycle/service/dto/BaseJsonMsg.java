package com.bicycle.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Json实体公共类
 */
@Data
public class BaseJsonMsg implements Serializable {

    private static final long serialVersionUID = 1L;
    // 数据类型
    private String type;
    // 数据体
    private Object jsonStr;
}