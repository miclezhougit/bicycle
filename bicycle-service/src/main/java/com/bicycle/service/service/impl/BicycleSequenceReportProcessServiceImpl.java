package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleSequenceReportProcess;
import com.bicycle.service.mapper.BicycleSequenceReportProcessMapper;
import com.bicycle.service.service.IBicycleSequenceReportProcessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 举报处理记录表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
@Service
public class BicycleSequenceReportProcessServiceImpl extends ServiceImpl<BicycleSequenceReportProcessMapper, BicycleSequenceReportProcess> implements IBicycleSequenceReportProcessService {

}
