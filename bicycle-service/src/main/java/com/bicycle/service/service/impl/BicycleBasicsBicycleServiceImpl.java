package com.bicycle.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.service.dto.BicycleBasicsBicycleMapDto;
import com.bicycle.service.dto.BicycleBasicsNoRecordMapDto;
import com.bicycle.service.entity.BicycleBasicsBicycle;
import com.bicycle.service.mapper.BicycleBasicsBicycleMapper;
import com.bicycle.service.service.IBicycleBasicsBicycleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车辆基础信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-08
 */
@Service
public class BicycleBasicsBicycleServiceImpl extends ServiceImpl<BicycleBasicsBicycleMapper, BicycleBasicsBicycle> implements IBicycleBasicsBicycleService {

    @Override
    public List<BicycleBasicsBicycleMapDto> selectBicyclePutOnCount(Map<String, Object> paramMap) {
        return this.getBaseMapper().selectBicyclePutOnCount(paramMap);
    }
}
