package com.bicycle.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bicycle.service.dto.BicycleBasicsNoRecordMapDto;
import com.bicycle.service.entity.BicycleBasicsNoRecord;

import java.util.List;

/**
 * <p>
 * 未备案车辆信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-08
 */
public interface IBicycleBasicsNoRecordService extends IService<BicycleBasicsNoRecord> {

    List<BicycleBasicsNoRecordMapDto> selectBicycleNoRecordCount();
}
