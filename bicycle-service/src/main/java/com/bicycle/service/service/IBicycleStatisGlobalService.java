package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleStatisGlobal;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bicycle.service.vo.BicycleStatisGlobalVo;

import java.util.List;

/**
 * <p>
 * 全量数据统计表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface IBicycleStatisGlobalService extends IService<BicycleStatisGlobal> {

    void statis();

    void statisDay(String param);

    void statisHour(String param);

    List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> dealBicycleStatisGlobal(List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList);
}
