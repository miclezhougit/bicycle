package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleDataBicycleSite;
import com.bicycle.service.mapper.BicycleDataBicycleSiteMapper;
import com.bicycle.service.service.IBicycleDataBicycleSiteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 车辆位置表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
@Service
public class BicycleDataBicycleSiteServiceImpl extends ServiceImpl<BicycleDataBicycleSiteMapper, BicycleDataBicycleSite> implements IBicycleDataBicycleSiteService {

}
