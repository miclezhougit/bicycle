package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleSystemOperationLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统日志信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface IBicycleSystemOperationLogService extends IService<BicycleSystemOperationLog> {

}
