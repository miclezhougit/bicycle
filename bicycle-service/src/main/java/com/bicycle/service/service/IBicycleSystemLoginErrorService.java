package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleSystemLoginError;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统用户登录错误表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface IBicycleSystemLoginErrorService extends IService<BicycleSystemLoginError> {

}
