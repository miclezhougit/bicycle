package com.bicycle.service.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleFacilitiesEnterpriseArea;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业停放区域信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface IBicycleFacilitiesEnterpriseAreaService extends IService<BicycleFacilitiesEnterpriseArea> {


    Message selectFacilitiesMapChart();
}
