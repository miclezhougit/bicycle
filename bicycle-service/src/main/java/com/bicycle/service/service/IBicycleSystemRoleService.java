package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleSystemRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface IBicycleSystemRoleService extends IService<BicycleSystemRole> {

}
