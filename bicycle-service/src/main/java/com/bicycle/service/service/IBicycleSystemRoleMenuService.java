package com.bicycle.service.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.dto.BicycleSystemRoleMenuDto;
import com.bicycle.service.entity.BicycleSystemRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface IBicycleSystemRoleMenuService extends IService<BicycleSystemRoleMenu> {

    Message insertBicycleSystemRoleMenu(BicycleSystemRoleMenuDto bicycleSystemRoleMenuDto);
}
