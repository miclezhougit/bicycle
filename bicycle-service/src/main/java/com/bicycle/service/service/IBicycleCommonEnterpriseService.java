package com.bicycle.service.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业基础信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface IBicycleCommonEnterpriseService extends IService<BicycleCommonEnterprise> {

    Message insertEnterprise(BicycleCommonEnterprise bicycleCommonEnterprise);
}
