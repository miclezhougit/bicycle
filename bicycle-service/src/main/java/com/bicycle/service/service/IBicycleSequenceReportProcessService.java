package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleSequenceReportProcess;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 举报处理记录表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface IBicycleSequenceReportProcessService extends IService<BicycleSequenceReportProcess> {

}
