package com.bicycle.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.CommonStatusCodeEnum;
import com.bicycle.common.enums.HasDelEnum;
import com.bicycle.common.enums.SequenceBusinessCodeEnum;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.service.entity.BicycleSequenceUser;
import com.bicycle.service.mapper.BicycleCommonEnterpriseMapper;
import com.bicycle.service.mapper.BicycleCommonRegionMapper;
import com.bicycle.service.mapper.BicycleSequenceUserMapper;
import com.bicycle.service.service.IBicycleSequenceUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * 移动端用户信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class BicycleSequenceUserServiceImpl extends ServiceImpl<BicycleSequenceUserMapper, BicycleSequenceUser> implements IBicycleSequenceUserService {

    @Autowired
    private BicycleCommonRegionMapper regionMapper;
    @Autowired
    private BicycleCommonEnterpriseMapper enterpriseMapper;

    @Override
    public Message insertSequenceProcessUser(BicycleSequenceUser bicycleSequenceUser) {
        Message message = new Message();

        BicycleSequenceUser qBicycleSequenceUser = new BicycleSequenceUser();
        qBicycleSequenceUser.setMobile(bicycleSequenceUser.getMobile());
        qBicycleSequenceUser.setUserType(bicycleSequenceUser.getUserType());
        BicycleSequenceUser sequenceProcessUser = this.getOne(new QueryWrapper<BicycleSequenceUser>(qBicycleSequenceUser));
        if (sequenceProcessUser != null) {
            // 信息已存在
            message.setCode(SequenceBusinessCodeEnum.CODE_MOBILE_ALREADY_EXISTS.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_MOBILE_ALREADY_EXISTS.getValue());
            return message;
        }

        if (bicycleSequenceUser.getDistrictId() != null) {
            bicycleSequenceUser
                    .setDistrictName(regionMapper.selectById(bicycleSequenceUser.getDistrictId()).getRegionName());
        }
        if (bicycleSequenceUser.getStreetId() != null) {
            bicycleSequenceUser.setStreetName(regionMapper.selectById(bicycleSequenceUser.getStreetId()).getRegionName());
        }
        if (StringUtils.isNotBlank(bicycleSequenceUser.getEnterpriseNo())) {
            bicycleSequenceUser
                    .setEnterpriseName(enterpriseMapper.selectOne(new QueryWrapper<BicycleCommonEnterprise>(BicycleCommonEnterprise.builder().enterpriseNo(bicycleSequenceUser.getEnterpriseNo()).build())).getEnterpriseAlias());
        }
        bicycleSequenceUser.setCreateTime(new Date());
        this.save(bicycleSequenceUser);

        return message;
    }

    @Override
    public Message updateSequenceProcessUser(BicycleSequenceUser bicycleSequenceUser) {
        Message message = new Message();

        BicycleSequenceUser qBicycleSequenceUser = new BicycleSequenceUser();
        qBicycleSequenceUser.setMobile(bicycleSequenceUser.getMobile());
        qBicycleSequenceUser.setUserType(bicycleSequenceUser.getUserType());
        qBicycleSequenceUser.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        BicycleSequenceUser sequenceProcessUser = this.getOne(new QueryWrapper<BicycleSequenceUser>(qBicycleSequenceUser));
        if (sequenceProcessUser != null && sequenceProcessUser.getUserId().intValue() != bicycleSequenceUser.getUserId()) {
            // 信息已存在
            message.setCode(SequenceBusinessCodeEnum.CODE_MOBILE_ALREADY_EXISTS.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_MOBILE_ALREADY_EXISTS.getValue());
            return message;
        }

        BicycleSequenceUser processUser = this.getById(bicycleSequenceUser.getUserId());

        processUser.setUpdateTime(new Date());
        processUser.setUpdateBy(bicycleSequenceUser.getUpdateBy());
        processUser.setUpdateName(bicycleSequenceUser.getUpdateName());
        processUser.setDistrictId(bicycleSequenceUser.getDistrictId());
        processUser.setStreetId(bicycleSequenceUser.getStreetId());
        processUser.setEnterpriseNo(bicycleSequenceUser.getEnterpriseNo());
        processUser.setMobile(bicycleSequenceUser.getMobile());
        processUser.setUserName(bicycleSequenceUser.getUserName());
        processUser.setUserStatus(bicycleSequenceUser.getUserStatus());
        if (StringUtils.isNotBlank(bicycleSequenceUser.getLoginPassword())) {
            processUser.setLoginPassword(bicycleSequenceUser.getLoginPassword());
        }
        if (processUser.getDistrictId() != null) {
            processUser.setDistrictName(regionMapper.selectById(processUser.getDistrictId()).getRegionName());
        } else {
            processUser.setDistrictName(null);
        }
        if (processUser.getStreetId() != null) {
            processUser.setStreetName(regionMapper.selectById(processUser.getStreetId()).getRegionName());
        } else {
            processUser.setStreetName(null);
        }
        if (StringUtils.isNotBlank(processUser.getEnterpriseNo())) {
            processUser.setEnterpriseName(enterpriseMapper.selectOne(new QueryWrapper<BicycleCommonEnterprise>(BicycleCommonEnterprise.builder().enterpriseNo(bicycleSequenceUser.getEnterpriseNo()).build())).getEnterpriseAlias());
        } else {
            processUser.setEnterpriseName(null);
        }
        BicycleSequenceUser qProcessUser = new BicycleSequenceUser();
        qProcessUser.setUserId(bicycleSequenceUser.getUserId());
        this.update(processUser, new QueryWrapper<BicycleSequenceUser>(qProcessUser));
        return message;
    }
}
