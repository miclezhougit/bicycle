package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleSystemUser;
import com.bicycle.service.mapper.BicycleSystemUserMapper;
import com.bicycle.service.service.IBicycleSystemUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
@Service
public class BicycleSystemUserServiceImpl extends ServiceImpl<BicycleSystemUserMapper, BicycleSystemUser> implements IBicycleSystemUserService {

}
