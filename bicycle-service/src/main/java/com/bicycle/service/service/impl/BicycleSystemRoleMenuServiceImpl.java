package com.bicycle.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.CommonStatusCodeEnum;
import com.bicycle.service.dto.BicycleSystemRoleMenuDto;
import com.bicycle.service.entity.BicycleSystemRoleMenu;
import com.bicycle.service.mapper.BicycleSystemRoleMenuMapper;
import com.bicycle.service.service.IBicycleSystemRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
@Service
public class BicycleSystemRoleMenuServiceImpl extends ServiceImpl<BicycleSystemRoleMenuMapper, BicycleSystemRoleMenu> implements IBicycleSystemRoleMenuService {

    @Override
    public Message insertBicycleSystemRoleMenu(BicycleSystemRoleMenuDto bicycleSystemRoleMenuDto) {
        Message message = new Message();

        // 清除旧的关联信息
        BicycleSystemRoleMenu qBicycleSystemRoleMenu = new BicycleSystemRoleMenu();
        qBicycleSystemRoleMenu.setRoleId(bicycleSystemRoleMenuDto.getRoleId());
        this.remove(new QueryWrapper<BicycleSystemRoleMenu>(qBicycleSystemRoleMenu));

        // 遍历插入关联信息
        bicycleSystemRoleMenuDto.getMenuIdList().forEach(menuId -> {
            BicycleSystemRoleMenu bicycleSystemRoleMenu = new BicycleSystemRoleMenu();
            bicycleSystemRoleMenu.setCreateBy(bicycleSystemRoleMenuDto.getCreateBy());
            bicycleSystemRoleMenu.setCreateName(bicycleSystemRoleMenuDto.getCreateName());
            bicycleSystemRoleMenu.setRoleId(bicycleSystemRoleMenuDto.getRoleId());
            bicycleSystemRoleMenu.setMenuId(menuId);
            this.save(bicycleSystemRoleMenu);
        });

        return message;
    }
}
