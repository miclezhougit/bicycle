package com.bicycle.service.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.dto.BicycleSequenceCloseReportDto;
import com.bicycle.service.dto.BicycleSequenceProcessDto;
import com.bicycle.service.entity.BicycleSequenceReportDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 举报记录详情表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface IBicycleSequenceReportDetailService extends IService<BicycleSequenceReportDetail> {


    Message processCase(BicycleSequenceProcessDto bicycleSequenceProcessDto);

    Message closeCase(BicycleSequenceCloseReportDto bicycleSequenceCloseReportDto);
}
