package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleCommonFile;
import com.bicycle.service.mapper.BicycleCommonFileMapper;
import com.bicycle.service.service.IBicycleCommonFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 文件信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
@Service
public class BicycleCommonFileServiceImpl extends ServiceImpl<BicycleCommonFileMapper, BicycleCommonFile> implements IBicycleCommonFileService {

}
