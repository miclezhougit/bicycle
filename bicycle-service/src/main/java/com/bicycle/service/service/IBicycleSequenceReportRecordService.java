package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleSequenceReportRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公众号用户举报记录表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface IBicycleSequenceReportRecordService extends IService<BicycleSequenceReportRecord> {

}
