package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleSystemMenu;
import com.bicycle.service.mapper.BicycleSystemMenuMapper;
import com.bicycle.service.service.IBicycleSystemMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
@Service
public class BicycleSystemMenuServiceImpl extends ServiceImpl<BicycleSystemMenuMapper, BicycleSystemMenu> implements IBicycleSystemMenuService {

}
