package com.bicycle.service.service.impl;

import com.bicycle.service.dto.BicycleBasicsAreaVehicleDto;
import com.bicycle.service.entity.BicycleBasicsAreaVehicle;
import com.bicycle.service.mapper.BicycleBasicsAreaVehicleMapper;
import com.bicycle.service.service.IBicycleBasicsAreaVehicleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 车辆区域关联表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-29
 */
@Service
public class BicycleBasicsAreaVehicleServiceImpl extends ServiceImpl<BicycleBasicsAreaVehicleMapper, BicycleBasicsAreaVehicle> implements IBicycleBasicsAreaVehicleService {

    @Override
    public List<BicycleBasicsAreaVehicleDto> selectAreaVehicleByAreaId(Integer area_id) {
        return this.getBaseMapper().selectAreaVehicleByAreaId(area_id);
    }

}
