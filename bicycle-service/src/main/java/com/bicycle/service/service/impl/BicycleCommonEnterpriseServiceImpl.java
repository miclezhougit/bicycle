package com.bicycle.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.CommonBusinessCodeEnum;
import com.bicycle.common.enums.CommonStatusCodeEnum;
import com.bicycle.common.enums.EnterpriseStatusEnum;
import com.bicycle.common.helper.RedisStringHelper;
import com.bicycle.service.cache.DataCacheOprator;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.service.mapper.BicycleCommonEnterpriseMapper;
import com.bicycle.service.service.IBicycleCommonEnterpriseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 企业基础信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
@Service
public class BicycleCommonEnterpriseServiceImpl extends ServiceImpl<BicycleCommonEnterpriseMapper, BicycleCommonEnterprise> implements IBicycleCommonEnterpriseService {


    @Autowired
    private RedisStringHelper redisStringHelper;

    @Override
    public Message insertEnterprise(BicycleCommonEnterprise bicycleCommonEnterprise) {
        Message message = new Message();

        BicycleCommonEnterprise qBicycleCommonEnterprise = new BicycleCommonEnterprise();
        qBicycleCommonEnterprise.setEnterpriseNo(bicycleCommonEnterprise.getEnterpriseNo());
        BicycleCommonEnterprise commonEnterprise = getOne(new QueryWrapper<BicycleCommonEnterprise>(qBicycleCommonEnterprise));

        if (commonEnterprise != null) {
            // 企业信息已存在
            message.setCode(CommonBusinessCodeEnum.CODE_INFO_ALREADY_EXISTS.getKey());
            message.setMessage(CommonBusinessCodeEnum.CODE_INFO_ALREADY_EXISTS.getValue());
            return message;
        }
        qBicycleCommonEnterprise.setEnterpriseAlias(bicycleCommonEnterprise.getEnterpriseAlias());
        commonEnterprise = getOne(new QueryWrapper<BicycleCommonEnterprise>(qBicycleCommonEnterprise));

        if (commonEnterprise != null) {
            // 企业信息已存在
            message.setCode(CommonBusinessCodeEnum.CODE_INFO_ALREADY_EXISTS.getKey());
            message.setMessage(CommonBusinessCodeEnum.CODE_INFO_ALREADY_EXISTS.getValue());
            return message;
        }
        save(bicycleCommonEnterprise);

        // 重新加载缓存
        BicycleCommonEnterprise qCommonEnterprise = new BicycleCommonEnterprise();
        qCommonEnterprise.setEnterpriseStatus(EnterpriseStatusEnum.ENTERPRISE_STATUS_0.getKey());
        List<BicycleCommonEnterprise> enterpriseList = list(new QueryWrapper<BicycleCommonEnterprise>(qCommonEnterprise));

        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        DataCacheOprator.getInstance().reloadEnterprise(enterpriseList);
        qCommonEnterprise.setEnterpriseStatus(null);
        enterpriseList = list(new QueryWrapper<BicycleCommonEnterprise>(qCommonEnterprise));
        DataCacheOprator.getInstance().reloadAllEnterprise(enterpriseList);

        return message;
    }
}
