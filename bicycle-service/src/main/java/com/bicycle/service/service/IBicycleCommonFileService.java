package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleCommonFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 文件信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface IBicycleCommonFileService extends IService<BicycleCommonFile> {

}
