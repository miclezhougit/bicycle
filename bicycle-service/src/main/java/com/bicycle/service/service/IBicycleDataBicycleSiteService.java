package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleDataBicycleSite;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 车辆位置表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface IBicycleDataBicycleSiteService extends IService<BicycleDataBicycleSite> {

}
