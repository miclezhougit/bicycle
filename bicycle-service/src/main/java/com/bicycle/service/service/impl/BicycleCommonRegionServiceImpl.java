package com.bicycle.service.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.CommonBusinessCodeEnum;
import com.bicycle.common.enums.CommonStatusCodeEnum;
import com.bicycle.common.enums.RegionTypeEnum;
import com.bicycle.common.helper.CoordinateHelper;
import com.bicycle.service.entity.BicycleCommonRegion;
import com.bicycle.service.mapper.BicycleCommonRegionMapper;
import com.bicycle.service.service.IBicycleCommonRegionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bicycle.service.vo.BicycleCommonRegionTreeVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 行政街道信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
@Service
public class BicycleCommonRegionServiceImpl extends ServiceImpl<BicycleCommonRegionMapper, BicycleCommonRegion> implements IBicycleCommonRegionService {

    @Override
    public Message selectBicycleCommonRegionByPoint(String lat, String lng) {
        Message message = new Message();
        BicycleCommonRegionTreeVo bicycleCommonRegionTreeVo = new BicycleCommonRegionTreeVo();
        BicycleCommonRegion bicycleCommonRegion = null;

        Point2D.Double point = new Point2D.Double(Double.valueOf(lng), Double.valueOf(lat));
        // 先判断在哪个行政区
        List<BicycleCommonRegion> commonRegions = list(new QueryWrapper<BicycleCommonRegion>(BicycleCommonRegion.builder().regionType(RegionTypeEnum.REGION_TYPE_DISTRICT.getKey()).build()));
        Integer regionId = null, streetId = null;
        for (BicycleCommonRegion commonRegion : commonRegions) {
            if (StringUtils.isBlank(commonRegion.getRegionPoints()) || point == null) {
                continue;
            }
            JSONArray areaArray = JSON.parseArray(commonRegion.getRegionPoints());
            if (areaArray == null || areaArray.isEmpty()) {
                continue;
            }
            List<Point2D.Double> pointList = new ArrayList<Point2D.Double>();
            for (int j = 0; j < areaArray.size(); j++) {
                double lngd = Double.valueOf(areaArray.getJSONObject(j).getString("lng"));
                double latd = Double.valueOf(areaArray.getJSONObject(j).getString("lat"));
                pointList.add(new Point2D.Double(lngd, latd));
            }
            if (CoordinateHelper.IsPtInPoly(point, pointList)) {
                regionId = commonRegion.getId();
                break;
            }
        }
        if (regionId == null) {
            message.setCode(CommonBusinessCodeEnum.CODE_OUT_OF_RANGE.getKey());
            // message.setMessage(CommonBusinessCodeEnum.CODE_OUT_OF_RANGE.getValue());
            message.setMessage(null);
            return message;
        }

        bicycleCommonRegion = getById(regionId);

        BeanUtil.copyProperties(bicycleCommonRegion, bicycleCommonRegionTreeVo);
        // 判断在哪个街道
        commonRegions = list(new QueryWrapper<BicycleCommonRegion>(BicycleCommonRegion.builder().parentId(regionId).build()));
        for (BicycleCommonRegion commonRegion : commonRegions) {
            if (StringUtils.isBlank(commonRegion.getRegionPoints()) || point == null) {
                continue;
            }
            JSONArray areaArray = JSON.parseArray(commonRegion.getRegionPoints());
            if (areaArray == null || areaArray.isEmpty()) {
                continue;
            }
            List<Point2D.Double> pointList = new ArrayList<Point2D.Double>();
            for (int j = 0; j < areaArray.size(); j++) {
                double lngd = Double.valueOf(areaArray.getJSONObject(j).getString("lng"));
                double latd = Double.valueOf(areaArray.getJSONObject(j).getString("lat"));
                pointList.add(new Point2D.Double(lngd, latd));
            }
            if (CoordinateHelper.IsPtInPoly(point, pointList)) {
                streetId = commonRegion.getId();
                break;
            }
        }
        if (streetId != null) {
            bicycleCommonRegion = getById(streetId);
            bicycleCommonRegionTreeVo.setChild(bicycleCommonRegion);
        }
        message.setResult(bicycleCommonRegionTreeVo);

        return message;
    }
}
