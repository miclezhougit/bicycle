package com.bicycle.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.CommonStatusCodeEnum;
import com.bicycle.common.enums.EnterpriseAreaTypeEnum;
import com.bicycle.common.enums.HasDelEnum;
import com.bicycle.service.cache.DataCacheOprator;
import com.bicycle.service.cache.RegionRelation;
import com.bicycle.service.entity.BicycleFacilitiesEnterpriseArea;
import com.bicycle.service.mapper.BicycleFacilitiesEnterpriseAreaMapper;
import com.bicycle.service.service.IBicycleFacilitiesEnterpriseAreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bicycle.service.vo.BicycleFacilitiesMapChartVo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 企业停放区域信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
@Service
public class BicycleFacilitiesEnterpriseAreaServiceImpl extends ServiceImpl<BicycleFacilitiesEnterpriseAreaMapper, BicycleFacilitiesEnterpriseArea> implements IBicycleFacilitiesEnterpriseAreaService {

    @Override
    public Message selectFacilitiesMapChart() {
        Message message = new Message();

        BicycleFacilitiesMapChartVo bicycleFacilitiesMapChartVo = new BicycleFacilitiesMapChartVo();

        BicycleFacilitiesEnterpriseArea qFacilitiesEnterpriseArea = new BicycleFacilitiesEnterpriseArea();
        qFacilitiesEnterpriseArea.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());

        // 锁桩站点数量
        qFacilitiesEnterpriseArea.setAreaType(EnterpriseAreaTypeEnum.AREA_TYPE_0.getKey());
        int siteCount = count(new QueryWrapper<BicycleFacilitiesEnterpriseArea>(qFacilitiesEnterpriseArea));

        bicycleFacilitiesMapChartVo.setSiteCount(siteCount);
        // 围栏数量
        qFacilitiesEnterpriseArea.setAreaType(EnterpriseAreaTypeEnum.AREA_TYPE_1.getKey());
        int fenceCount = count(new QueryWrapper<BicycleFacilitiesEnterpriseArea>(qFacilitiesEnterpriseArea));

        bicycleFacilitiesMapChartVo.setFenceCount(fenceCount);
        qFacilitiesEnterpriseArea.setAreaType(EnterpriseAreaTypeEnum.AREA_TYPE_0.getKey());
        List<BicycleFacilitiesEnterpriseArea> facilitiesEnterpriseAreas = list(new QueryWrapper<BicycleFacilitiesEnterpriseArea>(qFacilitiesEnterpriseArea));
        // 车桩数量
        int rackCount = 0;
        for (BicycleFacilitiesEnterpriseArea facilitiesEnterpriseArea : facilitiesEnterpriseAreas) {
            rackCount += facilitiesEnterpriseArea.getVolume();
        }
        bicycleFacilitiesMapChartVo.setRackCount(rackCount);

        message.setResult(bicycleFacilitiesMapChartVo);

        return message;
    }
}
