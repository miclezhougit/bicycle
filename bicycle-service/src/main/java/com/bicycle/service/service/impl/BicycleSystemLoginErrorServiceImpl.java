package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleSystemLoginError;
import com.bicycle.service.mapper.BicycleSystemLoginErrorMapper;
import com.bicycle.service.service.IBicycleSystemLoginErrorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统用户登录错误表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
@Service
public class BicycleSystemLoginErrorServiceImpl extends ServiceImpl<BicycleSystemLoginErrorMapper, BicycleSystemLoginError> implements IBicycleSystemLoginErrorService {

}
