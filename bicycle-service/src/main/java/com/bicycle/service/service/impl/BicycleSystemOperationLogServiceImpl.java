package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleSystemOperationLog;
import com.bicycle.service.mapper.BicycleSystemOperationLogMapper;
import com.bicycle.service.service.IBicycleSystemOperationLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统日志信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
@Service
public class BicycleSystemOperationLogServiceImpl extends ServiceImpl<BicycleSystemOperationLogMapper, BicycleSystemOperationLog> implements IBicycleSystemOperationLogService {

}
