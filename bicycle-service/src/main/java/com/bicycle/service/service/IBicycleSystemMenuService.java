package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleSystemMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface IBicycleSystemMenuService extends IService<BicycleSystemMenu> {


}
