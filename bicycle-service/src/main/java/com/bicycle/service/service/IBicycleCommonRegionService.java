package com.bicycle.service.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleCommonRegion;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 行政街道信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface IBicycleCommonRegionService extends IService<BicycleCommonRegion> {

    Message selectBicycleCommonRegionByPoint(String lat, String lng);
}
