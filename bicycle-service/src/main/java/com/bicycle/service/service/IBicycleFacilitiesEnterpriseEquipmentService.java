package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleFacilitiesEnterpriseEquipment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业智能设施信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface IBicycleFacilitiesEnterpriseEquipmentService extends IService<BicycleFacilitiesEnterpriseEquipment> {

}
