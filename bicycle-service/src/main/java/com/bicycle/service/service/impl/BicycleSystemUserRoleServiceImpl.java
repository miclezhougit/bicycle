package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleSystemUserRole;
import com.bicycle.service.mapper.BicycleSystemUserRoleMapper;
import com.bicycle.service.service.IBicycleSystemUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
@Service
public class BicycleSystemUserRoleServiceImpl extends ServiceImpl<BicycleSystemUserRoleMapper, BicycleSystemUserRole> implements IBicycleSystemUserRoleService {

}
