package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleSystemUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface IBicycleSystemUserRoleService extends IService<BicycleSystemUserRole> {

}
