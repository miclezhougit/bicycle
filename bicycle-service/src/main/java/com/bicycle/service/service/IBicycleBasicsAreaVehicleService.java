package com.bicycle.service.service;

import com.bicycle.service.dto.BicycleBasicsAreaVehicleDto;
import com.bicycle.service.entity.BicycleBasicsAreaVehicle;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 车辆区域关联表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-29
 */
public interface IBicycleBasicsAreaVehicleService extends IService<BicycleBasicsAreaVehicle> {

    List<BicycleBasicsAreaVehicleDto> selectAreaVehicleByAreaId(Integer area_id);

}
