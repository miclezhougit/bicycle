package com.bicycle.service.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleCommonFile;
import com.bicycle.service.entity.BicycleFacilitiesGovArea;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bicycle.service.vo.BicycleFacilitiesGovAreaGridVo;
import com.bicycle.service.vo.BicycleSystemUserLoginVo;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.util.List;

/**
 * <p>
 * 政府停放区域信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-08
 */
public interface IBicycleFacilitiesGovAreaService extends IService<BicycleFacilitiesGovArea> {

    /**
     * 根据主键修改信息
     *
     * @param bicycleFacilitiesGovArea
     * @return
     */
    Message updateFacilitiesGovAreaByPrimaryKey(BicycleFacilitiesGovArea bicycleFacilitiesGovArea);

    /**
     * 根据主键删除信息
     *
     * @param id
     * @return
     */
    Message deleteFacilitiesGovAreaByPrimaryKey(Integer id);

    /**
     * 新增区域信息
     *
     * @param bicycleFacilitiesGovArea
     * @return
     */
    Message insertFacilitiesGovAreaSelective(BicycleFacilitiesGovArea bicycleFacilitiesGovArea);

    /**
     * 获取parkvalue sum
     *
     * @return
     */
    Integer sumParkValue();

    /**
     * 批量导入停放区车辆位置信息
     *
     * @param file
     * @return
     */
    Message importFacilitiesGovAreaFile(File file, BicycleSystemUserLoginVo userLoginResp);

    /**
     * 导出停放区车辆位置信息
     *
     * @param sheetName
     * @param title
     * @param values
     * @param wb
     * @return
     */
    HSSFWorkbook exportFacilitiesGovAreaData(String sheetName,String []title, String [][]values, HSSFWorkbook wb);


}
