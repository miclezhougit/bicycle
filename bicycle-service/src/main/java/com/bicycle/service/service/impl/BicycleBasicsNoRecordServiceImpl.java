package com.bicycle.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bicycle.service.dto.BicycleBasicsNoRecordMapDto;
import com.bicycle.service.entity.BicycleBasicsNoRecord;
import com.bicycle.service.mapper.BicycleBasicsNoRecordMapper;
import com.bicycle.service.service.IBicycleBasicsNoRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 未备案车辆信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-08
 */
@Service
public class BicycleBasicsNoRecordServiceImpl extends ServiceImpl<BicycleBasicsNoRecordMapper, BicycleBasicsNoRecord> implements IBicycleBasicsNoRecordService {



    @Override
    public List<BicycleBasicsNoRecordMapDto> selectBicycleNoRecordCount() {
        return this.getBaseMapper().selectBicycleNoRecordCount();
    }

}
