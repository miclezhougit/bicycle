package com.bicycle.service.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleSequenceUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 移动端用户信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface IBicycleSequenceUserService extends IService<BicycleSequenceUser> {

    /**
     * 新增移动端用户
     *
     * @param bicycleSequenceUser
     * @return
     */
    Message insertSequenceProcessUser(BicycleSequenceUser bicycleSequenceUser);

    /**
     * 修改移动端用户
     *
     * @param bicycleSequenceUser
     * @return
     */
    Message updateSequenceProcessUser(BicycleSequenceUser bicycleSequenceUser);
}
