package com.bicycle.service.service.impl;

import com.bicycle.service.mapper.BicycleCreateTableMapper;
import com.bicycle.service.service.IBicycleCreateTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author : layne
 * @Date: 2020/8/22
 * @Time: 1:29
 * Description:
 */
@Service
public class BicycleCreateTableServiceImpl implements IBicycleCreateTableService {

    @Autowired
    private BicycleCreateTableMapper bicycleCreateTableMapper;

    @Override
    public void createOrderTable(String tableName) {
        bicycleCreateTableMapper.createOrderTable(tableName);
    }

    @Override
    public void createSiteTable(String tableName) {
        bicycleCreateTableMapper.createSiteTable(tableName);
    }
}
