package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleSequenceReportPhoto;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 举报记录详情图片表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface IBicycleSequenceReportPhotoService extends IService<BicycleSequenceReportPhoto> {

}
