package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleSequenceReportRecord;
import com.bicycle.service.mapper.BicycleSequenceReportRecordMapper;
import com.bicycle.service.service.IBicycleSequenceReportRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公众号用户举报记录表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
@Service
public class BicycleSequenceReportRecordServiceImpl extends ServiceImpl<BicycleSequenceReportRecordMapper, BicycleSequenceReportRecord> implements IBicycleSequenceReportRecordService {

}
