package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleSystemRole;
import com.bicycle.service.mapper.BicycleSystemRoleMapper;
import com.bicycle.service.service.IBicycleSystemRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
@Service
public class BicycleSystemRoleServiceImpl extends ServiceImpl<BicycleSystemRoleMapper, BicycleSystemRole> implements IBicycleSystemRoleService {

}
