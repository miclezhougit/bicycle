package com.bicycle.service.service;

import com.bicycle.service.dto.BicycleBasicsBicycleMapDto;
import com.bicycle.service.entity.BicycleBasicsBicycle;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车辆基础信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-08
 */
public interface IBicycleBasicsBicycleService extends IService<BicycleBasicsBicycle> {

    List<BicycleBasicsBicycleMapDto> selectBicyclePutOnCount(Map<String, Object> paramMap);
}
