package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleSequenceReportPhoto;
import com.bicycle.service.mapper.BicycleSequenceReportPhotoMapper;
import com.bicycle.service.service.IBicycleSequenceReportPhotoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 举报记录详情图片表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
@Service
public class BicycleSequenceReportPhotoServiceImpl extends ServiceImpl<BicycleSequenceReportPhotoMapper, BicycleSequenceReportPhoto> implements IBicycleSequenceReportPhotoService {

}
