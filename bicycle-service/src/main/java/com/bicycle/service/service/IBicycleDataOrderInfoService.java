package com.bicycle.service.service;

import com.bicycle.service.entity.BicycleDataOrderInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单信息表 服务类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface IBicycleDataOrderInfoService extends IService<BicycleDataOrderInfo> {

}
