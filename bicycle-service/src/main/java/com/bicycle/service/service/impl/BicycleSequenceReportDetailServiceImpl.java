package com.bicycle.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.QueryChainWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.CommonStatusCodeEnum;
import com.bicycle.common.enums.ReportStatusEnum;
import com.bicycle.common.enums.SequenceBusinessCodeEnum;
import com.bicycle.service.dto.BicycleSequenceCloseReportDto;
import com.bicycle.service.dto.BicycleSequenceProcessDto;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.service.entity.BicycleSequenceReportDetail;
import com.bicycle.service.entity.BicycleSequenceReportProcess;
import com.bicycle.service.entity.BicycleSequenceReportRecord;
import com.bicycle.service.mapper.BicycleSequenceReportDetailMapper;
import com.bicycle.service.mapper.BicycleSequenceReportProcessMapper;
import com.bicycle.service.service.IBicycleCommonEnterpriseService;
import com.bicycle.service.service.IBicycleSequenceReportDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bicycle.service.service.IBicycleSequenceReportProcessService;
import com.bicycle.service.service.IBicycleSequenceReportRecordService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 举报记录详情表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
@Service
public class BicycleSequenceReportDetailServiceImpl extends ServiceImpl<BicycleSequenceReportDetailMapper, BicycleSequenceReportDetail> implements IBicycleSequenceReportDetailService {

    @Autowired
    private IBicycleSequenceReportProcessService sequenceReportProcessService;
    @Autowired
    private IBicycleSequenceReportRecordService sequenceReportRecordService;

    @Autowired
    private IBicycleCommonEnterpriseService commonEnterpriseService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Message processCase(BicycleSequenceProcessDto bicycleSequenceProcessDto) {
        Message message = new Message();

        BicycleSequenceReportDetail sequenceReportDetail =
                this.getById(bicycleSequenceProcessDto.getDetailId());
        // 案件未处理不能进行操作
        if (sequenceReportDetail.getReportStatus() != ReportStatusEnum.REPORT_STATUS_2.getKey()) {
            message.setCode(SequenceBusinessCodeEnum.CODE_CASE_UNDISPOSED.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_CASE_UNDISPOSED.getValue());
            return message;
        }
        // 结案处理
        if (bicycleSequenceProcessDto.getOperateType() == 1) {
            // 已结案信息不再进行处理
            if (sequenceReportDetail.getReportStatus() != ReportStatusEnum.REPORT_STATUS_3.getKey()) {
                // 已结案
                sequenceReportDetail.setReportStatus(ReportStatusEnum.REPORT_STATUS_3.getKey());
                sequenceReportDetail.setUpdateTime(new Date());
                sequenceReportDetail.setCloseTime(new Date());
                this.updateById(sequenceReportDetail);
                // 插入操作记录
                BicycleSequenceReportProcess sequenceDetailProcessRecord = new BicycleSequenceReportProcess();
                sequenceDetailProcessRecord.setCreateTime(new Date());
                sequenceDetailProcessRecord.setDetailId(sequenceReportDetail.getDetailId());
                sequenceDetailProcessRecord.setHasTrue(sequenceReportDetail.getHasTrue());
                sequenceDetailProcessRecord.setProcessTime(new Date());
                sequenceDetailProcessRecord.setProcessUserId(bicycleSequenceProcessDto.getProcessUserId());
                sequenceDetailProcessRecord.setProcessUserName(bicycleSequenceProcessDto.getProcessUserName());
                sequenceDetailProcessRecord.setOperateType(3);
                sequenceDetailProcessRecord.setRemark(bicycleSequenceProcessDto.getRemark());
                sequenceReportProcessService.save(sequenceDetailProcessRecord);
            }
            // 有举报关联属性
            if (sequenceReportDetail.getReportId() != null) {
                BicycleSequenceReportDetail qSequenceReportDetail = new BicycleSequenceReportDetail();
                qSequenceReportDetail.setReportId(sequenceReportDetail.getReportId());
                List<BicycleSequenceReportDetail> sequenceReportDetails =
                        this.list(new QueryWrapper<BicycleSequenceReportDetail>(qSequenceReportDetail));
                boolean isFinish = true;
                // 筛选是否所有企业处理完本条举报
                for (BicycleSequenceReportDetail reportDetail : sequenceReportDetails) {
                    if (reportDetail.getReportStatus().intValue() == ReportStatusEnum.REPORT_STATUS_3.getKey()) {
                        // 已结案
                        continue;
                    } else {
                        isFinish = false;
                        break;
                    }
                }
                if (isFinish) { // 是否所有企业都处理完毕
                    BicycleSequenceReportRecord reportRecord =
                            sequenceReportRecordService.getById(sequenceReportDetail.getReportId());
                    if (reportRecord != null) {
                        reportRecord.setReportStatus(1); // 已处理
                        sequenceReportRecordService.updateById(reportRecord);
                    }
                }
            }
        } else if (bicycleSequenceProcessDto.getOperateType() == 2) {
            // 推送处理
            // 同一企业推送
            if (StringUtils.equalsIgnoreCase(sequenceReportDetail.getEnterpriseNo(), bicycleSequenceProcessDto.getEnterpriseNo())) {
                // 已推送
                sequenceReportDetail.setReportStatus(ReportStatusEnum.REPORT_STATUS_1.getKey());
                sequenceReportDetail.setUpdateTime(new Date());
                BicycleSequenceReportDetail qBicycleSequenceReportDetail = new BicycleSequenceReportDetail();
                qBicycleSequenceReportDetail.setDetailId(sequenceReportDetail.getDetailId());

                this.update(sequenceReportDetail, new QueryWrapper<BicycleSequenceReportDetail>(qBicycleSequenceReportDetail));

                // 插入操作记录
                BicycleSequenceReportProcess sequenceDetailProcessRecord = new BicycleSequenceReportProcess();
                sequenceDetailProcessRecord.setCreateTime(new Date());
                sequenceDetailProcessRecord.setDetailId(bicycleSequenceProcessDto.getDetailId());
                sequenceDetailProcessRecord.setHasTrue(sequenceReportDetail.getHasTrue());
                sequenceDetailProcessRecord.setProcessTime(new Date());
                sequenceDetailProcessRecord.setProcessUserId(bicycleSequenceProcessDto.getProcessUserId());
                sequenceDetailProcessRecord.setProcessUserName(bicycleSequenceProcessDto.getProcessUserName());
                sequenceDetailProcessRecord.setProcessEnterpriseNo(sequenceReportDetail.getEnterpriseNo());
                sequenceDetailProcessRecord.setProcessEnterpriseName(sequenceReportDetail.getEnterpriseName());
                sequenceDetailProcessRecord.setPushEnterpriseNo(sequenceReportDetail.getEnterpriseNo());
                sequenceDetailProcessRecord.setPushEnterpriseName(sequenceReportDetail.getEnterpriseName());
                sequenceDetailProcessRecord.setRemark(bicycleSequenceProcessDto.getRemark());
                sequenceDetailProcessRecord.setOperateType(2);
                sequenceReportProcessService.save(sequenceDetailProcessRecord);
            } else {
                // 不同企业推送
                BicycleSequenceReportDetail wxReportDetail = this.getOne(new QueryWrapper<BicycleSequenceReportDetail>(
                        BicycleSequenceReportDetail.builder().reportNo(sequenceReportDetail.getReportNo()).enterpriseNo(bicycleSequenceProcessDto.getEnterpriseNo()).build()));
                if (wxReportDetail != null) {
                    message.setCode(SequenceBusinessCodeEnum.CODE_CASE_PROCESSING.getKey());
                    message.setMessage(SequenceBusinessCodeEnum.CODE_CASE_PROCESSING.getValue());
                    return message;
                } else {
                    BicycleCommonEnterprise commonEnterprise =
                            commonEnterpriseService.getOne(new QueryWrapper<BicycleCommonEnterprise>(BicycleCommonEnterprise.builder().enterpriseNo(bicycleSequenceProcessDto.getEnterpriseNo()).build()));

                    // 插入操作记录
                    BicycleSequenceReportProcess sequenceDetailProcessRecord = new BicycleSequenceReportProcess();
                    sequenceDetailProcessRecord.setCreateTime(new Date());
                    sequenceDetailProcessRecord.setDetailId(bicycleSequenceProcessDto.getDetailId());
                    sequenceDetailProcessRecord.setHasTrue(sequenceReportDetail.getHasTrue());
                    sequenceDetailProcessRecord.setProcessTime(new Date());
                    sequenceDetailProcessRecord.setProcessUserId(bicycleSequenceProcessDto.getProcessUserId());
                    sequenceDetailProcessRecord.setProcessUserName(bicycleSequenceProcessDto.getProcessUserName());
                    sequenceDetailProcessRecord.setRemark(bicycleSequenceProcessDto.getRemark());
                    sequenceDetailProcessRecord.setProcessEnterpriseNo(sequenceReportDetail.getEnterpriseNo());
                    sequenceDetailProcessRecord.setProcessEnterpriseName(sequenceReportDetail.getEnterpriseName());
                    sequenceDetailProcessRecord.setPushEnterpriseNo(commonEnterprise.getEnterpriseNo());
                    sequenceDetailProcessRecord.setPushEnterpriseName(commonEnterprise.getEnterpriseAlias());
                    sequenceDetailProcessRecord.setOperateType(2);
                    sequenceReportProcessService.save(sequenceDetailProcessRecord);
                    sequenceReportDetail.setReportStatus(ReportStatusEnum.REPORT_STATUS_1.getKey());
                    sequenceReportDetail.setUpdateTime(new Date());
                    sequenceReportDetail.setEnterpriseNo(commonEnterprise.getEnterpriseNo());
                    sequenceReportDetail.setEnterpriseName(commonEnterprise.getEnterpriseAlias());
                    BicycleSequenceReportDetail qBicycleSequenceReportDetail = new BicycleSequenceReportDetail();
                    qBicycleSequenceReportDetail.setDetailId(sequenceReportDetail.getDetailId());
                    this.update(sequenceReportDetail, new QueryWrapper<BicycleSequenceReportDetail>(qBicycleSequenceReportDetail));
                }
            }
        }

        return message;
    }

    @Override
    public Message closeCase(BicycleSequenceCloseReportDto bicycleSequenceCloseReportDto) {
        Message message = new Message();

        int length = bicycleSequenceCloseReportDto.getDetailIds().length;
        for (int i = 0; i < length; i++) {
            BicycleSequenceReportDetail sequenceReportDetail = this.getById(bicycleSequenceCloseReportDto.getDetailIds()[i]);

            // 已结案信息不再进行处理
            if (sequenceReportDetail.getReportStatus() != 3) {
                // 已结案
                sequenceReportDetail.setReportStatus(3);
                sequenceReportDetail.setUpdateTime(new Date());
                sequenceReportDetail.setCloseTime(new Date());
                this.updateById(sequenceReportDetail);

                // 插入操作记录
                BicycleSequenceReportProcess sequenceDetailProcessRecord = new BicycleSequenceReportProcess();
                sequenceDetailProcessRecord.setCreateTime(new Date());
                sequenceDetailProcessRecord.setDetailId(sequenceReportDetail.getDetailId());
                sequenceDetailProcessRecord.setHasTrue(sequenceReportDetail.getHasTrue());
                sequenceDetailProcessRecord.setProcessTime(new Date());
                sequenceDetailProcessRecord.setProcessUserId(bicycleSequenceCloseReportDto.getProcessUserId());
                sequenceDetailProcessRecord.setProcessUserName(bicycleSequenceCloseReportDto.getProcessUserName());
                sequenceDetailProcessRecord.setOperateType(3);
                sequenceReportProcessService.save(sequenceDetailProcessRecord);
            }
            List<BicycleSequenceReportDetail> sequenceReportDetails =
                    this.list(new QueryWrapper<BicycleSequenceReportDetail>(BicycleSequenceReportDetail.builder().reportId(sequenceReportDetail.getReportId()).build()));

            boolean isFinish = true;
            // 筛选是否所有企业处理完本条举报
            for (BicycleSequenceReportDetail reportDetail : sequenceReportDetails) {
                if (reportDetail.getReportStatus().intValue() == 3) {
                    // 已结案
                    continue;
                } else {
                    isFinish = false;
                    break;
                }
            }
            if (isFinish) { // 是否所有企业都处理完毕
                BicycleSequenceReportRecord reportRecord =
                        sequenceReportRecordService.getById(sequenceReportDetail.getReportId());
                if (reportRecord != null) {
                    reportRecord.setReportStatus(1); // 已处理
                    sequenceReportRecordService.updateById(reportRecord);
                }
            }
        }

        return message;
    }
}
