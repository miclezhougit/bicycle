package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleFacilitiesEnterpriseEquipment;
import com.bicycle.service.mapper.BicycleFacilitiesEnterpriseEquipmentMapper;
import com.bicycle.service.service.IBicycleFacilitiesEnterpriseEquipmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业智能设施信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
@Service
public class BicycleFacilitiesEnterpriseEquipmentServiceImpl extends ServiceImpl<BicycleFacilitiesEnterpriseEquipmentMapper, BicycleFacilitiesEnterpriseEquipment> implements IBicycleFacilitiesEnterpriseEquipmentService {

}
