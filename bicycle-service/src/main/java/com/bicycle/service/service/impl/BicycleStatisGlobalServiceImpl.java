package com.bicycle.service.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.helper.RedisStringHelper;
import com.bicycle.service.cache.DataCacheOprator;
import com.bicycle.service.config.MyBatisPlusConfig;
import com.bicycle.service.entity.*;
import com.bicycle.service.mapper.BicycleStatisGlobalMapper;
import com.bicycle.service.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bicycle.service.vo.BicycleStatisGlobalVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 全量数据统计表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
@Service
public class BicycleStatisGlobalServiceImpl extends ServiceImpl<BicycleStatisGlobalMapper, BicycleStatisGlobal> implements IBicycleStatisGlobalService {


    @Autowired
    private IBicycleDataOrderInfoService orderInfoService;
    @Autowired
    private IBicycleDataBicycleSiteService bicycleSiteService;
    @Autowired
    private IBicycleBasicsBicycleService basicsBicycleService;
    @Autowired
    private IBicycleSequenceReportDetailService reportDetailService;
    @Autowired
    private IBicycleBasicsNoRecordService bicycleBasicsNoRecordService;
    @Autowired
    private IBicycleCommonEnterpriseService enterpriseService;
    @Autowired
    private RedisStringHelper redisStringHelper;
    @Autowired
    private Environment environment;


    @Override
    public void statis() {
        Date statisDate = DateUtil.offsetDay(new Date(), -1);
        this.remove(new QueryWrapper<BicycleStatisGlobal>(BicycleStatisGlobal.builder().statisDate(statisDate).statisType(1).build()));
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        List<BicycleCommonEnterprise> enterpriseList = DataCacheOprator.getInstance().getEnterpriseList();

        for (BicycleCommonEnterprise commonEnterprise : enterpriseList) {
            //车辆总数
            QueryWrapper<BicycleDataBicycleSite> queryWrapper = new QueryWrapper<BicycleDataBicycleSite>();
            queryWrapper.groupBy("bicycle_no");
            if(environment.getProperty("region.district") != null) {
                queryWrapper.eq("district", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                queryWrapper.eq("street", environment.getProperty("region.street"));
            }
            MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(statisDate, "yyyyMMdd"));
            List<BicycleDataBicycleSite> bicycleSites = bicycleSiteService.list(queryWrapper);
            int bicycleCount = bicycleSites == null ? 0 : bicycleSites.size();

            //订单总数
            QueryWrapper<BicycleDataOrderInfo> orderInfoQueryWrapper = new QueryWrapper<BicycleDataOrderInfo>();
            orderInfoQueryWrapper.between("end_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            if(environment.getProperty("region.district") != null) {
                orderInfoQueryWrapper.eq("end_district", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                orderInfoQueryWrapper.eq("end_street", environment.getProperty("region.street"));
            }
            MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(statisDate, "yyyyMM"));
            int orderCount = orderInfoService.count(orderInfoQueryWrapper);

            //超量告警总数
            QueryWrapper<BicycleSequenceReportDetail> reportDetailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            reportDetailQueryWrapper.between("report_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            reportDetailQueryWrapper.like("report_type", 5);
            if(environment.getProperty("region.district") != null) {
                reportDetailQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                reportDetailQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int excessCount = reportDetailService.count(reportDetailQueryWrapper);

            //违停总数
            QueryWrapper<BicycleSequenceReportDetail> sequenceReportDetailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            sequenceReportDetailQueryWrapper.between("report_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            sequenceReportDetailQueryWrapper.like("report_type", 1);
            if(environment.getProperty("region.district") != null) {
                sequenceReportDetailQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                sequenceReportDetailQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int illegalParkCount = reportDetailService.count(sequenceReportDetailQueryWrapper);

            //执法总数
            QueryWrapper<BicycleSequenceReportDetail> detailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            detailQueryWrapper.between("report_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            detailQueryWrapper.eq("report_resource", 1);
            if(environment.getProperty("region.district") != null) {
                detailQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                detailQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int enforceCaseCount = reportDetailService.count(detailQueryWrapper);

            //违投总数
            QueryWrapper<BicycleSequenceReportDetail> illegalDeliveryQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            illegalDeliveryQueryWrapper.between("report_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            illegalDeliveryQueryWrapper.eq("report_resource", 4);
            if(environment.getProperty("region.district") != null) {
                illegalDeliveryQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                illegalDeliveryQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int illegalDeliveryCount = reportDetailService.count(illegalDeliveryQueryWrapper);

            BicycleStatisGlobal statisGlobal = new BicycleStatisGlobal();
            //日期
            statisGlobal.setStatisType(1);
            statisGlobal.setCreateTime(new Date());
            statisGlobal.setBikeCount(bicycleCount);
            statisGlobal.setEnforceCaseCount(enforceCaseCount);
            statisGlobal.setIllegalParkCount(illegalParkCount);
            statisGlobal.setIllegalDeliveryCount(illegalDeliveryCount);
            statisGlobal.setOrderCount(orderCount);
            statisGlobal.setExcessCount(excessCount);
            statisGlobal.setStatisDate(statisDate);
            statisGlobal.setEnterpriseNo(commonEnterprise.getEnterpriseNo());
            this.save(statisGlobal);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void statisHour(String param) {
        Date statisDate = DateUtil.offsetHour(new Date(), -1);
        if (StringUtils.isNotBlank(param)) {
            statisDate = DateUtil.parse(param, "yyyy-MM-dd HH");
        }
        int hour = DateUtil.hour(statisDate, true);
        this.remove(new QueryWrapper<BicycleStatisGlobal>(BicycleStatisGlobal.builder().statisDate(DateUtil.beginOfDay(statisDate)).statisType(2).statisHour(String.valueOf(hour)).build()));

        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        List<BicycleCommonEnterprise> enterpriseList = DataCacheOprator.getInstance().getEnterpriseList();

        for (BicycleCommonEnterprise commonEnterprise : enterpriseList) {
            //车辆总数
            /*QueryWrapper<BicycleDataBicycleSite> queryWrapper = new QueryWrapper<BicycleDataBicycleSite>();
            queryWrapper.eq("com_id", commonEnterprise.getEnterpriseNo());
            queryWrapper.between("create_date", DateUtil.format(statisDate, "yyyy-MM-dd HH:00:00"), DateUtil.format(statisDate, "yyyy-MM-dd HH:59:59"));
            queryWrapper.groupBy("bicycle_no");
            if(environment.getProperty("region.district") != null) {
                queryWrapper.eq("district", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                queryWrapper.eq("street", environment.getProperty("region.street"));
            }
            MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(statisDate, "yyyyMMdd"));
            List<BicycleDataBicycleSite> bicycleSites = bicycleSiteService.list(queryWrapper);
            int bicycleCount = bicycleSites == null ? 0 : bicycleSites.size();*/

            QueryWrapper<BicycleBasicsBicycle> queryWrapper = new QueryWrapper<BicycleBasicsBicycle>();
            queryWrapper.between("create_date", DateUtil.format(statisDate, "yyyy-MM-dd HH:00:00"), DateUtil.format(statisDate, "yyyy-MM-dd HH:59:59"));
            queryWrapper.eq("enterprise_no", commonEnterprise.getEnterpriseNo());
            if(environment.getProperty("region.district") != null) {
                queryWrapper.eq("district", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                queryWrapper.eq("street", environment.getProperty("region.street"));
            }
            int bicycleCount = basicsBicycleService.count(queryWrapper);

            //订单总数
            QueryWrapper<BicycleDataOrderInfo> orderInfoQueryWrapper = new QueryWrapper<BicycleDataOrderInfo>();
            orderInfoQueryWrapper.eq("com_id", commonEnterprise.getEnterpriseNo());
            orderInfoQueryWrapper.between("end_time", DateUtil.format(statisDate, "yyyy-MM-dd HH:00:00"), DateUtil.format(statisDate, "yyyy-MM-dd HH:59:59"));
            if(environment.getProperty("region.district") != null) {
                orderInfoQueryWrapper.eq("end_district", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                orderInfoQueryWrapper.eq("end_street", environment.getProperty("region.street"));
            }
            MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(statisDate, "yyyyMM"));
            int orderCount = orderInfoService.count(orderInfoQueryWrapper);

            //超量告警总数
            QueryWrapper<BicycleSequenceReportDetail> reportDetailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            reportDetailQueryWrapper.eq("enterprise_no", commonEnterprise.getEnterpriseNo());
            reportDetailQueryWrapper.between("report_time", DateUtil.format(statisDate, "yyyy-MM-dd HH:00:00"), DateUtil.format(statisDate, "yyyy-MM-dd HH:59:59"));
            reportDetailQueryWrapper.like("report_type", 5);
            if(environment.getProperty("region.district") != null) {
                reportDetailQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                reportDetailQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int excessCount = reportDetailService.count(reportDetailQueryWrapper);

            //违停总数
            QueryWrapper<BicycleSequenceReportDetail> sequenceReportDetailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            sequenceReportDetailQueryWrapper.eq("enterprise_no", commonEnterprise.getEnterpriseNo());
            sequenceReportDetailQueryWrapper.between("report_time", DateUtil.format(statisDate, "yyyy-MM-dd HH:00:00"), DateUtil.format(statisDate, "yyyy-MM-dd HH:59:59"));
            sequenceReportDetailQueryWrapper.like("report_type", 1);
            if(environment.getProperty("region.district") != null) {
                sequenceReportDetailQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                sequenceReportDetailQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int illegalParkCount = reportDetailService.count(sequenceReportDetailQueryWrapper);

            //执法总数
            QueryWrapper<BicycleSequenceReportDetail> detailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            detailQueryWrapper.eq("enterprise_no", commonEnterprise.getEnterpriseNo());
            detailQueryWrapper.between("report_time", DateUtil.format(statisDate, "yyyy-MM-dd HH:00:00"), DateUtil.format(statisDate, "yyyy-MM-dd HH:59:59"));
            detailQueryWrapper.eq("report_resource", 1);
            if(environment.getProperty("region.district") != null) {
                detailQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                detailQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int enforceCaseCount = reportDetailService.count(detailQueryWrapper);

            //违投总数
            QueryWrapper<BicycleSequenceReportDetail> illegalDeliveryQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            illegalDeliveryQueryWrapper.between("report_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            illegalDeliveryQueryWrapper.eq("report_resource", 4);
            if(environment.getProperty("region.district") != null) {
                illegalDeliveryQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                illegalDeliveryQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int illegalDeliveryCount = reportDetailService.count(illegalDeliveryQueryWrapper);


            BicycleStatisGlobal statisGlobal = new BicycleStatisGlobal();
            //日期
            statisGlobal.setStatisType(2);
            statisGlobal.setCreateTime(new Date());
            statisGlobal.setBikeCount(bicycleCount);
            statisGlobal.setEnforceCaseCount(enforceCaseCount);
            statisGlobal.setIllegalParkCount(illegalParkCount);
            statisGlobal.setIllegalDeliveryCount(illegalDeliveryCount);
            statisGlobal.setOrderCount(orderCount);
            statisGlobal.setExcessCount(excessCount);
            statisGlobal.setStatisDate(statisDate);
            statisGlobal.setStatisHour(String.valueOf(hour));
            statisGlobal.setEnterpriseNo(commonEnterprise.getEnterpriseNo());
            this.save(statisGlobal);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void statisDay(String param) {
        Date statisDate = DateUtil.beginOfDay(DateUtil.offsetDay(new Date(), -1));
        if (StringUtils.isNotBlank(param)) {
            statisDate = DateUtil.beginOfDay(DateUtil.parse(param, "yyyy-MM-dd"));
        }

        this.remove(new QueryWrapper<BicycleStatisGlobal>(BicycleStatisGlobal.builder().statisDate(statisDate).statisType(1).build()));
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        List<BicycleCommonEnterprise> enterpriseList = DataCacheOprator.getInstance().getEnterpriseList();

        for (BicycleCommonEnterprise commonEnterprise : enterpriseList) {
            //车辆总数
            /*QueryWrapper<BicycleDataBicycleSite> queryWrapper = new QueryWrapper<BicycleDataBicycleSite>();
            queryWrapper.groupBy("bicycle_no");
            queryWrapper.eq("com_id", commonEnterprise.getEnterpriseNo());
            if(environment.getProperty("region.district") != null) {
                queryWrapper.eq("district", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                queryWrapper.eq("street", environment.getProperty("region.street"));
            }
            MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(statisDate, "yyyyMMdd"));
            List<BicycleDataBicycleSite> bicycleSites = bicycleSiteService.list(queryWrapper);*/
            QueryWrapper<BicycleBasicsBicycle> queryWrapper = new QueryWrapper<BicycleBasicsBicycle>();
            queryWrapper.eq("enterprise_no", commonEnterprise.getEnterpriseNo());
            if(environment.getProperty("region.district") != null) {
                queryWrapper.eq("district", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                queryWrapper.eq("street", environment.getProperty("region.street"));
            }
            int bicycleCount = basicsBicycleService.count(queryWrapper);

            //int bicycleCount = bicycleSites == null ? 0 : bicycleSites.size();

            //订单总数
            QueryWrapper<BicycleDataOrderInfo> orderInfoQueryWrapper = new QueryWrapper<BicycleDataOrderInfo>();
            orderInfoQueryWrapper.eq("com_id", commonEnterprise.getEnterpriseNo());
            orderInfoQueryWrapper.between("end_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            if(environment.getProperty("region.district") != null) {
                orderInfoQueryWrapper.eq("end_district", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                orderInfoQueryWrapper.eq("end_street", environment.getProperty("region.street"));
            }
            MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(statisDate, "yyyyMM"));
            int orderCount = orderInfoService.count(orderInfoQueryWrapper);

            //超量告警总数
            QueryWrapper<BicycleSequenceReportDetail> reportDetailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            reportDetailQueryWrapper.eq("enterprise_no", commonEnterprise.getEnterpriseNo());
            reportDetailQueryWrapper.between("report_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            reportDetailQueryWrapper.like("report_type", 5);
            if(environment.getProperty("region.district") != null) {
                reportDetailQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                reportDetailQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int excessCount = reportDetailService.count(reportDetailQueryWrapper);

            //违停总数
            QueryWrapper<BicycleSequenceReportDetail> sequenceReportDetailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            sequenceReportDetailQueryWrapper.eq("enterprise_no", commonEnterprise.getEnterpriseNo());
            sequenceReportDetailQueryWrapper.between("report_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            sequenceReportDetailQueryWrapper.like("report_type", 1);
            if(environment.getProperty("region.district") != null) {
                sequenceReportDetailQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                sequenceReportDetailQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int illegalParkCount = reportDetailService.count(sequenceReportDetailQueryWrapper);

            //执法总数
            QueryWrapper<BicycleSequenceReportDetail> detailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            detailQueryWrapper.eq("enterprise_no", commonEnterprise.getEnterpriseNo());
            detailQueryWrapper.between("report_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            detailQueryWrapper.eq("report_resource", 1);
            if(environment.getProperty("region.district") != null) {
                detailQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                detailQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int enforceCaseCount = reportDetailService.count(detailQueryWrapper);

            //违投总数
            QueryWrapper<BicycleSequenceReportDetail> illegalDeliveryQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
            illegalDeliveryQueryWrapper.between("report_time", DateUtil.beginOfDay(statisDate), DateUtil.endOfDay(statisDate));
            illegalDeliveryQueryWrapper.eq("report_resource", 4);
            if(environment.getProperty("region.district") != null) {
                illegalDeliveryQueryWrapper.eq("district_id", environment.getProperty("region.district"));
            }
            if(environment.getProperty("region.street") != null) {
                illegalDeliveryQueryWrapper.eq("street_id", environment.getProperty("region.street"));
            }
            int illegalDeliveryCount = reportDetailService.count(illegalDeliveryQueryWrapper);

            BicycleStatisGlobal statisGlobal = new BicycleStatisGlobal();
            //日期
            statisGlobal.setStatisType(1);
            statisGlobal.setCreateTime(new Date());
            statisGlobal.setBikeCount(bicycleCount);
            statisGlobal.setEnforceCaseCount(enforceCaseCount);
            statisGlobal.setIllegalParkCount(illegalParkCount);
            statisGlobal.setIllegalDeliveryCount(illegalDeliveryCount);
            statisGlobal.setOrderCount(orderCount);
            statisGlobal.setExcessCount(excessCount);
            statisGlobal.setStatisDate(statisDate);
            statisGlobal.setEnterpriseNo(commonEnterprise.getEnterpriseNo());
            this.save(statisGlobal);
        }

    }

    /**
     * @Description: 针对统计分析按日期或者时间的统计，针对某个期间 不存在数据的，则默认设置0
     * @Author: zhouzm
     * @Date: 2021/7/27 21:31
     * @param enterpriseVoList:
     * @return: java.util.List<com.bicycle.service.vo.BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>
     **/
    @Override
    public List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>
            dealBicycleStatisGlobal(List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> enterpriseVoList) {
        //获取企业信息
        QueryWrapper<BicycleCommonEnterprise> queryWrapper = new QueryWrapper<BicycleCommonEnterprise>();
        queryWrapper.eq("enterprise_status",0);
        List<BicycleCommonEnterprise> bicycleCommonEnterpriseList = enterpriseService.list(queryWrapper);
        boolean flag = false;
        List<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo> qEnterpriseVoList = new ArrayList<BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo>();
        BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
        if(enterpriseVoList != null && enterpriseVoList.size() > 0) {
            for(BicycleCommonEnterprise commonEnterprise : bicycleCommonEnterpriseList) {
                //当分析数据集合内没有匹配到对应企业数据时，则表名该企业没有对应汇总数据，需要设置默认值
                flag = false;
                for(BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo statisGlobalEnterpriseVo : enterpriseVoList) {
                    if(commonEnterprise.getEnterpriseNo().equals(statisGlobalEnterpriseVo.getEnterpriseNo())) {
                        flag = true;
                        enterpriseVo = statisGlobalEnterpriseVo;
                        break;
                    }
                }
                if(!flag) {//未匹配到数据，添加默认汇总数据
                    enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                    enterpriseVo.setEnterpriseNo(commonEnterprise.getEnterpriseNo());
                    enterpriseVo.setEnterpriseName(commonEnterprise.getEnterpriseAlias());
                    enterpriseVo.setStatisCount(0);
                }
                qEnterpriseVoList.add(enterpriseVo);
            }
        } else {
            for(BicycleCommonEnterprise commonEnterprise : bicycleCommonEnterpriseList) {
                enterpriseVo = new BicycleStatisGlobalVo.BicycleStatisGlobalEnterpriseVo();
                enterpriseVo.setEnterpriseNo(commonEnterprise.getEnterpriseNo());
                enterpriseVo.setEnterpriseName(commonEnterprise.getEnterpriseAlias());
                enterpriseVo.setStatisCount(0);
                qEnterpriseVoList.add(enterpriseVo);
            }
        }
        return qEnterpriseVoList;
    }
}
