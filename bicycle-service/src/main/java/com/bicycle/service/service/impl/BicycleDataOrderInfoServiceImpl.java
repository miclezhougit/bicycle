package com.bicycle.service.service.impl;

import com.bicycle.service.entity.BicycleDataOrderInfo;
import com.bicycle.service.mapper.BicycleDataOrderInfoMapper;
import com.bicycle.service.service.IBicycleDataOrderInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单信息表 服务实现类
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
@Service
public class BicycleDataOrderInfoServiceImpl extends ServiceImpl<BicycleDataOrderInfoMapper, BicycleDataOrderInfo> implements IBicycleDataOrderInfoService {

}
