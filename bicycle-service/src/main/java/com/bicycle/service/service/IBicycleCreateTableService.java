package com.bicycle.service.service;

/**
 * @author : layne
 * @Date: 2020/8/22
 * @Time: 1:29
 * Description:
 */
public interface IBicycleCreateTableService {

    void createOrderTable(String tableName);

    void createSiteTable(String tableName);
}
