package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleStatisGlobal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 全量数据统计表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface BicycleStatisGlobalMapper extends BaseMapper<BicycleStatisGlobal> {

}
