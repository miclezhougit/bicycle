package com.bicycle.service.mapper;

import com.bicycle.service.dto.BicycleBasicsBicycleMapDto;
import com.bicycle.service.entity.BicycleBasicsBicycle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 车辆基础信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-08
 */
public interface BicycleBasicsBicycleMapper extends BaseMapper<BicycleBasicsBicycle> {

    List<BicycleBasicsBicycleMapDto> selectBicyclePutOnCount(Map<String, Object> paramMap);
}
