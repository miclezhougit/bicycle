package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSequenceUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 移动端用户信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface BicycleSequenceUserMapper extends BaseMapper<BicycleSequenceUser> {

}
