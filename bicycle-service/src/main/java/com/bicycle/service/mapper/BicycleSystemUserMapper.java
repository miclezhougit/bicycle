package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSystemUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统用户信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface BicycleSystemUserMapper extends BaseMapper<BicycleSystemUser> {

}
