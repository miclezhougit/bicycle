package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleCommonFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 文件信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface BicycleCommonFileMapper extends BaseMapper<BicycleCommonFile> {

}
