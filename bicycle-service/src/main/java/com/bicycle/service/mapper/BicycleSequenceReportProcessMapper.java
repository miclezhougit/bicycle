package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSequenceReportProcess;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 举报处理记录表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface BicycleSequenceReportProcessMapper extends BaseMapper<BicycleSequenceReportProcess> {

}
