package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSystemRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface BicycleSystemRoleMenuMapper extends BaseMapper<BicycleSystemRoleMenu> {

}
