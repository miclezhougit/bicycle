package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSystemRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface BicycleSystemRoleMapper extends BaseMapper<BicycleSystemRole> {

}
