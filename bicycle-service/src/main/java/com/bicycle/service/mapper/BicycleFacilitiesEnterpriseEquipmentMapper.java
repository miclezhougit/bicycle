package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleFacilitiesEnterpriseEquipment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业智能设施信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface BicycleFacilitiesEnterpriseEquipmentMapper extends BaseMapper<BicycleFacilitiesEnterpriseEquipment> {

}
