package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSystemLoginError;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统用户登录错误表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface BicycleSystemLoginErrorMapper extends BaseMapper<BicycleSystemLoginError> {

}
