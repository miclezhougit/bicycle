package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSystemOperationLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统日志信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface BicycleSystemOperationLogMapper extends BaseMapper<BicycleSystemOperationLog> {

}
