package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSystemMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 菜单信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface BicycleSystemMenuMapper extends BaseMapper<BicycleSystemMenu> {

    List<BicycleSystemMenu> selectSystemMenuListByRole(@Param("roleId") Integer roleId);
}
