package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业基础信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface BicycleCommonEnterpriseMapper extends BaseMapper<BicycleCommonEnterprise> {

}
