package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleCommonRegion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 行政街道信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface BicycleCommonRegionMapper extends BaseMapper<BicycleCommonRegion> {

}
