package com.bicycle.service.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * @author : layne
 * @Date: 2020/8/22
 * @Time: 1:30
 * Description:
 */
public interface BicycleCreateTableMapper {

    public void createOrderTable(@Param("tableName") String tableName);

    public void createSiteTable(@Param("tableName") String tableName);
}
