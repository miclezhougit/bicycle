package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSequenceReportPhoto;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 举报记录详情图片表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface BicycleSequenceReportPhotoMapper extends BaseMapper<BicycleSequenceReportPhoto> {

}
