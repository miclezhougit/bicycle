package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleDataOrderInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface BicycleDataOrderInfoMapper extends BaseMapper<BicycleDataOrderInfo> {

}
