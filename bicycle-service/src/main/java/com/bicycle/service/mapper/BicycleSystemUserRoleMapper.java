package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSystemUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
public interface BicycleSystemUserRoleMapper extends BaseMapper<BicycleSystemUserRole> {

}
