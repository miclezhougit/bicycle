package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleDataBicycleSite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 车辆位置表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface BicycleDataBicycleSiteMapper extends BaseMapper<BicycleDataBicycleSite> {

}
