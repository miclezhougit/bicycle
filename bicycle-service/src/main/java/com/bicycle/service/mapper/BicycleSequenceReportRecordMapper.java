package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSequenceReportRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公众号用户举报记录表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface BicycleSequenceReportRecordMapper extends BaseMapper<BicycleSequenceReportRecord> {

}
