package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleFacilitiesEnterpriseArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业停放区域信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
public interface BicycleFacilitiesEnterpriseAreaMapper extends BaseMapper<BicycleFacilitiesEnterpriseArea> {

}
