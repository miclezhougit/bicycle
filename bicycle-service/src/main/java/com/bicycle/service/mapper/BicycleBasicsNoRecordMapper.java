package com.bicycle.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bicycle.service.dto.BicycleBasicsNoRecordMapDto;
import com.bicycle.service.entity.BicycleBasicsBicycle;
import com.bicycle.service.entity.BicycleBasicsNoRecord;

import java.util.List;

/**
 * <p>
 * 未备案车辆信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-08
 */
public interface BicycleBasicsNoRecordMapper extends BaseMapper<BicycleBasicsNoRecord> {

    List<BicycleBasicsNoRecordMapDto> selectBicycleNoRecordCount();

}
