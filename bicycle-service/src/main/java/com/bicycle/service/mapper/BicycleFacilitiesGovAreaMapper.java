package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleFacilitiesGovArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 政府停放区域信息表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-08
 */
public interface BicycleFacilitiesGovAreaMapper extends BaseMapper<BicycleFacilitiesGovArea> {

    Integer sumParkValue();
}
