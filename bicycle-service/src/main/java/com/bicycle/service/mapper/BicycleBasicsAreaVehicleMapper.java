package com.bicycle.service.mapper;

import com.bicycle.service.dto.BicycleBasicsAreaVehicleDto;
import com.bicycle.service.entity.BicycleBasicsAreaVehicle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 车辆区域关联表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-29
 */
public interface BicycleBasicsAreaVehicleMapper extends BaseMapper<BicycleBasicsAreaVehicle> {

    List<BicycleBasicsAreaVehicleDto> selectAreaVehicleByAreaId(Integer area_id);

}
