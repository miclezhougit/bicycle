package com.bicycle.service.mapper;

import com.bicycle.service.entity.BicycleSequenceReportDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 举报记录详情表 Mapper 接口
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
public interface BicycleSequenceReportDetailMapper extends BaseMapper<BicycleSequenceReportDetail> {

}
