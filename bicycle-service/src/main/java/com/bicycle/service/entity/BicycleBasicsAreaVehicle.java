package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 车辆区域关联表
* </p>
*
* @author layne
* @since 2020-08-29
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleBasicsAreaVehicle对象", description="车辆区域关联表")
public class BicycleBasicsAreaVehicle implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "企业编号")
	private String enterpriseNo;

    @ApiModelProperty(value = "企业名称")
	private String enterpriseName;

    @ApiModelProperty(value = "车辆编号")
	private String vehicleNo;

    @ApiModelProperty(value = "区域主键")
	private Integer areaId;

    @ApiModelProperty(value = "区域属性：0：停放区，1：监控区，2：禁停区")
	private Integer areaAttribute;

    @ApiModelProperty(value = "创建人")
	private String createName;

    @ApiModelProperty(value = "创建人主键")
	private Integer createBy;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "修改人")
	private String updateName;

    @ApiModelProperty(value = "修改人主键")
	private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
	private Date updateTime;


}
