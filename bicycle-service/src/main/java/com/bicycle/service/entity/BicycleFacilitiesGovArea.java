package com.bicycle.service.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 政府停放区域信息表
 * </p>
 *
 * @author layne
 * @since 2020-08-08
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "BicycleFacilitiesGovArea对象", description = "政府停放区域信息表")
public class BicycleFacilitiesGovArea implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "区域名称")
    private String areaName;

    @ApiModelProperty(value = "区域编号")
    private String areaNo;

    @ApiModelProperty(value = "区域类型：0：停放区，1：禁停区，2：规范停放区，3：动态调节区，4：临时管控区")
    private Integer areaType;

    @ApiModelProperty(value = "区域属性：0：停放区，1：监控区，2：禁停区")
    private Integer areaAttribute;

    @ApiModelProperty(value = "停放容量")
    private Integer parkValue;

    @ApiModelProperty(value = "告警值")
    private Integer alarmValue;

    @ApiModelProperty(value = "预警次数")
    private Integer alarmCount;

    @ApiModelProperty(value = "监控间隔时间 （分钟单位）")
    private Integer monitorInterval;

    @ApiModelProperty(value = "紧急类型：0：一般，1：紧急")
    private Integer urgentType;

    @ApiModelProperty(value = "监控时段")
    private String monitorTime;

    @ApiModelProperty(value = "面积")
    private Integer area;

    @ApiModelProperty(value = "行政区主键")
    private Integer district;

    @ApiModelProperty(value = "街道主键")
    private Integer street;

    @ApiModelProperty(value = "有效期开始时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date periodBegin;

    @ApiModelProperty(value = "有效期结束时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date periodEnd;

    @ApiModelProperty(value = "区域状态：0：正常，1：规划中，2：停用，3：废除")
    private Integer areaStatus;

    @ApiModelProperty(value = "经度")
    private String lng;

    @ApiModelProperty(value = "纬度")
    private String lat;

    @ApiModelProperty(value = "区域坐标点")
    private String areaPoints;

    @ApiModelProperty(value = "区域宽度（单位米）")
    private BigDecimal areaWide;

    @ApiModelProperty(value = "区域长度（单位米）")
    private BigDecimal areaLength;

    @ApiModelProperty(value = "区域形式：0：斜列式，1：垂直式")
    private Integer areaModel;

    @ApiModelProperty(value = "区域位置")
    private String areaAddress;

    @ApiModelProperty(value = "是否删除 0：未删除 1：:已删除")
    private Integer hasDel;

    @ApiModelProperty(value = "任务点")
    private Integer taskPoint;

    @ApiModelProperty(value = "创建人")
    private String createName;

    @ApiModelProperty(value = "创建人主键")
    private Integer createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改人")
    private String updateName;

    @ApiModelProperty(value = "修改人主键")
    private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;


}
