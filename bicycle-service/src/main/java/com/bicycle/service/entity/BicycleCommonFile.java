package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 文件信息表
* </p>
*
* @author layne
* @since 2020-08-06
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleCommonFile对象", description="文件信息表")
public class BicycleCommonFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "文件名称")
	private String fileName;

    @ApiModelProperty(value = "文件后缀")
	private String fileSuffix;

    @ApiModelProperty(value = "文件相对路径")
	private String relativePath;

    @ApiModelProperty(value = "文件绝对路径")
	private String absolutePath;

    @ApiModelProperty(value = "访问下载路径")
	private String downloadPath;

    @ApiModelProperty(value = "文件大小")
	private Integer fileSize;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;


}
