package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 行政街道信息表
* </p>
*
* @author layne
* @since 2020-08-06
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleCommonRegion对象", description="行政街道信息表")
public class BicycleCommonRegion implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "区域编码")
	private String regionCode;

    @ApiModelProperty(value = "区域名称")
	private String regionName;

    @ApiModelProperty(value = "父级区域")
	private Integer parentId;

    @ApiModelProperty(value = "区域阈值")
	private Integer volume;

    @ApiModelProperty(value = "预警值")
	private Integer alarmValue;

    @ApiModelProperty(value = "区域坐标点")
	private String regionPoints;

    @ApiModelProperty(value = "经度")
	private String lng;

    @ApiModelProperty(value = "纬度")
	private String lat;

    @ApiModelProperty(value = "区域级别：0：行政区，1：街道")
	private Integer regionType;

    @ApiModelProperty(value = "创建人")
	private String createName;

    @ApiModelProperty(value = "创建人主键")
	private Integer createBy;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "修改人")
	private String updateName;

    @ApiModelProperty(value = "修改人主键")
	private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
	private Date updateTime;


}
