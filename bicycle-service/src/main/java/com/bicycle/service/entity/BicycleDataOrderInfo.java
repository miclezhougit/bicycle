package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 订单信息表
* </p>
*
* @author layne
* @since 2020-08-15
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleDataOrderInfo对象", description="订单信息表")
public class BicycleDataOrderInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键：主键")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "企业标识：统一分配")
	private String comId;

    @ApiModelProperty(value = "订单编号：用户订单编号")
	private String orderNo;

    @ApiModelProperty(value = "车辆编号：车辆编号")
	private String bicycleNo;

    @ApiModelProperty(value = "下单用户：用户唯一标示")
	private String userId;

    @ApiModelProperty(value = "下单时间：下单时间")
	private Date startTime;

    @ApiModelProperty(value = "下单经度：下单经度")
	private String startLng;

    @ApiModelProperty(value = "下单纬度：下单纬度")
	private String startLat;

    @ApiModelProperty(value = "还车时间：还车时间")
	private Date endTime;

    @ApiModelProperty(value = "结束经度：结束经度")
	private String endLng;

    @ApiModelProperty(value = "结束纬度：结束纬度")
	private String endLat;

    @ApiModelProperty(value = "订单里程：订单里程数")
	private Double orderMile;

    @ApiModelProperty(value = "订单时长 单位：秒")
	private Integer orderTime;

    @ApiModelProperty(value = "最后更新时间：订单最后更新时间")
	private Date uploadTime;

    @ApiModelProperty(value = "借车区域")
	private Integer startDistrict;

    @ApiModelProperty(value = "借车街道")
	private Integer startStreet;

    @ApiModelProperty(value = "还车区域")
	private Integer endDistrict;

    @ApiModelProperty(value = "还车街道")
	private Integer endStreet;

    @ApiModelProperty(value = "创建日期：创建日期")
	private Date createDate;


}
