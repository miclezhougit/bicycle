package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 系统用户登录错误表
* </p>
*
* @author layne
* @since 2020-08-10
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleSystemLoginError对象", description="系统用户登录错误表")
public class BicycleSystemLoginError implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "用户账户")
	private String account;

    @ApiModelProperty(value = "登录IP")
	private String loginIp;

    @ApiModelProperty(value = "登录日期")
	private Date loginDate;

    @ApiModelProperty(value = "错误次数")
	private Integer errorNum;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "更新时间")
	private Date updateTime;


}
