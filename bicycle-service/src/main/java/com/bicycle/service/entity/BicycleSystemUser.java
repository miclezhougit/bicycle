package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 系统用户信息表
* </p>
*
* @author layne
* @since 2020-08-10
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleSystemUser对象", description="系统用户信息表")
public class BicycleSystemUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "sys_user_id", type = IdType.AUTO)
	private Integer sysUserId;

    @ApiModelProperty(value = "用户账户")
	private String account;

    @ApiModelProperty(value = "登录密码")
	private String password;

    @ApiModelProperty(value = "姓名")
	private String userName;

    @ApiModelProperty(value = "联系电话")
	private String mobile;

    @ApiModelProperty(value = "联系邮箱")
	private String email;

    @ApiModelProperty(value = "部门")
	private String department;

    @ApiModelProperty(value = "用户状态：0：废除，1：启用，2：停止")
	private Integer userStatus;

    @ApiModelProperty(value = "区域ID")
	private Integer regionId;

    @ApiModelProperty(value = "区域名称")
	private String regionName;

    @ApiModelProperty(value = "用户类型：1：市交通，2：市交警，3：市城管，4：区政府")
	private Integer userType;

    @ApiModelProperty(value = "登录时间")
	private Date loginTime;

    @ApiModelProperty(value = "是否删除 0：未删除 1：:已删除")
	private Integer hasDel;

    @ApiModelProperty(value = "创建人")
	private String createName;

    @ApiModelProperty(value = "创建人主键")
	private Integer createBy;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "修改人")
	private String updateName;

    @ApiModelProperty(value = "修改人主键")
	private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
	private Date updateTime;


}
