package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.bicycle.common.entity.base.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 移动端用户信息表
* </p>
*
* @author layne
* @since 2020-08-06
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleSequenceUser对象", description="移动端用户信息表")
public class BicycleSequenceUser extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "user_id", type = IdType.AUTO)
	private Integer userId;

    @ApiModelProperty(value = "手机号")
	private String mobile;

    @ApiModelProperty(value = "身份证")
	private String idCard;

    @ApiModelProperty(value = "姓名")
	private String userName;

    @ApiModelProperty(value = "用户类型：1：企业用户，2：政府用户")
	private Integer userType;

    @ApiModelProperty(value = "归属单位")
	private Integer agencyId;

    @ApiModelProperty(value = "归属单位名称")
	private String agencyName;

    @ApiModelProperty(value = "企业编号")
	private String enterpriseNo;

    @ApiModelProperty(value = "企业名称")
	private String enterpriseName;

    @ApiModelProperty(value = "区域主键")
	private Integer districtId;

    @TableField(strategy = FieldStrategy.NOT_EMPTY)
    @ApiModelProperty(value = "区域名称")
	private String districtName;

    @ApiModelProperty(value = "街道主键")
	private Integer streetId;

    @TableField(strategy = FieldStrategy.NOT_EMPTY)
    @ApiModelProperty(value = "街道名称")
	private String streetName;

    @ApiModelProperty(value = "是否删除 0：未删除 1：:已删除")
	private Integer hasDel;

    @ApiModelProperty(value = "微信openid")
	private String openId;

    @ApiModelProperty(value = "用户状态：0：正常，1：锁定，2：作废")
	private Integer userStatus;

    @ApiModelProperty(value = "上次登录时间")
	private Date loginTime;

    @ApiModelProperty(value = "登录密码")
	private String loginPassword;

    @ApiModelProperty(value = "创建人")
	private String createName;

    @ApiModelProperty(value = "创建人主键")
	private Integer createBy;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "修改人")
	private String updateName;

    @ApiModelProperty(value = "修改人主键")
	private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
	private Date updateTime;


}
