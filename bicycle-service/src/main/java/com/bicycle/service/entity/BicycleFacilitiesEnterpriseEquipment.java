package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import com.bicycle.common.entity.base.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
 * <p>
 * 企业智能设施信息表
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "BicycleFacilitiesEnterpriseEquipment对象", description = "企业智能设施信息表")
public class BicycleFacilitiesEnterpriseEquipment extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "企业编号")
    private String enterpriseNo;

    @ApiModelProperty(value = "企业简称")
    private String enterpriseName;

    @ApiModelProperty(value = "设备编号")
    private String equipmentNo;

    @ApiModelProperty(value = "设备类型：0：网关，1：车桩，2：采集器")
    private Integer equipmentType;

    @ApiModelProperty(value = "设备型号")
    private String equipmentModel;

    @ApiModelProperty(value = "设备蓝牙")
    private String equipmentMac;

    @ApiModelProperty(value = "设备状态：0：正常，1：停用，2：作废")
    private Integer equipmentStatus;

    @ApiModelProperty(value = "电量")
    private Integer electric;

    @ApiModelProperty(value = "软件版本")
    private String softVersion;

    @ApiModelProperty(value = "在线状态：0：离线，1：在线")
    private Integer onlineStatus;

    @ApiModelProperty(value = "区域主键")
    private Integer areaId;

    @ApiModelProperty(value = "区域编号")
    private String areaNo;

    @ApiModelProperty(value = "行政区主键")
    private Integer district;

    @ApiModelProperty(value = "街道主键")
    private Integer street;

    @ApiModelProperty(value = "车辆编号")
    private String vehicleNo;

    @ApiModelProperty(value = "车辆企业编号")
    private String vehicleEnterpriseNo;

    @ApiModelProperty(value = "是否删除 0：未删除 1：:已删除")
    private Integer hasDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;


}
