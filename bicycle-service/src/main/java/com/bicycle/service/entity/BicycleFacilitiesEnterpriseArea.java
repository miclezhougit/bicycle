package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import com.bicycle.common.entity.base.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
 * <p>
 * 企业停放区域信息表
 * </p>
 *
 * @author layne
 * @since 2020-08-15
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "BicycleFacilitiesEnterpriseArea对象", description = "企业停放区域信息表")
public class BicycleFacilitiesEnterpriseArea extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "企业编号")
    private String enterpriseNo;

    @ApiModelProperty(value = "企业简称")
    private String enterpriseName;

    @ApiModelProperty(value = "区域名称")
    private String areaName;

    @ApiModelProperty(value = "区域编号")
    private String areaNo;

    @ApiModelProperty(value = "区域属性：0：普通，1：紧急")
    private Integer areaAttribute;

    @ApiModelProperty(value = "区域类型：0：站点，1：围栏")
    private Integer areaType;

    @ApiModelProperty(value = "停放容量")
    private Integer volume;

    @ApiModelProperty(value = "停放数量")
    private Integer parkNum;

    @ApiModelProperty(value = "面积")
    private Integer area;

    @ApiModelProperty(value = "行政区主键")
    private Integer district;

    @ApiModelProperty(value = "街道主键")
    private Integer street;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "区域状态：0：正常，1：停用")
    private Integer areaStatus;

    @ApiModelProperty(value = "区域坐标点")
    private String areaPoints;

    @ApiModelProperty(value = "经度")
    private String lng;

    @ApiModelProperty(value = "纬度")
    private String lat;

    @ApiModelProperty(value = "是否删除 0：未删除 1：:已删除")
    private Integer hasDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;


}
