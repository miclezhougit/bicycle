package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 企业基础信息表
* </p>
*
* @author layne
* @since 2020-08-06
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleCommonEnterprise对象", description="企业基础信息表")
public class BicycleCommonEnterprise implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "enterprise_id", type = IdType.AUTO)
	private Integer enterpriseId;

    @ApiModelProperty(value = "企业编号")
	private String enterpriseNo;

    @ApiModelProperty(value = "企业全称")
	private String enterpriseName;

    @ApiModelProperty(value = "企业简称")
	private String enterpriseAlias;

    @ApiModelProperty(value = "企业类型 1：共享单车 2：设备企业")
	private Integer enterpriseType;

    @ApiModelProperty(value = "企业注册号")
	private String societyCredit;

    @ApiModelProperty(value = "注册地址")
	private String regAddress;

    @ApiModelProperty(value = "注册城市")
	private String regCity;

    @ApiModelProperty(value = "注册资本")
	private String regCapital;

    @ApiModelProperty(value = "法定代表人")
	private String legalName;

    @ApiModelProperty(value = "注册类型")
	private String legalType;

    @ApiModelProperty(value = "成立日期")
	private Date establishDate;

    @ApiModelProperty(value = "注册日期")
	private Date regDate;

    @ApiModelProperty(value = "联系电话")
	private String contactsPhone;

    @ApiModelProperty(value = "举报电话")
	private String informPhone;

    @ApiModelProperty(value = "办公地址")
	private String cityAddress;

    @ApiModelProperty(value = "联系电话(总部)")
	private String hqPhone;

    @ApiModelProperty(value = "举报电话（总部）")
	private String hqAddress;

    @ApiModelProperty(value = "负责人（深圳）")
	private String principalCity;

    @ApiModelProperty(value = "负责人职务")
	private String position;

    @ApiModelProperty(value = "负责人联系电话")
	private String principalPhone;

    @ApiModelProperty(value = "是否免押金 0:未免押 1：已免押")
	private Integer hasDeposit;

    @ApiModelProperty(value = "押金托管机构全称")
	private String custodianBank;

    @ApiModelProperty(value = "押金托管机构所在城市")
	private String custodianBankCity;

    @ApiModelProperty(value = "押金托管机构地址")
	private String custodianBankAddr;

    @ApiModelProperty(value = "企业状态：0：正常，1：已退出")
	private Integer enterpriseStatus;

    @ApiModelProperty(value = "企业主题色")
	private String color;

    @ApiModelProperty(value = "创建人")
	private String createName;

    @ApiModelProperty(value = "创建人主键")
	private Integer createBy;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "修改人")
	private String updateName;

    @ApiModelProperty(value = "修改人主键")
	private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
	private Date updateTime;


}
