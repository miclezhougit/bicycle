package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 举报记录详情图片表
* </p>
*
* @author layne
* @since 2020-08-06
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleSequenceReportPhoto对象", description="举报记录详情图片表")
public class BicycleSequenceReportPhoto implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "photo_id", type = IdType.AUTO)
	private Integer photoId;

    @ApiModelProperty(value = "图片类型 1：举报图片 2：处理前图片 3：处理后图片")
	private Integer photoType;

    @ApiModelProperty(value = "图片地址")
	private String photoUrl;

    @ApiModelProperty(value = "图片主键")
	private Integer fileId;

    @ApiModelProperty(value = "记录详情主键")
	private Integer detailId;

    @ApiModelProperty(value = "微信原始文件ID")
	private String mediaId;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "处理记录主键")
	private Integer processId;


}
