package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 车辆基础信息表
* </p>
*
* @author layne
* @since 2020-08-08
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleBasicsBicycle对象", description="车辆基础信息表")
public class BicycleBasicsBicycle implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "企业编号")
	private String enterpriseNo;

    @ApiModelProperty(value = "企业名称")
	private String enterpriseName;

    @ApiModelProperty(value = "车辆编号")
	private String vehicleNo;

    @ApiModelProperty(value = "电子标签")
	private String eleLabel;

    @ApiModelProperty(value = "车锁蓝牙")
	private String vehicleMac;

    @ApiModelProperty(value = "使用年限")
	private Integer yearNumber;

    @ApiModelProperty(value = "车辆类型：1：自行车，2：电动车")
	private Integer vehicleType;

    @ApiModelProperty(value = "车辆型号")
	private String vehicleModel;

    @ApiModelProperty(value = "出厂日期")
	private Date factoryDate;

    @ApiModelProperty(value = "车辆状态： 1：正常，2：故障，3：报废")
	private Integer vehicleStatus;

    @ApiModelProperty(value = "车锁状态：0：开锁，1：关锁")
	private Integer lockStatus;

    @ApiModelProperty(value = "车辆经度")
	private String lng;

    @ApiModelProperty(value = "车辆纬度")
	private String lat;

    @ApiModelProperty(value = "行政区")
	private Integer district;

    @ApiModelProperty(value = "街道")
	private Integer street;

    @ApiModelProperty(value = "车辆备案状态： 0：未备案，1：已备案，2：已回收")
	private Integer putOnStatus;

    @ApiModelProperty(value = "车辆是否违停：0：否，1：是")
	private Integer hasIllegalPark;

    @ApiModelProperty(value = "车辆定位时间")
	private Date gnssTime;

    @ApiModelProperty(value = "车辆订单时间")
	private Date orderTime;

    @ApiModelProperty(value = "车辆备案时间")
	private Date putOnTime;

    @ApiModelProperty(value = "车辆回收时间")
	private Date recycleTime;

    @ApiModelProperty(value = "车辆投放批次")
	private String batchNo;

    @ApiModelProperty(value = "城际状态：0：正常，1：流入，2：流出")
	private Integer intercityStatus;

    @ApiModelProperty(value = "车辆保险")
	private String vehicleInsurance;

    @ApiModelProperty(value = "车辆累计骑行次数")
	private Integer vehicleRideTotal;

    @ApiModelProperty(value = "车辆运维次数")
	private Integer vehicleOperateTotal;

    @ApiModelProperty(value = "创建人")
	private String createName;

    @ApiModelProperty(value = "创建人主键")
	private Integer createBy;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "修改人")
	private String updateName;

    @ApiModelProperty(value = "修改人主键")
	private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
	private Date updateTime;


}
