package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import com.bicycle.common.entity.base.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
 * <p>
 * 角色信息表
 * </p>
 *
 * @author layne
 * @since 2020-08-10
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "BicycleSystemRole对象", description = "角色信息表")
public class BicycleSystemRole extends Page implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "role_id", type = IdType.AUTO)
    private Integer roleId;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "角色类型：1：市交通，2：市交警，3：市城管，4：区政府")
    private Integer roleType;

    @ApiModelProperty(value = "角色级别：1：管理员，2：普通")
    private Integer roleLevel;

    @ApiModelProperty(value = "角色状态：1：启用，2：停止")
    private Integer roleStatus;

    @ApiModelProperty(value = "经度")
    private String lng;

    @ApiModelProperty(value = "纬度")
    private String lat;

    @ApiModelProperty(value = "是否删除 0：未删除 1：:已删除")
    private Integer hasDel;

    @ApiModelProperty(value = "单位ID")
    private Integer agencyId;

    @ApiModelProperty(value = "单位名称")
    private String agencyName;

    @ApiModelProperty(value = "区域ID")
    private Integer regionId;

    @ApiModelProperty(value = "区域名称")
    private String regionName;

    @ApiModelProperty(value = "创建人")
    private String createName;

    @ApiModelProperty(value = "创建人主键")
    private Integer createBy;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "修改人")
    private String updateName;

    @ApiModelProperty(value = "修改人主键")
    private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;


}
