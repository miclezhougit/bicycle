package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 车辆位置表
* </p>
*
* @author layne
* @since 2020-08-15
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleDataBicycleSite对象", description="车辆位置表")
public class BicycleDataBicycleSite implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键：主键")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "企业标识：统一分配")
	private String comId;

    @ApiModelProperty(value = "车辆编号：车辆编号")
	private String bicycleNo;

    @ApiModelProperty(value = "所在区编号")
	private String districtCode;

    @ApiModelProperty(value = "定位时间：定位时间")
	private Date gnssTime;

    @ApiModelProperty(value = "车辆坐标经度：车辆位置经度")
	private String lng;

    @ApiModelProperty(value = "车辆坐标纬度：车辆位置纬度")
	private String lat;

    @ApiModelProperty(value = "行政区")
    private Integer district;

    @ApiModelProperty(value = "街道")
    private Integer street;

    @ApiModelProperty(value = "车锁状态：0：开；1：关")
	private Integer lockState;

    @ApiModelProperty(value = "创建日期：创建日期")
	private Date createDate;


}
