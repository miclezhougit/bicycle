package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 举报处理记录表
* </p>
*
* @author layne
* @since 2020-08-06
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleSequenceReportProcess对象", description="举报处理记录表")
public class BicycleSequenceReportProcess implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "记录ID")
	private Integer detailId;

    @ApiModelProperty(value = "手机号码")
	private String mobile;

    @ApiModelProperty(value = "是否属实 0：待核实，1：不属实 2：重复案件，3：属实")
	private Integer hasTrue;

    @ApiModelProperty(value = "处理时间")
	private Date processTime;

    @ApiModelProperty(value = "处理人员主键")
	private Integer processUserId;

    @ApiModelProperty(value = "处理人员姓名")
	private String processUserName;

    @ApiModelProperty(value = "处理企业编号")
	private String processEnterpriseNo;

    @ApiModelProperty(value = "处理企业名称")
	private String processEnterpriseName;

    @ApiModelProperty(value = "处理备注")
	private String remark;

    @ApiModelProperty(value = "推送企业编号")
	private String pushEnterpriseNo;

    @ApiModelProperty(value = "推送企业名称")
	private String pushEnterpriseName;

    @ApiModelProperty(value = "操作类型 1：处理 2：推送企业 3：结案")
	private Integer operateType;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;


}
