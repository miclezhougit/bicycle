package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 用户角色表
* </p>
*
* @author layne
* @since 2020-08-10
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleSystemUserRole对象", description="用户角色表")
public class BicycleSystemUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "系统用户ID")
	private Integer sysUserId;

    @ApiModelProperty(value = "角色ID")
	private Integer roleId;

    @ApiModelProperty(value = "创建人")
	private String createName;

    @ApiModelProperty(value = "创建人主键")
	private Integer createBy;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "修改人")
	private String updateName;

    @ApiModelProperty(value = "修改人主键")
	private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
	private Date updateTime;


}
