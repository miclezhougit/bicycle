package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 公众号用户举报记录表
* </p>
*
* @author layne
* @since 2020-08-06
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleSequenceReportRecord对象", description="公众号用户举报记录表")
public class BicycleSequenceReportRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "report_id", type = IdType.AUTO)
	private Integer reportId;

    @ApiModelProperty(value = "举报编号")
	private String reportNo;

    @ApiModelProperty(value = "举报时间")
	private Date reportTime;

    @ApiModelProperty(value = "举报类型 1：乱停乱放 2：损坏车辆 3：其他")
	private String reportType;

    @ApiModelProperty(value = "举报来源：1：政府巡查，2：群众举报")
	private Integer reportResource;

    @ApiModelProperty(value = "是否违规投放 0：否 1：是")
	private Integer hasViolation;

    @ApiModelProperty(value = "状态  0：未处理 1：已处理")
	private Integer reportStatus;

    @ApiModelProperty(value = "订单类型：1：普通订单 2：紧急订单")
	private Integer orderType;

    @ApiModelProperty(value = "区域主键")
	private Integer districtId;

    @ApiModelProperty(value = "区域名称")
	private String districtName;

    @ApiModelProperty(value = "用户主键")
	private Integer userId;

    @ApiModelProperty(value = "非法企业车辆数")
	private Integer illegalVehicle;

    @ApiModelProperty(value = "举报内容")
	private String reportContent;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;


}
