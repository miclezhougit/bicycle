package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 菜单信息表
* </p>
*
* @author layne
* @since 2020-08-10
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleSystemMenu对象", description="菜单信息表")
public class BicycleSystemMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "menu_id", type = IdType.AUTO)
	private Integer menuId;

    @ApiModelProperty(value = "菜单名")
	private String menuName;

    @ApiModelProperty(value = "父级菜单id")
	private Integer parentId;

    @ApiModelProperty(value = "菜单所对应的类型：1：按钮，2：菜单")
	private Integer menuType;

    @ApiModelProperty(value = "菜单描述")
	private String menuDesc;

    @ApiModelProperty(value = "菜单所对应的路径")
	private String url;

    @ApiModelProperty(value = "状态：1：启用，2：停止")
	private Integer state;

    @ApiModelProperty(value = "图标")
	private String icon;

    @ApiModelProperty(value = "排序")
	private Integer sort;

    @ApiModelProperty(value = "创建人")
	private String createName;

    @ApiModelProperty(value = "创建人主键")
	private Integer createBy;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;

    @ApiModelProperty(value = "修改人")
	private String updateName;

    @ApiModelProperty(value = "修改人主键")
	private Integer updateBy;

    @ApiModelProperty(value = "修改时间")
	private Date updateTime;


}
