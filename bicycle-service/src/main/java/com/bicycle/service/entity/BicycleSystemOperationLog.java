package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 系统日志信息表
* </p>
*
* @author layne
* @since 2020-08-10
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleSystemOperationLog对象", description="系统日志信息表")
public class BicycleSystemOperationLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "log_id", type = IdType.AUTO)
	private Integer logId;

    @ApiModelProperty(value = "用户ID")
	private Integer sysUserId;

    @ApiModelProperty(value = "用户账户")
	private String account;

    @ApiModelProperty(value = "访问ip")
	private String ip;

    @ApiModelProperty(value = "访问url")
	private String url;

    @ApiModelProperty(value = "操作菜单")
	private String menu;

    @ApiModelProperty(value = "数据结构")
	private String dataStruts;

    @ApiModelProperty(value = "操作描述")
	private String operateDesc;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;


}
