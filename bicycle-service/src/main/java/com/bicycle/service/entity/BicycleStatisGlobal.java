package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**
* <p>
* 全量数据统计表
* </p>
*
* @author layne
* @since 2020-08-15
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleStatisGlobal对象", description="全量数据统计表")
public class BicycleStatisGlobal implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "统计日期")
	private Date statisDate;

    @ApiModelProperty(value = "统计类型：1：日期，2：小时")
	private Integer statisType;

    @ApiModelProperty(value = "统计小时")
	private String statisHour;

    @ApiModelProperty(value = "订单总数")
	private Integer orderCount;

    @ApiModelProperty(value = "车辆总数")
	private Integer bikeCount;

    @ApiModelProperty(value = "超量告警数")
	private Integer excessCount;

    @ApiModelProperty(value = "违停告警数")
	private Integer illegalParkCount;

    @ApiModelProperty(value = "违规投放数")
    private Integer illegalDeliveryCount;

    @ApiModelProperty(value = "执法案件数")
	private Integer enforceCaseCount;

    @ApiModelProperty(value = "企业编号")
	private String enterpriseNo;

    @ApiModelProperty(value = "任务描述")
	private String taskDesc;

    @ApiModelProperty(value = "创建时间")
	private Date createTime;


}
