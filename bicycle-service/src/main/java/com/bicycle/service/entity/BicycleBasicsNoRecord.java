package com.bicycle.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
* <p>
* 未备案车辆信息表
* </p>
*
* @author layne
* @since 2020-08-15
*/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="BicycleBasicsNoRecord对象", description="未备案车辆信息表")
public class BicycleBasicsNoRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键：主键")
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;

    @ApiModelProperty(value = "企业唯一标识")
    private String comId;

    @ApiModelProperty(value = "车辆编号：车辆编号")
	private String bicycleNo;

    @ApiModelProperty(value = "车辆经度")
	private String lng;

    @ApiModelProperty(value = "车辆纬度")
	private String lat;

    @ApiModelProperty(value = "行政区域")
	private Integer district;

    @ApiModelProperty(value = "街道")
	private Integer street;

    @ApiModelProperty(value = "创建日期：创建日期")
	private Date createDate;


}
