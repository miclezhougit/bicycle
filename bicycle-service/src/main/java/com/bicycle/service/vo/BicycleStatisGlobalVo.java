package com.bicycle.service.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Collection;
import java.util.Date;

/**
 * @author : layne
 * @Date: 2020/8/15
 * @Time: 15:56
 * Description:
 */
@Data
public class BicycleStatisGlobalVo {

    @ApiModelProperty(value = "统计日期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date statisDate;

    @ApiModelProperty(value = "统计小时")
    private String statisHour;

    @ApiModelProperty(value = "各企业数据")
    private Collection colletions;

    @Data
    public static class BicycleStatisGlobalEnterpriseVo {
        @ApiModelProperty(value = "统计数量")
        private Integer statisCount;

        @ApiModelProperty(value = "企业编号")
        private String enterpriseNo;

        @ApiModelProperty(value = "企业名称")
        private String enterpriseName;
    }

}
