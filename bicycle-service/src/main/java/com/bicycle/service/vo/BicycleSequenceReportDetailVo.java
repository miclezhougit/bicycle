package com.bicycle.service.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.bicycle.service.entity.BicycleSequenceReportPhoto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/15
 * @Time: 0:00
 * Description:
 */
@Data
public class BicycleSequenceReportDetailVo {

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "detail_id", type = IdType.AUTO)
    private Integer detailId;

    @ApiModelProperty(value = "举报记录详情ID")
    private Integer reportId;

    @ApiModelProperty(value = "举报编号")
    private String reportNo;

    @ApiModelProperty(value = "举报时间")
    private Date reportTime;

    @ApiModelProperty(value = "举报类型 1：乱停乱放 2：损坏车辆 3：其他 4：违规投放，5：区域超量")
    private String[] reportType;

    @ApiModelProperty(value = "举报内容")
    private String reportContent;

    @ApiModelProperty(value = "举报来源：1：政府巡查，2：群众举报，3：监管平台")
    private Integer reportResource;

    @ApiModelProperty(value = "状态  0：未处理 1：已退回，2：已处理，3：已结案")
    private Integer reportStatus;

    @ApiModelProperty(value = "订单类型：1：普通订单 2：紧急订单")
    private Integer orderType;

    @ApiModelProperty(value = "企业编号")
    private String enterpriseNo;

    @ApiModelProperty(value = "企业名称")
    private String enterpriseName;

    @ApiModelProperty(value = "区域主键")
    private Integer districtId;

    @ApiModelProperty(value = "区域名称")
    private String districtName;

    @ApiModelProperty(value = "街道主键")
    private Integer streetId;

    @ApiModelProperty(value = "街道名称")
    private String streetName;

    @ApiModelProperty(value = "用户主键")
    private Integer userId;

    @ApiModelProperty(value = "是否属实 0：待核实，1：不属实 2：重复案件，3：属实")
    private Integer hasTrue;

    @ApiModelProperty(value = "是否违规投放 0：否 1：是")
    private Integer hasViolation;

    @ApiModelProperty(value = "经度")
    private String lng;

    @ApiModelProperty(value = "纬度")
    private String lat;

    @ApiModelProperty(value = "详细地址")
    private String detailAddress;

    @ApiModelProperty(value = "车辆编号")
    private String vehicleNo;

    @ApiModelProperty(value = "车辆数量")
    private Integer vehicleCount;

    @ApiModelProperty(value = "处理时间")
    private Date processTime;

    @ApiModelProperty(value = "处理人员主键")
    private Integer processUserId;

    @ApiModelProperty(value = "处理人员姓名")
    private String processUserName;

    @ApiModelProperty(value = "处理备注")
    private String remark;

    @ApiModelProperty(value = "重复案件编号")
    private String repeatCaseNo;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "结案时间")
    private Date closeTime;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    @ApiModelProperty(value = "调度数量")
    private Integer dispatchNum;

    @ApiModelProperty(value = "是否处理超时：0：未超时，1：已超时")
    private Integer hasOvertime;

    @ApiModelProperty(value = "告警编号")
    private String alarmNo;

    @ApiModelProperty(value = "是否智能案件：0：否，1：是")
    private Integer hasIntelligence;

    @ApiModelProperty(value = "非法车辆")
    List<String> illegal;

    @ApiModelProperty(value = "合法车辆")
    List<String> legal;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "举报图片")
    private List<BicycleSequenceReportPhoto> reportImages;

    @ApiModelProperty(value = "处理前图片")
    private List<BicycleSequenceReportPhoto> preImages;

    @ApiModelProperty(value = "处理后图片")
    private List<BicycleSequenceReportPhoto> afterImages;

}
