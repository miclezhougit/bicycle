package com.bicycle.service.vo;

import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/15
 * @Time: 10:27
 * Description:
 */
@Data
public class BicycleFacilitiesMapChartVo {

    private Integer regionId;

    private String regionName;

    /**
     * 站点数量
     */
    private Integer siteCount;
    /**
     * 围栏数量
     */
    private Integer fenceCount;

    /**
     * 车桩数量
     */
    private Integer rackCount;
}
