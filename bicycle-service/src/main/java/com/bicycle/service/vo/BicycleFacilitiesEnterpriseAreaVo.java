package com.bicycle.service.vo;

import com.bicycle.service.entity.BicycleFacilitiesEnterpriseArea;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/15
 * @Time: 9:51
 * Description:
 */
@Data
public class BicycleFacilitiesEnterpriseAreaVo extends BicycleFacilitiesEnterpriseArea {

    private String districtName;

    private String streetName;

}
