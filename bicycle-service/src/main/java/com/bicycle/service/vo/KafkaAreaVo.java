package com.bicycle.service.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author : layne
 * @Date: 2020/9/4
 * @Time: 23:30
 * Description:
 */
@Data
public class KafkaAreaVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "区域名称")
    private String areaName;

    @ApiModelProperty(value = "区域编号")
    private String areaNo;

    @ApiModelProperty(value = "停放容量")
    private Integer parkValue;

    @ApiModelProperty(value = "监控时段")
    private String monitorTime;

    @ApiModelProperty(value = "有效期开始时间")
    private Date periodBegin;

    @ApiModelProperty(value = "有效期结束时间")
    private Date periodEnd;

    @ApiModelProperty(value = "区域状态：0：正常，1：规划中，2：停用，3：废除")
    private Integer areaStatus;

    @ApiModelProperty(value = "经度")
    private String lng;

    @ApiModelProperty(value = "纬度")
    private String lat;

    @ApiModelProperty(value = "区域坐标点")
    private String areaPoints;

    @ApiModelProperty(value = "区域宽度（单位米）")
    private BigDecimal areaWide;

    @ApiModelProperty(value = "区域长度（单位米）")
    private BigDecimal areaLength;

    @ApiModelProperty(value = "区域形式：0：斜列式，1：垂直式")
    private Integer areaModel;

    @ApiModelProperty(value = "区域位置")
    private String areaAddress;


}
