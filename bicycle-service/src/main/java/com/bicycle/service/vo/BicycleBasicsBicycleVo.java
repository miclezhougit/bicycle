
package com.bicycle.service.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * @Description: 备案车辆信息
 * @Author: zhouzm
 * @Date: 2021/7/11 20:41
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BicycleBasicsBicycleVo implements Serializable {

    /**
     * 意义，目的和功能，以及被用到的地方<br>
     */
    private static final long serialVersionUID = 1L;

    // 企业编号
    private String enterpriseNo;

    // 开始时间
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private transient Date beginTime;

    // 结束时间
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private transient Date endTime;

}
