/*
 * 文件名：BicycleSystemUserLoginVo.java 版权：Copyright by 联通系统集成有限公司 描述： 修改人：焦凯旋 修改时间：2019年6月21日 跟踪单号： 修改单号： 修改内容：
 */

package com.bicycle.service.vo;

import com.bicycle.service.entity.BicycleSystemUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class BicycleSystemUserLoginVo extends BicycleSystemUser {

	/**
	 * 意义，目的和功能，以及被用到的地方<br>
	 */
	private static final long serialVersionUID = 1L;

	// 主键
	private Integer roleId;

	// 角色名称
	private String roleName;

	// 角色类型：1：市交通，2：市交警，3：市城管，4：区政府
	private Integer roleType;

	// 角色级别：1：管理员，2：普通
	private Integer roleLevel;

	// 单位ID
	private Integer agencyId;

	// 单位名称
	private String agencyName;

	// 登录token
	private String token;

	@ApiModelProperty(value = "经度")
	private String lng;

	@ApiModelProperty(value = "纬度")
	private String lat;

}
