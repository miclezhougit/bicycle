/*
 * 文件名：SystemMenuTreeResp.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月24日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.service.vo;

import com.bicycle.service.entity.BicycleSystemMenu;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class BicycleSystemMenuTreeVo extends BicycleSystemMenu {
    /**
     * 意义，目的和功能，以及被用到的地方<br>
     */
    private static final long serialVersionUID = 1L;

    /**
     * 子集
     */
    private List<BicycleSystemMenuTreeVo> childValues;

}
