package com.bicycle.service.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/16
 * @Time: 0:14
 * Description:
 */
@Data
public class BicycleIndexStatisVo {
    @ApiModelProperty("车辆总数")
    private Integer bicycleCount = 0;

    @ApiModelProperty("订单总数")
    private Integer orderCount = 0;

    @ApiModelProperty("超量总数")
    private Integer excessCount = 0;

    @ApiModelProperty("违停总数")
    private Integer illegalParkCount = 0;

    @ApiModelProperty("总量控制数量")
    private Integer bicycleParkCount = 0;

    @ApiModelProperty("实时上报位置车辆数")
    private Integer reportLocationCount = 0;

    @ApiModelProperty("违规投放总数")
    private Integer illegalDeliveryCount = 0;

}
