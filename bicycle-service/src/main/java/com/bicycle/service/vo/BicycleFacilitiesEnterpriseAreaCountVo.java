/*
 * 文件名：BicycleFacilitiesEnterpriseAreaCountVo.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月14日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.service.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class BicycleFacilitiesEnterpriseAreaCountVo implements Serializable {
	/**
	 * 意义，目的和功能，以及被用到的地方<br>
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 站点数量
	 */
	private Integer siteCount;
	/**
	 * 围栏数量
	 */
	private Integer railCount;

	/**
	 * 车桩数量
	 */
	private Integer rackCount;
	/**
	 * 总数
	 */
	private Integer totalCount;

}
