package com.bicycle.service.vo;

import com.bicycle.service.dto.BicycleBasicsAreaVehicleDto;
import com.bicycle.service.entity.BicycleFacilitiesEnterpriseArea;
import com.bicycle.service.entity.BicycleFacilitiesGovArea;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/19
 * @Time: 22:04
 * Description:
 */
@Data
public class BicycleFacilitiesMapVo {

//    private List<BicycleFacilitiesEnterpriseArea> enterpriseAreaList;
//
//    private List<BicycleFacilitiesGovArea> govAreaList;

    @ApiModelProperty(value = "区域坐标点")
    private String areaPoints;

    @ApiModelProperty(value = "区域名称")
    private String areaName;

    @ApiModelProperty(value = "区域编号")
    private String areaNo;

    @ApiModelProperty(value = "经度")
    private String lng;

    @ApiModelProperty(value = "纬度")
    private String lat;

    @ApiModelProperty("区域属性：0：停放区，1：监控区, 2:电子围栏")
    private Integer type;

    @ApiModelProperty(value = "停放容量")
    private Integer parkValue;

    @ApiModelProperty(value = "停放数量")
    private Integer parkNum;

    @ApiModelProperty(value = "面积")
    private Integer area;

    @ApiModelProperty(value = "区域位置")
    private String areaAddress;

    private List<BicycleBasicsAreaVehicleDto> vehicleDtoList;

}
