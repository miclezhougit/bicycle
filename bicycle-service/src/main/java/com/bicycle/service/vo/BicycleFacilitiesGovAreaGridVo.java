/*
 * 文件名：BicycleFacilitiesGovAreaPageResp.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月19日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.service.vo;

import com.bicycle.service.entity.BicycleFacilitiesGovArea;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class BicycleFacilitiesGovAreaGridVo extends BicycleFacilitiesGovArea {
    /**
     * 意义，目的和功能，以及被用到的地方<br>
     */
    private static final long serialVersionUID = 1L;

    /**
     * 行政区名称
     */
    @ApiModelProperty("行政区名称")
    private String districtName;
    /**
     * 街道名称
     */
    @ApiModelProperty("街道名称")
    private String streetName;
    /**
     * 是否有未处理案件：0：否，1：是
     */
    @ApiModelProperty("是否有未处理案件：0：否，1：是")
    private Integer hasCase;
    /**
     * 是否有告警 0:否，1：是
     */
    @ApiModelProperty("是否有告警 0:否，1：是")
    private Integer hasAlarm;

}
