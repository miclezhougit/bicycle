package com.bicycle.service.vo;

import com.bicycle.service.entity.BicycleFacilitiesEnterpriseEquipment;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/15
 * @Time: 10:10
 * Description:
 */
@Data
public class BicycleFacilitiesEnterpriseEquipmentVo extends BicycleFacilitiesEnterpriseEquipment {

    private String districtName;

    private String streetName;
}
