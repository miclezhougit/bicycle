/*
 * 文件名：BicycleSequenceDetailProcessVo.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月13日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.service.vo;

import com.bicycle.service.entity.BicycleSequenceReportPhoto;
import com.bicycle.service.entity.BicycleSequenceReportProcess;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class BicycleSequenceDetailProcessVo extends BicycleSequenceReportProcess {


    /**
     * 处理前照片
     */
    private List<BicycleSequenceReportPhoto> prePhotos;
    /**
     * 处理后图片
     */
    private List<BicycleSequenceReportPhoto> afterPhotos;
}
