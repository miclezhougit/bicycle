package com.bicycle.service.cache;

import com.bicycle.common.enums.RegionTypeEnum;
import com.bicycle.service.entity.BicycleCommonRegion;
import com.bicycle.service.service.IBicycleCommonRegionService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 初始化带坐标的区域数据
 *
 * @author 程昂
 * @version 2019年8月8日
 * @see CommonDataCache
 * @since
 */
@Slf4j
@Component
public class ReginDataCache implements CommandLineRunner {

    @Autowired
    private IBicycleCommonRegionService regionService;

    private static List<RegionRelation> regionRelationList = null;

    private Map<Integer, RegionRelation> regionMap = new LinkedHashMap<Integer, RegionRelation>();


    /**
     * 初始化所有行政区域
     *
     * @see
     */
    private void initRegion() {
        log.warn(">>>>>>>>>>>>>>>>>>RtcReginDataCache 初始化区域数据开始");
        List<BicycleCommonRegion> regionList = regionService.list();
        // 循环区
        for (BicycleCommonRegion region : regionList) {
            if (region.getRegionType().intValue() == RegionTypeEnum.REGION_TYPE_DISTRICT.getKey()) {
                if (!regionMap.containsKey(region.getId())) {
                    RegionRelation regionRelation = new RegionRelation();
                    regionRelation.setParentRegion(region);
                    regionMap.put(region.getId(), regionRelation);
                    continue;
                }
            }
        }
        // 循环街道
        for (BicycleCommonRegion region : regionList) {
            if (region.getRegionType().intValue() == RegionTypeEnum.REGION_TYPE_STREET.getKey()) {
                RegionRelation relation = regionMap.get(region.getParentId());
                relation.getRegion().add(region);
                continue;
            }
        }
        regionRelationList = new ArrayList<RegionRelation>(regionMap.values());
        log.warn(">>>>>>>>>>>>>>>>>>RtcReginDataCache 初始化区域数据完成");
    }


    @Override
    public void run(String... args) throws Exception {
        this.initRegion();
    }

    public static List<RegionRelation> getRegionRelationList() {
        return regionRelationList;
    }
}
