package com.bicycle.service.cache;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bicycle.common.enums.DataCacheKeyEnum;
import com.bicycle.common.enums.EnterpriseTypeEnum;
import com.bicycle.common.enums.RegionTypeEnum;
import com.bicycle.common.helper.RedisStringHelper;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.service.entity.BicycleCommonRegion;
import com.bicycle.service.entity.BicycleFacilitiesGovArea;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class DataCacheOprator {
    private static DataCacheOprator instance = new DataCacheOprator();
    private RedisStringHelper redisStringHelper;
    // 企业基本数据
    private Map<String, BicycleCommonEnterprise> enterpriseMap = new ConcurrentHashMap<String, BicycleCommonEnterprise>();
    // 行政区街道基本数据
    private Map<Integer, BicycleCommonRegion> regionMap = new ConcurrentHashMap<Integer, BicycleCommonRegion>();
    // 政府区域数据
    private Map<Integer, BicycleFacilitiesGovArea> govAreaMap = new ConcurrentHashMap<Integer, BicycleFacilitiesGovArea>();
    // 行政区和行政区下的街道数据
    private Map<Integer, RegionRelation> regionRelationMap = new ConcurrentHashMap<Integer, RegionRelation>();

    private List<RegionRelation> regionRelationList = Collections.synchronizedList(new ArrayList<RegionRelation>());

    // 所有企业基本数据
    private Map<String, BicycleCommonEnterprise> enterpriseAllMap = new ConcurrentHashMap<String, BicycleCommonEnterprise>();

    private DataCacheOprator() {

    }

    public static DataCacheOprator getInstance() {
        return instance;
    }

    public void setRedisStringHelper(RedisStringHelper redisStringHelper) {
        if (this.redisStringHelper == null) {
            this.redisStringHelper = redisStringHelper;
        }
    }

    /**
     * 根据企业ID获取企业
     *
     * @param enterpriseNo
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public BicycleCommonEnterprise getEnterprise(String enterpriseNo) {
        if (enterpriseMap.size() == 0) {
            String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_ENTERPRISE.getKey());
            List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
            for (JSONObject object : JSONObjectList) {
                BicycleCommonEnterprise enterprise = object.toJavaObject(BicycleCommonEnterprise.class);
                enterpriseMap.put(enterprise.getEnterpriseNo(), enterprise);
            }
        }
        return enterpriseMap.get(enterpriseNo);
    }

    /**
     * 根据企业ID获取企业 包含已退出企业
     *
     * @param enterpriseNo
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public BicycleCommonEnterprise getAllEnterprise(String enterpriseNo) {
        if (enterpriseAllMap.size() == 0) {
            String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_ALL_ENTERPRISE.getKey());
            List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
            for (JSONObject object : JSONObjectList) {
                BicycleCommonEnterprise enterprise = object.toJavaObject(BicycleCommonEnterprise.class);
                enterpriseAllMap.put(enterprise.getEnterpriseNo(), enterprise);
            }
        }
        return enterpriseAllMap.get(enterpriseNo);
    }

    /**
     * 获取所有的企业
     *
     * @param enterpriseNo
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public List<BicycleCommonEnterprise> getEnterpriseList() {
        List<BicycleCommonEnterprise> enterpriseList = new ArrayList<BicycleCommonEnterprise>();
        if (enterpriseMap.size() == 0) {
            String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_ENTERPRISE.getKey());
            List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
            for (JSONObject object : JSONObjectList) {
                BicycleCommonEnterprise enterprise = object.toJavaObject(BicycleCommonEnterprise.class);
                if (enterprise.getEnterpriseType().intValue() == EnterpriseTypeEnum.ENTERPRISE_TYPE_1.getKey()) {
                    enterpriseMap.put(enterprise.getEnterpriseNo(), enterprise);
                }
            }
        }
        enterpriseMap.forEach((key, value) -> {
            enterpriseList.add(value);
        });
        return enterpriseList;
    }

    /**
     * 获取所有的企业 包含已退出企业
     *
     * @param enterpriseNo
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public List<BicycleCommonEnterprise> getAllEnterpriseList() {
        List<BicycleCommonEnterprise> enterpriseList = new ArrayList<BicycleCommonEnterprise>();
        if (enterpriseAllMap.size() == 0) {
            String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_ALL_ENTERPRISE.getKey());
            List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
            for (JSONObject object : JSONObjectList) {
                BicycleCommonEnterprise enterprise = object.toJavaObject(BicycleCommonEnterprise.class);
                enterpriseAllMap.put(enterprise.getEnterpriseNo(), enterprise);
            }
        }
        enterpriseAllMap.forEach((key, value) -> {
            enterpriseList.add(value);
        });
        return enterpriseList;
    }

    /**
     * 根据行政区ID或者街道ID获取行政区或者街道
     *
     * @param areaId
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public BicycleCommonRegion getRegion(int areaId) {
        if (regionMap.size() == 0) {
            String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_REGION.getKey());
            List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
            for (JSONObject object : JSONObjectList) {
                BicycleCommonRegion region = object.toJavaObject(BicycleCommonRegion.class);
                regionMap.put(region.getId(), region);
            }
        }
        return regionMap.get(areaId);
    }

    /**
     * 获取所有行政区
     *
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public List<BicycleCommonRegion> getRegionList() {
        List<BicycleCommonRegion> regionList = new ArrayList<BicycleCommonRegion>();
        String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_REGION.getKey());
        List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
        for (JSONObject object : JSONObjectList) {
            BicycleCommonRegion region = object.toJavaObject(BicycleCommonRegion.class);
            if (region.getRegionType().intValue() == RegionTypeEnum.REGION_TYPE_DISTRICT.getKey()) {
                regionList.add(region);
            }
        }
        return regionList;
    }

    /**
     * 根据行政区ID获取当前行政区以及行政区下的街道
     *
     * @param areaId
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public RegionRelation getRelation(int areaId) {
        if (regionRelationMap.size() == 0) {
            String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_REGION.getKey());
            List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
            for (JSONObject object : JSONObjectList) {
                BicycleCommonRegion region = object.toJavaObject(BicycleCommonRegion.class);
                if (region.getRegionType().intValue() == RegionTypeEnum.REGION_TYPE_DISTRICT.getKey()) {
                    RegionRelation regionRelation = new RegionRelation();
                    regionRelation.setParentRegion(region);
                    regionRelationMap.put(region.getId(), regionRelation);
                    continue;
                }
            }

            for (JSONObject object : JSONObjectList) {
                BicycleCommonRegion region = object.toJavaObject(BicycleCommonRegion.class);
                if (region.getRegionType().intValue() == RegionTypeEnum.REGION_TYPE_STREET.getKey()) {
                    RegionRelation regionRelation = regionRelationMap.get(region.getParentId());
                    regionRelation.getRegion().add(region);
                    regionRelationMap.put(region.getParentId(), regionRelation);
                    continue;
                }
            }
        }
        return regionRelationMap.get(areaId);
    }

    /**
     * 获取所有行政区和行政区下的街道
     *
     * @param areaId
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public List<RegionRelation> getRelationList() {
        if (regionRelationList.size() == 0) {
            String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_REGION.getKey());
            List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
            for (JSONObject object : JSONObjectList) {
                BicycleCommonRegion region = object.toJavaObject(BicycleCommonRegion.class);
                if (region.getRegionType().intValue() == RegionTypeEnum.REGION_TYPE_DISTRICT.getKey()) {
                    RegionRelation regionRelation = this.getRelation(region.getId());
                    regionRelationList.add(regionRelation);
                    continue;
                }
            }

            // for (JSONObject object : JSONObjectList) {
            // CommonRegion region = object.toJavaObject(CommonRegion.class);
            // if (region.getRegionType().intValue() == RegionTypeEnum.REGION_TYPE_STREET.getKey()) {
            // RegionRelation regionRelation = this.getRelation(region.getParentId());
            // regionRelation.getRegion().add(region);
            // continue;
            // }
            // }
        }
        return regionRelationList;
    }

    /**
     * 根据区域ID获取区域
     *
     * @param govAreaId
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public BicycleFacilitiesGovArea getGovArea(int govAreaId) {
        if (govAreaMap.size() == 0) {
            String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_GOVAREA.getKey());
            List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
            for (JSONObject object : JSONObjectList) {
                BicycleFacilitiesGovArea govArea = object.toJavaObject(BicycleFacilitiesGovArea.class);
                govAreaMap.put(govArea.getId(), govArea);
            }
        }
        return govAreaMap.get(govAreaId);
    }

    /**
     * 根据条件获取政府区域
     *
     * @param areaType
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    public List<BicycleFacilitiesGovArea> getGovAreaList(BicycleFacilitiesGovArea parmGovArea) {
        List<BicycleFacilitiesGovArea> govAreaList = new ArrayList<BicycleFacilitiesGovArea>();
        String jsonStr = redisStringHelper.get(DataCacheKeyEnum.CACHE_GOVAREA.getKey());
        if (StringUtils.isNotBlank(jsonStr)) {
            List<JSONObject> JSONObjectList = JSON.parseObject(jsonStr, ArrayList.class);
            for (JSONObject object : JSONObjectList) {
                BicycleFacilitiesGovArea govArea = object.toJavaObject(BicycleFacilitiesGovArea.class);

                // 筛选当前行政区内的区域
                if (parmGovArea.getAreaAttribute() == null) {
                    govAreaList.add(govArea);
                    continue;
                }
                // 筛选当前行政区内，指定类型的政府划分区域
                if (govArea.getAreaAttribute().intValue() == parmGovArea.getAreaAttribute().intValue()) {
                    govAreaList.add(govArea);
                    continue;
                }

            }
        }
        return govAreaList;
    }

    /**
     * 重新加载区域信息
     *
     * @param regionList
     * @see
     */
    public void reloadRegion(List<BicycleCommonRegion> regionList) {
        for (BicycleCommonRegion region : regionList) {
            region.setRegionPoints(null);
        }
        redisStringHelper.set(DataCacheKeyEnum.CACHE_REGION.getKey(), JSON.toJSONString(regionList));
        regionMap.clear();
        regionRelationMap.clear();
        regionRelationList.clear();
    }

    /**
     * 重新加载政府区域信息
     *
     * @param govAreaList
     * @see
     */
    public void reloadGovArea(List<BicycleFacilitiesGovArea> govAreaList) {
        redisStringHelper.set(DataCacheKeyEnum.CACHE_GOVAREA.getKey(), JSON.toJSONString(govAreaList));
        govAreaMap.clear();
    }

    /**
     * 重新加载企业信息
     *
     * @param enterpriseList
     * @see
     */
    public void reloadEnterprise(List<BicycleCommonEnterprise> enterpriseList) {
        redisStringHelper.set(DataCacheKeyEnum.CACHE_ENTERPRISE.getKey(), JSON.toJSONString(enterpriseList));
        enterpriseMap.clear();
    }

    /**
     * 重新加载企业信息 包含已退出企业
     *
     * @param enterpriseList
     * @see
     */
    public void reloadAllEnterprise(List<BicycleCommonEnterprise> enterpriseList) {
        redisStringHelper.set(DataCacheKeyEnum.CACHE_ALL_ENTERPRISE.getKey(), JSON.toJSONString(enterpriseList));
        enterpriseAllMap.clear();
    }


}
