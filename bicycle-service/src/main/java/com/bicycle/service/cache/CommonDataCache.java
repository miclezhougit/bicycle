package com.bicycle.service.cache;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.enums.AreaStatusEnum;
import com.bicycle.common.enums.DataCacheKeyEnum;
import com.bicycle.common.enums.EnterpriseStatusEnum;
import com.bicycle.common.enums.HasDelEnum;
import com.bicycle.common.helper.RedisStringHelper;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.service.entity.BicycleCommonRegion;
import com.bicycle.service.entity.BicycleFacilitiesGovArea;
import com.bicycle.service.service.IBicycleCommonEnterpriseService;
import com.bicycle.service.service.IBicycleCommonRegionService;

import com.bicycle.service.service.IBicycleFacilitiesGovAreaService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class CommonDataCache implements CommandLineRunner {

    @Autowired
    private IBicycleCommonEnterpriseService enterpriseService;
    @Autowired
    private IBicycleCommonRegionService regionService;
    @Autowired
    private RedisStringHelper redisStringHelper;
    @Autowired
    private IBicycleFacilitiesGovAreaService govAreaService;


    /**
     * 初始化所有企业
     *
     * @see
     */
    private void initEnterprise() {
        BicycleCommonEnterprise qEnterprise = new BicycleCommonEnterprise();
        qEnterprise.setEnterpriseStatus(EnterpriseStatusEnum.ENTERPRISE_STATUS_0.getKey());
        List<BicycleCommonEnterprise> enterpriseList = enterpriseService.list(new QueryWrapper<BicycleCommonEnterprise>(qEnterprise));
        if (redisStringHelper.exists(DataCacheKeyEnum.CACHE_ENTERPRISE.getKey())) {
            redisStringHelper.del(DataCacheKeyEnum.CACHE_ENTERPRISE.getKey());
        }
        if (CollectionUtils.isNotEmpty(enterpriseList)) {
            redisStringHelper.set(DataCacheKeyEnum.CACHE_ENTERPRISE.getKey(), JSON.toJSONString(enterpriseList));
        }
        qEnterprise.setEnterpriseStatus(null);
        enterpriseList = enterpriseService.list(new QueryWrapper<BicycleCommonEnterprise>(qEnterprise));
        if (redisStringHelper.exists(DataCacheKeyEnum.CACHE_ALL_ENTERPRISE.getKey())) {
            redisStringHelper.del(DataCacheKeyEnum.CACHE_ALL_ENTERPRISE.getKey());
        }
        if (CollectionUtils.isNotEmpty(enterpriseList)) {
            redisStringHelper.set(DataCacheKeyEnum.CACHE_ALL_ENTERPRISE.getKey(), JSON.toJSONString(enterpriseList));
        }

    }

    /**
     * 初始化所有行政区域
     *
     * @see
     */
    private void initRegion() {
        List<BicycleCommonRegion> regionList = regionService.list();
        if (redisStringHelper.exists(DataCacheKeyEnum.CACHE_REGION.getKey())) {
            redisStringHelper.del(DataCacheKeyEnum.CACHE_REGION.getKey());
        }
        if (CollectionUtils.isNotEmpty(regionList)) {
            for (BicycleCommonRegion region : regionList) {
                region.setRegionPoints(null);
            }
            redisStringHelper.set(DataCacheKeyEnum.CACHE_REGION.getKey(), JSON.toJSONString(regionList));
        }
    }

    /**
     * 初始化政府区域
     *
     * @see
     */
    private void initGovArea() {
        BicycleFacilitiesGovArea qGovArea = new BicycleFacilitiesGovArea();
        qGovArea.setAreaStatus(AreaStatusEnum.AREA_STATUS_0.getKey());
        qGovArea.setHasDel(HasDelEnum.HAS_DEL_FALSE.getKey());
        List<BicycleFacilitiesGovArea> govAreaList = govAreaService.list(new QueryWrapper<BicycleFacilitiesGovArea>(qGovArea));
        if (redisStringHelper.exists(DataCacheKeyEnum.CACHE_GOVAREA.getKey())) {
            redisStringHelper.del(DataCacheKeyEnum.CACHE_GOVAREA.getKey());
        }
        if (CollectionUtils.isNotEmpty(govAreaList)) {
            redisStringHelper.set(DataCacheKeyEnum.CACHE_GOVAREA.getKey(), JSON.toJSONString(govAreaList));
        }
    }

    /**
     * 初始化政府单位
     *
     * @see
     */
//    private void initGovAgency() {
//        List<RtcCommonGovAgency> govAgencieList = govAgencyService.selectRtcCommonGovAgencyList(RtcCommonGovAgency.builder().build());
//
//        if (redisStringHelper.exists(DataCacheKeyEnum.CACHE_GOVAGENCY.getKey())) {
//            redisStringHelper.del(DataCacheKeyEnum.CACHE_GOVAGENCY.getKey());
//        }
//        if (CollectionUtils.isNotEmpty(govAgencieList)) {
//            redisStringHelper.set(DataCacheKeyEnum.CACHE_GOVAGENCY.getKey(), JSON.toJSONString(govAgencieList));
//        }
//    }

    @Override
    public void run(String... args) throws Exception {
        this.initEnterprise();
        this.initRegion();
        this.initGovArea();
    }
}
