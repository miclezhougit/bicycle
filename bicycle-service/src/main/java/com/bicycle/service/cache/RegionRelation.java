package com.bicycle.service.cache;

import com.bicycle.service.entity.BicycleCommonRegion;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class RegionRelation implements java.io.Serializable {

    /**
     * 意义，目的和功能，以及被用到的地方<br>
     */
    private static final long serialVersionUID = 1L;

    private BicycleCommonRegion parentRegion;

    private List<BicycleCommonRegion> region = new ArrayList<BicycleCommonRegion>();

}
