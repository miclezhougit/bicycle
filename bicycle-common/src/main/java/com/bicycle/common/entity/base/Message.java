package com.bicycle.common.entity.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Message<T> {
	/** 错误码 **/
	private int code = 200;
	/** 返回消息 **/
	private String message = "success";
	/** 返回结果 **/
	private T result;

}