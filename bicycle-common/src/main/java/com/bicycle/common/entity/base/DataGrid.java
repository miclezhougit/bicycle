package com.bicycle.common.entity.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@SuppressWarnings("rawtypes")
public class DataGrid<E> extends Message {

	/** 总记录数 **/
	private long total;
	/** 数据集 **/
	private Collection<E> rows;
}
