package com.bicycle.common.entity.base;

import com.baomidou.mybatisplus.annotation.TableField;

/**
 * 翻页请求实体类
 *
 */
public class Page implements java.io.Serializable {
	/**
	 * 意义，目的和功能，以及被用到的地方<br>
	 */
	private static final long serialVersionUID = 2365517291924329476L;
	/** 当前页 **/
	@TableField(exist = false)
	private int page;
	/** 每页条数 **/
	@TableField(exist = false)
	private int rows;
	/** 起始行号 **/
	@TableField(exist = false)
	private Integer pageNo;
	/** 每页条数 **/
	@TableField(exist = false)
	private Integer pageSize;
	/** 排序条件 **/
	@TableField(exist = false)
	private String orderByClause;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public Integer getPageNo() {
		pageNo = (page - 1) * rows;
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		pageSize = rows;
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

}
