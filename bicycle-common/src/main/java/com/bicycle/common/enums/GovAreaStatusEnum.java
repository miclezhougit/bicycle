/*
 * 文件名：GovAreaStatusEnum.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月3日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.common.enums;

public enum GovAreaStatusEnum {
	
	AREA_STATUS_NORMAL(0,"正常"),
	AREA_STATUS_PLAN(1,"规划"),
	AREA_STATUS_PAUSE(2,"停用"),
	AREA_STATUS_ABOLISH(3,"废除");
	
	private Integer key;
	private String value;

	private GovAreaStatusEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
