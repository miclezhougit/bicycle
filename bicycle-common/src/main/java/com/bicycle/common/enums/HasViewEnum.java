package com.bicycle.common.enums;

public enum HasViewEnum {
	HAS_VIEW_FALSE(0, "不展示"), 
	HAS_VIEW_TRUE(1, "展示");

	private Integer key;
	private String value;

	private HasViewEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
