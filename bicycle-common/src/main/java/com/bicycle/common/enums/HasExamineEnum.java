package com.bicycle.common.enums;

public enum HasExamineEnum {
	HAS_EXAMINE_FALSE(0, "不参与"), 
	HAS_EXAMINE_TRUE(1, "参与");

	private Integer key;
	private String value;

	private HasExamineEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
