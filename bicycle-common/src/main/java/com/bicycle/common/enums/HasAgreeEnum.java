package com.bicycle.common.enums;

public enum HasAgreeEnum {
	HAS_AGREE_TRUE(1, "同意"), HAS_AGREE_FALSE(2, "不同意");

	private Integer key;
	private String value;

	private HasAgreeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
