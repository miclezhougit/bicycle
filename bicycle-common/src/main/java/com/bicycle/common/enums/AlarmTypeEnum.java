
package com.bicycle.common.enums;

public enum AlarmTypeEnum {

	ALARM_TYPE_0(0, "违规投放"),
	ALARM_TYPE_1(1, "区域超量"),
	ALARM_TYPE_2(2, "乱停乱放");

	private Integer key;
	private String value;
	
	private AlarmTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}
	public Integer getKey() {
		return key;
	}
	public void setKey(Integer key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
