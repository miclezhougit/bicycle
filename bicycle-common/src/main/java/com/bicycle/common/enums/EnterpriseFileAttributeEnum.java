
package com.bicycle.common.enums;

public enum EnterpriseFileAttributeEnum {

	FILE_ATTR_PUTON(0,"备案"),
	FILE_ATTR_EXIT(1,"退出");
	
	private Integer key;
	private String value;
	private EnterpriseFileAttributeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}
	public Integer getKey() {
		return key;
	}
	public void setKey(Integer key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
