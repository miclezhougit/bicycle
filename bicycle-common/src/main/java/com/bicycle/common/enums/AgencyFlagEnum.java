package com.bicycle.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public enum AgencyFlagEnum {
    AGENCY_FLAG_TRANSPORT("transport", "市交通局"),
    AGENCY_FLAG_TRAFFIC("traffic", "市交警局"),
    AGENCY_FLAG_CITYMANAGE("citymanage", "市城管局"),
    AGENCY_FLAG_AREAGOV("areagov", "区政府");

    @Getter
    @Setter
    private String key;
    @Getter
    @Setter
    private String value;
}
