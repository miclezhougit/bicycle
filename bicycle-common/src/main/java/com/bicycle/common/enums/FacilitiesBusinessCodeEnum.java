/*
 * 文件名：FacilitiesBusinessCodeEnum.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月3日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.common.enums;

/**
 * 基础设施业务状态枚举</br>
 * 范围 2000~2499
 * 
 * @author 焦凯旋
 * @version 2019年6月3日
 * @see FacilitiesBusinessCodeEnum
 * @since
 */
public enum FacilitiesBusinessCodeEnum {

	CODE_INFO_ALREADY_EXISTS(2000, "信息已存在"),
	CODE_NUMBER_ALREADY_EXISTS(2001, "编码信息已存在"),
	CODE_NAME_ALREADY_EXISTS(2002, "名称信息已存在");

	private Integer key;

	private String value;

	private FacilitiesBusinessCodeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
