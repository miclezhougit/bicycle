/*
 * 文件名：GovAreaTypeEnum.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月3日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */


package com.bicycle.common.enums;

public enum GovAreaTypeEnum {
	
	AREA_TYPE_0(0,"停放区"),
	AREA_TYPE_1(1,"禁停区"),
	AREA_TYPE_2(2,"规范停放区"),
	AREA_TYPE_3(3,"动态调节区"),
	AREA_TYPE_4(4,"临时管控区");
	
	private Integer key;
	private String value;

	private GovAreaTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
