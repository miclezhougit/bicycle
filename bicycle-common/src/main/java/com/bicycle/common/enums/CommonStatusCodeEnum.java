/*
 * 文件名：CommonStatusCodeEnum.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月3日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.common.enums;

/**
 * 公共状态码枚举信息</br>
 * 状态码范围 0~999
 * 
 * @author 焦凯旋
 * @version 2019年6月3日
 * @see CommonStatusCodeEnum
 * @since
 */
public enum CommonStatusCodeEnum {
	CODE_200(200, "success"),
	CODE_777(777, "文件类型错误."),
	CODE_888(888, "文件生成中"),
	CODE_999(999, "文件不能大于500M"),
	CODE_500(500, "数据加载中...");

	private Integer key;

	private String value;

	private CommonStatusCodeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
