package com.bicycle.common.enums;

public enum RoleTypeEnum {

	ROLE_TYPE_1(1, "市交通"), 
	ROLE_TYPE_2(2, "市交警"), 
	ROLE_TYPE_3(3, "市城管"), 
	ROLE_TYPE_4(4, "区政府");

	private Integer key;
	private String value;

	private RoleTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
