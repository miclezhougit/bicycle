
package com.bicycle.common.enums;

/**
 * 违规案件处理状态枚举
 * 
 * @author 焦凯旋
 * @version 2019年6月19日
 * @see ReportStatusEnum
 * @since
 */
public enum ReportStatusEnum {

	REPORT_STATUS_0(0, "未处理"),
	REPORT_STATUS_1(1, "已退回"),
	REPORT_STATUS_2(2, "已处理"),
	REPORT_STATUS_3(3, "已结案");
	
	private Integer key;

	private String value;
	
	

	private ReportStatusEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
