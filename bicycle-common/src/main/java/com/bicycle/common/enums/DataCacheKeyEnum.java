package com.bicycle.common.enums;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public enum DataCacheKeyEnum {
	CACHE_ENTERPRISE("bicycle_cache_enterprise", "企业数据缓存key"),
	CACHE_REGION("bicycle_cache_region", "行政区域街道数据缓存key"),
	CACHE_GOVAREA("bicycle_cache_govArea", "政府区域类型缓存key"),
	CACHE_GOVAGENCY("bicycle_cache_govAgency", "政府单位数据缓存key"),
	CACHE_JSAPISIGNATURE("bicycle_cache_jsapisignature", "微信公众号api签名"),
	CACHE_ALL_ENTERPRISE("bicycle_cache_all_enterprise", "所有企业数据缓存key(包含已退出企业)");

	private String key;
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
