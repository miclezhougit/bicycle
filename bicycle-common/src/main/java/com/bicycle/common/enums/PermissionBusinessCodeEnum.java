
package com.bicycle.common.enums;

/**
 * 权限业务状态枚举</br>
 * 范围 2500~2999
 * 
 * @author 焦凯旋
 * @version 2019年6月3日
 * @see PermissionBusinessCodeEnum
 * @since
 */
public enum PermissionBusinessCodeEnum {

	CODE_INFO_ALREADY_EXISTS(2500, "信息已存在"),
	CODE_INFO_PASSWORD_ACCOUNT_ERROR(2501, "账户不存在或密码错误"),
	CODE_INFO_ACCOUNT_STATUS_ERROR(2502, "账户已被禁用"),
	CODE_INFO_ACCOUNT_ROLE_ERROR(2503, "账户未分配角色或角色已禁用"),
	CODE_INFO_LOGIN_EXPIRE_ERROR(2504, "登录失效，重新登录"),
	CODE_INFO_ACCOUNT_PERMISSION_ERROR(2505, "账户无权限"),
	CODE_INFO_KAPTCHA_ERROR(2506, "验证码错误"),
	CODE_INFO_ACCOUNT_STATUS_ABOLISH(2507, "账户已被废除"),
	CODE_INFO_ACCOUNT_ALREADY_EXISTS(2508, "用户名已存在"),
	CODE_INFO_PASSWORD_NUM_ERROR(2509, "今日输入密码错误次数已达上限!"),
	CODE_INFO_ACCOUNT_NONEXISTS(2510, "账户不存在"),
	CODE_INFO_PASSWORD_ERROR(2511, "密码错误,剩余{0}次"),
	CODE_INFO_LOGIN_TOKEN_ERROR(2512, "当前账号已在其他设备上进行登录!"),
	CODE_INFO_PASSWORD_FAIL(2513, "密码错误"),
	CODE_INFO_LOGIN_COUNT_ERROR(2514, "当前用户登录数量已达到最大值!"),
	CODE_INFO_ACCOUNT_STOP_ERROR(2515, "账户已禁用，退出当前登录");

	private Integer key;

	private String value;

	private PermissionBusinessCodeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
