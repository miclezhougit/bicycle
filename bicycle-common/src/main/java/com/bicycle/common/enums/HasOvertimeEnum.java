package com.bicycle.common.enums;

public enum HasOvertimeEnum {
	HAS_OVERTIME_FALSE(0, "未超时"), 
	HAS_OVERTIME_TRUE(1, "已超时");

	private Integer key;
	private String value;

	private HasOvertimeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
