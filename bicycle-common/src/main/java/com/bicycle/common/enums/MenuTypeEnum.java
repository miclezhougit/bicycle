/*
 * 文件名：MenuTypeEnum.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年7月3日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.common.enums;

public enum MenuTypeEnum {
	MENU_TYPE_BUTTON(1, "按钮"), 
	MENU_TYPE_MENU(2, "菜单");

	private Integer key;
	private String value;
	
	private MenuTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}
	public Integer getKey() {
		return key;
	}
	public void setKey(Integer key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
