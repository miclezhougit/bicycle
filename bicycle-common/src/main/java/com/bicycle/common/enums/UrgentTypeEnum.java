package com.bicycle.common.enums;

public enum UrgentTypeEnum {
	URGENT_TYPE_0(0, "一般"), URGENT_TYPE_1(1, "紧急");

	private Integer key;
	private String value;

	private UrgentTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
