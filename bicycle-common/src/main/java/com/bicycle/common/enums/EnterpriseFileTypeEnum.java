
package com.bicycle.common.enums;

public enum EnterpriseFileTypeEnum {

	FILE_TYPE_0(0,"深圳市互联网租赁自行车企业备案表"),
	FILE_TYPE_1(1,"企业法人营业执照"),
	FILE_TYPE_2(2,"经营和办公场所的权属证明或者租赁合同"),
	FILE_TYPE_3(3,"经营管理制度"),
	FILE_TYPE_4(4,"押金、预付资金专用账户证明及押金存管协议"),
	FILE_TYPE_5(5,"人员证明资料"),
	FILE_TYPE_6(6,"提前30日书面通知"),
	FILE_TYPE_7(7,"社会公示"),
	FILE_TYPE_8(8,"押金退还"),
	FILE_TYPE_9(9,"车辆及相关设施清退"),
	FILE_TYPE_10(10,"车辆备案注销");
	
	private Integer key;
	private String value;
	private EnterpriseFileTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}
	public Integer getKey() {
		return key;
	}
	public void setKey(Integer key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
