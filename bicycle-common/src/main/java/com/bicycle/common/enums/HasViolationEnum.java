
package com.bicycle.common.enums;



public enum HasViolationEnum {

	HAS_VIOLATION_FALSE(0, "未违规"), 
	HAS_VIOLATION_TRUE(1, "已违规");

	private Integer key;
	private String value;
	private HasViolationEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}
	public Integer getKey() {
		return key;
	}
	public void setKey(Integer key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
	
}
