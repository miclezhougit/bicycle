package com.bicycle.common.enums;

public enum HasAttachmentEnum {
	HAS_DEL_FALSE(0, "没有附件"), 
	HAS_DEL_TRUE(1, "有附件");

	private Integer key;
	private String value;

	private HasAttachmentEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
