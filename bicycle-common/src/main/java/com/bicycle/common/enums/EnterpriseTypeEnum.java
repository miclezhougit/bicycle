package com.bicycle.common.enums;

public enum EnterpriseTypeEnum {
	ENTERPRISE_TYPE_1(1, "共享单车"), 
	ENTERPRISE_TYPE_2(2, "设备企业");

	private Integer key;
	private String value;

	private EnterpriseTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
