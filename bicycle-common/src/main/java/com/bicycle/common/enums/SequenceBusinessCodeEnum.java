
package com.bicycle.common.enums;

/**
 * 停放秩序业务状态枚举</br>
 * 范围 3000~3499
 * 
 * @author 焦凯旋
 * @version 2019年6月3日
 * @see SequenceBusinessCodeEnum
 * @since
 */
public enum SequenceBusinessCodeEnum {

	CODE_INFO_ALREADY_EXISTS(3000, "信息已存在"),
	CODE_CASE_UNDISPOSED(3001, "案件未处理,不能进行操作"),
	CODE_CASE_PROCESSING(3002, "您所推送的企业已在处理此案件，请勿推送！"),
	CODE_INFO_NOT_EXIST(3003, "信息不存在"),
	CODE_CASE_PROCESSED(3004, "案件已处理"),
	CODE_CASE_NOT_CLOSE(3005, "案件号未结案!"),
	CODE_LOGIN_EXPIRE(3006, "登录失效，重新登录"),
	CODE_PASSWORD_ERROR(3007, "密码错误"),
	CODE_ACCOUNT_ABOLISH_ERROR(3008, "用户已被废除"),
	CODE_ACCOUNT_PAUSE_ERROR(3009, "用户已被禁用"),
	CODE_MOBILE_ALREADY_EXISTS(3010, "手机号已存在");

	private Integer key;

	private String value;

	private SequenceBusinessCodeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
