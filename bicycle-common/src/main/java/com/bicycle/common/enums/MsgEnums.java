package com.bicycle.common.enums;

import lombok.Getter;
import lombok.Setter;

/**
 * topic枚举类
 *
 * @author 程昂
 * @version 2019年5月5日
 * @see MsgEnums
 * @since
 */
public enum MsgEnums {
    MSG_CONVERT_BICYCLESITE("MSG_CONVERT_BICYCLESITE", "车辆位置信息(新)转换成车辆动态信息(旧)"),
    MSG_COMPANY("MSG_COMPANY", "企业基本信息"),
    MSG_BICYCLE("MSG_BICYCLE", "车辆基本信息"),
    MSG_USER("MSG_USER", "用户信息"),
    MSG_ORDER("MSG_ORDER", "订单信息"),
    MSG_ORDER_TRACK("MSG_ORDER_TRACK", "订单轨迹上报"),
    MSG_BICYCLE_SITE("MSG_BICYCLE_SITE", "车辆位置上报"),
    MSG_FORBID_AREA("MSG_FORBID_AREA", "禁停区信息推送"),
    MSG_PARK_AREA("MSG_PARK_AREA", "停放区信息推送");

    @Getter
    @Setter
    private String topicKey;
    @Getter
    @Setter
    private String topicDesc;

    private MsgEnums(String topicKey, String topicDesc) {
        this.topicKey = topicKey;
        this.topicDesc = topicDesc;
    }

}
