package com.bicycle.common.enums;

public enum AreaStatusEnum {
	AREA_STATUS_0(0, "正常"), 
	AREA_STATUS_1(1, "停用");

	private Integer key;
	private String value;

	private AreaStatusEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
