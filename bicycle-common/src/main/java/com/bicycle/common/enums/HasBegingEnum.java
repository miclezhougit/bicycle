package com.bicycle.common.enums;

public enum HasBegingEnum {
	HAS_BEGING_FALSE(0, "否"), 
	HAS_BEGING_TRUE(1, "是");

	private Integer key;
	private String value;

	private HasBegingEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
