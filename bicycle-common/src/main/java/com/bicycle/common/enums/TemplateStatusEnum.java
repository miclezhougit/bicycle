package com.bicycle.common.enums;

public enum TemplateStatusEnum {

	TEMPLATE_STATUS_0(0, "未删除"), 
	TEMPLATE_STATUS_1(1, "已删除");

	private Integer key;

	private String value;

	private TemplateStatusEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
