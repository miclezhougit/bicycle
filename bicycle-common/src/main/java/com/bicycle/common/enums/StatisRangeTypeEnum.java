package com.bicycle.common.enums;

public enum StatisRangeTypeEnum {

	RANGE_TYPE_0(0, 5, 10),
	RANGE_TYPE_1(1, 10, 20),
	RANGE_TYPE_2(2, 20, null);

	private Integer key;

	private Integer minValue;

	private Integer maxValue;

	private StatisRangeTypeEnum(Integer key, Integer minValue, Integer maxValue) {
		this.key = key;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public Integer getMinValue() {
		return minValue;
	}

	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}

	public Integer getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}

}
