package com.bicycle.common.enums;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public enum StatisTaskDescEnum {
	STATIS_ORDER("订单统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；4、根据企业ID区域ID获取当前区域订单；"),
	STATIS_VEHICLEREVOLVE("车辆周转率统计", "1、获取所有区域；2、获取所有企业；3、获取所有企业备案车辆数；4、循环企业和区域；5、根据企业ID区域ID获取当前区域订单；"),
	STATIS_CYCLINGCOUNT("骑行次数统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；4、根据企业ID区域ID获取当前区域订单和骑行用户数（去重）；"),
	STATIS_ONLINEVEHICLE("在线车辆统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；5、根据企业ID、区域ID、72小时内有有定位数据的车辆查询车辆基础表的数量；"),
	STATIS_ACTIVEVEHICLE("活跃车辆统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；5、根据企业ID、区域ID、168小时内有订单时间查询车辆基础表的数量；"),
	STATIS_ZOMBIEVEHICLE("僵尸车辆统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；5、根据企业ID、区域ID、168小时前有订单时间查询车辆基础表的数量；"),
	STATIS_CYCLINGDISTANCE("人均骑行距离统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；4、根据企业ID、区域ID获取当前区域的订单骑行距离和骑行用户数(去重);"),
	STATIS_CYCLINGTIME("人均骑行时间统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；4、根据企业ID、区域ID获取当前区域的订单骑行时间和骑行用户数(去重);"),
	STATIS_AREAPROPORTION("区域占比统计", "1  、获取所有的政府单位；2、获取所有行政区；3、查询所有备案车辆数；4、循环所有的行政区和政府单位;"),
	STATIS_ENTERPRISEAREAPROPORTION("企业车辆区域占比统计", "1  、获取所有行政区；2、获取所有企业；3、获取所有企业区域备案车辆和总备案车辆；4、循环所有的行政区和行政区;"),
	STATIS_VIOLATIONCASE("违规案件统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；4、根据企业ID和区域ID获取案件数量；"),
	STATIS_ELECFENCE("电子围栏数量统计", "1、获取所有区域；2、获取所有设施企业；3、循环企业和区域；4、根据企业ID和区域ID获取电子围栏数量；"),
	STATIS_ELECLABEL("电子标签投放统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；4、根据企业ID和区域ID获取电子标签数量；"),
	STATIS_PARKSITE("智能车桩数量统计", "1、获取所有区域；2、循环区域；3、根据区域ID获取所有锁桩站点；4、循环锁桩站点累加停放数量"),
	STATIS_REGISTERVEHICLE("备案车辆数统计", "1、获取所有企业；2、循环企业；3、根据企业ID获取车辆备案数量，具备蓝牙信息车辆数，最近30天在线车辆数，最近30天骑行车辆数；"),
	STATIS_AREA("停放区数量统计", "1、获取所有区域；2、获取所有停放区类型；3、循环区域和停放区类型；3、根据区域ID和类型获取停放区数量；"),
	STATIS_REGIONVEHICLE("行政区街道车辆数量统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；5、根据企业ID、区域ID、查询车辆基础表的数量；"),
	STATIS_GOVAREAVEHICLE("政府区域车辆数量统计", "1、获取所有区域；2、获取所有企业；3、循环企业和区域；5、根据企业ID、区域ID、查询车辆区域关联表的数量；"),
	STATIS_DUBIOUSORDER("可疑订单数量统计", "1、获取所有企业；2、根据企业ID、查询可疑车辆产生的订单数量之和；"),
	STATIS_DUBIOUSVEHICLE("可疑车辆数量统计", "1、获取所有企业；2、根据企业ID、查询可疑车辆数量之和；"),
	STATIS_INDUSTRYREPORT("行业分析报告统计", "1、获取所有企业；2、根据企业ID统计每月订单数量，备案车辆数，活跃车辆数，车辆周转率；"),
	STATIS_VEHICLEORDER("车辆订单数量统计", "1、获取所有企业；2、根据企业ID统计车辆每天产生的订单数量"),
	STATIS_CYCLINGORDERHOUR("车辆订单每小时数量统计", "1、获取原始数据订单表中每小时的车辆数"),
	STATIS_CYCLINGVEHICLEHOUR("车辆每小时数量统计", "1、获取原始数据订单表中每小时的去重车辆数"),
	STATIS_REPLACEVEHICLE("每日车辆置换统计",
			"统计时间必须在活跃车辆统计以及备案车辆统计后开始统计。1.获取所有企业；2.根据未备案订单信息表获取未备案订单以及核减订单数量；3.去活跃统计表查询活跃车辆；4：去备案统计表查询备案车辆；5：根据核减查询累计核减车辆以及当日核减车辆；6：根据备案时间查询累计备案车辆以及当日备案车辆。"),
	STATIS_PUTREDUCEVEHIVLEMONTH("车辆投放核减月统计", "1、获取所有区域；2、获取所有企业；3:根据企业区域去rtc_basics_put_vehicle表统计数量");

	private String key;
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
