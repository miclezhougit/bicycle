package com.bicycle.common.enums;

public enum VehicleTypeEnum {
	VEHICLE_TYPE_1(1, "自行车"), 
	VEHICLE_TYPE_2(2, "电动车");

	private Integer key;

	private String value;
	

	private VehicleTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
