package com.bicycle.common.enums;

public enum EnterpriseStatusEnum {
	ENTERPRISE_STATUS_0(0, "正常"), 
	ENTERPRISE_STATUS_1(1, "已退出");

	private Integer key;
	private String value;

	private EnterpriseStatusEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
