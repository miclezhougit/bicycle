package com.bicycle.common.enums;

/**
 * 规范停放区数据类型枚举
 * 
 * @author 焦凯旋
 *
 */
public enum AreaDataTypeEnum {

	AREA_DATA_TYPE_0(0, "区域内"),
	AREA_DATA_TYPE_1(1, "20米"),
	AREA_DATA_TYPE_2(2, "50米"),
	AREA_DATA_TYPE_3(3, "100米");

	private Integer key;
	private String value;

	private AreaDataTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
