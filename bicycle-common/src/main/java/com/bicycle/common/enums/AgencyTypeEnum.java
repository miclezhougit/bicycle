package com.bicycle.common.enums;

public enum AgencyTypeEnum {
	AGENCY_TYPE_1(1, "市直属单位"),
	AGENCY_TYPE_2(2, "区政府");

	private Integer key;
	private String value;

	private AgencyTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
