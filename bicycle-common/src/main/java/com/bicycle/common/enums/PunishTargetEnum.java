package com.bicycle.common.enums;

/**
 * 处罚对象枚举类
 * 
 * @author 焦凯旋
 */
public enum PunishTargetEnum {

	PUNISH_TARGET_COMPANY(0, "企业"),
	PUNISH_TARGET_USER(1, "用户");

	private Integer key;
	private String value;

	private PunishTargetEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
