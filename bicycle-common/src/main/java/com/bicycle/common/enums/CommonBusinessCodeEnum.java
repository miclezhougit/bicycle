
package com.bicycle.common.enums;

/**
 * 公共业务状态枚举</br>
 * 范围 1000~1499
 * 
 * @author 焦凯旋
 * @version 2019年6月3日
 * @see CommonBusinessCodeEnum
 * @since
 */
public enum CommonBusinessCodeEnum {

	CODE_INFO_ALREADY_EXISTS(1000, "信息已存在"),
	CODE_OUT_OF_RANGE(1001, "匹配不到区域信息"),
	CODE_PUT_DISTRICT_LT_STREET(1002, "行政区总量小于各街道总量和"),
	CODE_ALARM_DISTRICT_LT_STREET(1003, "行政区预警值小于各街道预警值和"),
	CODE_SIGN_ERROR(1004, "参数签名错误"),
	CODE_FILE_ERROR(1005, "上传文件错误，请参照模板上传正确文件"),
	CODE_ENTERPRISE_MISSING(1006, "缺失企业编号信息"),
	CODE_ENTERPRISE_INVALID(1007, "企业编号信息不合法"),
	CODE_REQUEST_OVERTIME(1008, "请求超时"),
	CODE_REQUEST_FREQUENTLY(1009, "请求频繁异常");

	private Integer key;

	private String value;

	private CommonBusinessCodeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
