package com.bicycle.common.enums;

public enum HasDelEnum {
	HAS_DEL_FALSE(0, "未删除"), 
	HAS_DEL_TRUE(1, "已删除");

	private Integer key;
	private String value;

	private HasDelEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
