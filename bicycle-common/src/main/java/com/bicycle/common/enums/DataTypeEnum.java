package com.bicycle.common.enums;

public enum DataTypeEnum {

	DATA_TYPE_0(0, "MSG_BICYCLE_SITE", "车辆位置信息"),
	DATA_TYPE_1(1, "MSG_ORDER", "订单信息"),
	DATA_TYPE_2(2, "MSG_ORDER_TRACK", "订单轨迹信息");

	private Integer key;
	private String value;
	private String message;

	private DataTypeEnum(Integer key, String value, String message) {
		this.key = key;
		this.value = value;
		this.message = message;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static Integer getDataTypeKey(String value) {
		for (DataTypeEnum enums : DataTypeEnum.values()) {
			if (enums.getValue().equals(value)) {
				return enums.getKey();
			}
		}
		return null;
	}

	public static String getDataTypeMessage(String value) {
		for (DataTypeEnum enums : DataTypeEnum.values()) {
			if (enums.getValue().equals(value)) {
				return enums.getMessage();
			}
		}
		return "";
	}
}
