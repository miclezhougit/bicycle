
package com.bicycle.common.enums;

/**
 * 举报来源状态枚举
 * 
 * @author 焦凯旋
 * @version 2019年6月19日
 * @see ReportResourceEnum
 * @since
 */
public enum ReportResourceEnum {

	GOV(1, "政府巡查"),
	MASS(2, "群众举报"),
	PLATFORM(3, "监管平台");
	
	private Integer key;

	private String value;
	
	

	private ReportResourceEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
