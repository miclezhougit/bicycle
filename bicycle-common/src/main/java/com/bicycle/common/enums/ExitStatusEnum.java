package com.bicycle.common.enums;

public enum ExitStatusEnum {

	EXIT_STATUS_1(1, "已完成"),
	EXIT_STATUS_2(2, "未完成");

	private Integer key;
	private String value;

	private ExitStatusEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
