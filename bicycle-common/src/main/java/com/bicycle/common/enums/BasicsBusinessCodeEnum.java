
package com.bicycle.common.enums;

/**
 * 基础信息业务状态枚举</br>
 * 范围 3500-3999
 * 
 * @author 焦凯旋
 * @version 2019年6月3日
 * @see BasicsBusinessCodeEnum
 * @since
 */
public enum BasicsBusinessCodeEnum {

	CODE_INFO_ALREADY_EXISTS(3500, "信息已存在"),
	CODE_INFO_MOBILE_EXISTS(3501, "手机号已存在"),
	CODE_INFO_BATCH_NO_EXISTS(3502, "编号已存在"),
	CODE_INFO_BATCH_IS_EFFECTIVE(3503, "指标已生效,不能进行信息修改"),
	CODE_INFO_OVER_QUANTITY(3504, "备案数量超过指标数量"),
	CODE_INFO_REGISTER_NO_EXISTS(3505, "备案批次已存在");

	private Integer key;

	private String value;

	private BasicsBusinessCodeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
