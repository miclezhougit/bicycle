package com.bicycle.common.enums;

public enum OrderTypeEnum {
	ORDER_TYPE_1(1, "普通"), ORDER_TYPE_2(2, "紧急");

	private Integer key;
	private String value;

	private OrderTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
