
package com.bicycle.common.enums;

/**
 * 
 * 车辆备案状态枚举
 * @author 焦凯旋
 * @version 2019年6月19日
 * @see PutOnStatusEnum
 * @since
 */
public enum PutOnStatusEnum {

	PUT_ON_STATUS_0(0, "未备案"),
	PUT_ON_STATUS_1(1, "已备案"),
	PUT_ON_STATUS_2(2, "已回收");

	private Integer key;

	private String value;

	private PutOnStatusEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
