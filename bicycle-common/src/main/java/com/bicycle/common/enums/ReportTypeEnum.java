package com.bicycle.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public enum ReportTypeEnum {
	REPORT_TYPE_1("1", "乱停乱放"),
	REPORT_TYPE_2("2", "损坏车辆"),
	REPORT_TYPE_3("3", "其他"),
	REPORT_TYPE_4("4", "违规投放"),
	REPORT_TYPE_5("5", "区域超量");

	@Getter
	@Setter
	private String key;
	@Getter
	@Setter
	private String value;
}
