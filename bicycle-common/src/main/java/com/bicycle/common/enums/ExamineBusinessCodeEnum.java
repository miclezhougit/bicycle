/*
 * 文件名：ExamineBusinessCodeEnum.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月3日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.common.enums;

/**
 * 考核业务状态码枚举</br>
 * 范围 1500~1999
 * 
 * @author 焦凯旋
 * @version 2019年6月3日
 * @see ExamineBusinessCodeEnum
 * @since
 */
public enum ExamineBusinessCodeEnum {
	CODE_ERROR_TEMPLATE_NOTEXIST(1500, "模板ID:{0},查询不到模板"),
	CODE_ERROR_TEMPLATE_NOT_ALLOW_DEL(1500, "当前模板有正在进行的考核任务，不允许删除"),
	CODE_ERROR_EXAMINETASK_EXIST(1500, "当前有正在进行的考核任务"),
	CODE_ERROR_EXAMINETASK_NOTEXIST(1500, "当前没有正在进行的考核任务"),
	CODE_ERROR_EXAMINETASK_NOT_PRERELEASE(1500, "当前考核任务还未预发布!"),
	CODE_ERROR_EXAMINETASK_PRERELEASE(1500, "当前考核任务已预发布，请进入正式发布查看!"),
	CODE_ERROR_EXAMINETASK_NOT_RELEASE(1500, "意见征询未完成!"),
	CODE_ERROR_EXAMINETASK_OPINION_COMPLETE(1500, "意见征询过时间点，系统默认已同意"),
	CODE_ERROR_ATTACHMENT_NOTSUBMIT(1500, "指标项名称:{0},未提交考核附件"),
	CODE_ERROR_FINAL_SCORE_NOTSUBMIT(1500, "指标项名称:{0},未提交考核得分"),
	CODE_ERROR_AGENCY_TASK_ALREADY_SUBMIT(1500, "单位考核任务已提交，请勿重复提交"),
	CODE_ERROR_TEMPLATE_NOTEQUAL_HUNDRED(1500, "提交失败,模板总分不等于100分！"),
	CODE_ERROR_AGENCY_SUBMIT(1500, "考核意见已提交."),
	CODE_ERROR_FILE_FORMAT_NOT_ALLOW(1500, "只允许上传word或者excel!"),
	CODE_ERROR_FILE_TO_LONG(1500, "只允许上传word或者excel!"),
	CODE_ERROR_CATEGORY_EXIST(1500, "指标类别已存在！"),
	CODE_ERROR_ITEMS_EXIST(1500, "指标项名称已存在！"),
	CODE_ERROR_ATTACHMENT_NOTEXIST(1500, "该单位无附件！"),
	@Deprecated
	CODE_ERROR_AGENCY_NOTSUBMIT(1500, "意见收集中，请耐心等待");

	private Integer key;

	private String value;

	private ExamineBusinessCodeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
