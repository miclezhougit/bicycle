package com.bicycle.common.enums;

public enum CurrentStatusEnum {
	CURRENT_STATUS_0(0, "未发布"), 
	CURRENT_STATUS_1(1, "预发布"), 
	CURRENT_STATUS_3(3, "部分已退回"), 
	CURRENT_STATUS_4(4, "意见征询完成"), 
	CURRENT_STATUS_5(5, "已发布");

	private Integer key;
	private String value;

	private CurrentStatusEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
