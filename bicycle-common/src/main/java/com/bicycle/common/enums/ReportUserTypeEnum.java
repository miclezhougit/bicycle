/*
 * 文件名：ReportUserTypeEnum.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月3日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.common.enums;

/**
 * 公众号用户类型
 * 
 * 
 * @author 焦凯旋
 * @version 2019年6月5日
 * @see ReportUserTypeEnum
 * @since
 */
public enum ReportUserTypeEnum {
	
	USER_TYPE_NORMAL(1,"普通"),
	USER_TYPE_GOV(2,"政府");
	
	private Integer key;
	private String value;

	private ReportUserTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
