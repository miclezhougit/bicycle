

package com.bicycle.common.enums;

public enum GovAreaAttributeEnum {
    AREA_ATTRI_PARK(0, "停放区"),
    AREA_ATTRI_MONITOR(1, "监控区"),
    AREA_ATTRI_ILLEGAL_PARK(2, "禁停区");

    private Integer key;
    private String value;

    private GovAreaAttributeEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
