package com.bicycle.common.enums;

/**
 * 车辆操作类型枚举类
 * 
 * @author 焦凯旋
 */
public enum OperateTypeEnum {

	OPERATE_TYPE_0(0, "入库"),
	OPERATE_TYPE_1(1, "备案"),
	OPERATE_TYPE_2(2, "回收");

	private Integer key;
	private String value;

	private OperateTypeEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
