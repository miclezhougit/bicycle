package com.bicycle.common.enums;

/**
 * 短信通知模板内容
 * 
 * @author 焦凯旋
 */
public enum SmsTemplateEnum {

	SMS_TEMPLATE_SAFE_NOTIFY("企业安全应急通知", "深圳市互联网租赁自行车综合监管平台安全应急通知:标题：%s，内容：%s！"),
	SMS_TEMPLATE_MONITOR_NOTIFY("企业数据传输监控通知", "深圳市互联网租赁自行车综合监管平台监控告警提示:%s企业于%s出现%s异常告警，当前数量%s条，低于阈值%s条，请及时核查异常情况！");

	private String key;

	private String value;

	private SmsTemplateEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
