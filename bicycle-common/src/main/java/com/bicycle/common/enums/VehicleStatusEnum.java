package com.bicycle.common.enums;

public enum VehicleStatusEnum {
	VEHICLE_STATUS_1(1, "正常"), 
	VEHICLE_STATUS_2(2, "故障"), 
	VEHICLE_STATUS_3(3, "报废");

	private Integer key;

	private String value;
	

	private VehicleStatusEnum(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public void setKey(Integer key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
