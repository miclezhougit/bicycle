/*
 * 文件名：GenratorNoHelper.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月21日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.common.helper;


import cn.hutool.core.date.DateUtil;

import java.util.Date;
import java.util.Random;

public class GenratorNoHelper {

    public static String generateTokenNo() {
        Random random = new Random();
        return System.currentTimeMillis() + String.format("%04d", random.nextInt(10000));
    }

    /**
     * 获取举报编号
     *
     * @return
     * @see
     */
    public static String generateReportNo() {
        Random random = new Random();
        return DateUtil.format(new Date(), "yyyyMMddHHmmss") + String.format("%04d", random.nextInt(10000));
    }

    /**
     * 获取告警编号
     *
     * @return
     */
    public static String generateAlarmNo() {
        Random random = new Random();
        return DateUtil.format(new Date(), "yyyyMMddHHmmss") + String.format("%05d", random.nextInt(100000));
    }

}
