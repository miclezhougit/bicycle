package com.bicycle.common.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.Jedis;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author : layne
 * @Date: 2020/8/11
 * @Time: 14:13
 * Description:
 */
public class RedisStringHelper {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * get获取数据
     *
     * @param key
     * @return {@link String}
     */
    public String get(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * set获取数据
     *
     * @param key
     * @param value
     * @return {@link String}
     */
    public void set(String key, String value) {
        stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * 判断key是否存在
     *
     * @param key
     * @return
     * @see
     */
    public boolean exists(String key) {
        return stringRedisTemplate.hasKey(key);
    }

    /**
     * 设置key过期时间
     *
     * @param key
     * @param timeout
     * @see
     */
    public boolean expire(String key, long timeout) {
        return stringRedisTemplate.expire(key, timeout, TimeUnit.SECONDS);
    }

    /**
     * 获取相似的key<br/>
     * bs_* 则会获取到bs_1、bs_2<br/>
     *
     * @param keys
     * @return
     * @see
     */
    public Set<String> keys(String keys) {
        return stringRedisTemplate.keys(keys);
    }

    /**
     * 删除key
     *
     * @param key
     * @see
     */
    public boolean del(String key) {
        return stringRedisTemplate.delete(key);
    }

    /**
     * 自增key方法
     *
     * @param key
     * @return
     * @see
     */
    public long incr(String key) {
        return stringRedisTemplate.opsForValue().increment(key);
    }
}
