/*
 * 文件名：JedisHelper.java
 * 版权：Copyright by 深圳市优联物联网有限公司
 * 描述：
 * 修改人：admin
 * 修改时间：2019年4月12日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.common.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Set;

/**
 * Jedis帮助类
 * 
 * @author 杨杰
 * @version 2019年4月12日
 * @see JedisHelper
 * @since
 */
public class JedisHelper {
	private static Logger LOGGER = LoggerFactory.getLogger(JedisHelper.class);

	private JedisPool jedisPool;

	public void setJedisPool(JedisPool jedisPool) {
		this.jedisPool = jedisPool;
	}

	private void returnResource(Jedis jedis) {
		if (jedis != null) {
			// 返回链接池
			jedis.close();
		}
	}

	/**
	 * get获取数据
	 * 
	 * @param key
	 * @return {@link String}
	 */
	public String get(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.get(key);
		} catch (Exception e) {
			LOGGER.error("JedisHelper get method error:", e);
		} finally {
			// 用完jedis 返还到连接池
			returnResource(jedis);
		}
		return null;
	}

	/**
	 * set获取数据
	 *
	 * @param key
	 * @param value
	 * @return {@link String}
	 */
	public String set(String key, String value) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.set(key, value);
		} catch (Exception e) {
			LOGGER.error("JedisHelper set method error:", e);
		} finally {
			// 用完jedis 返还到连接池
			returnResource(jedis);
		}
		return null;
	}

	/**
	 * 判断key是否存在
	 * 
	 * @param key
	 * @return
	 * @see
	 */
	public boolean exists(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.exists(key);
		} catch (Exception e) {
			LOGGER.error("JedisHelper exists method error:", e);
			return false;
		} finally {
			// 用完jedis 返还到连接池
			returnResource(jedis);
		}
	}

	/**
	 * 
	 * 设置key过期时间
	 * 
	 * @param key
	 * @param expire
	 * @see
	 */
	public long expire(String key, int expire) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.expire(key, expire);
		} catch (Exception e) {
			LOGGER.error("JedisHelper expire method error:", e);
			return 0;
		} finally {
			// 用完jedis 返还到连接池
			returnResource(jedis);
		}
	}

	/**
	 * 获取相似的key<br/>
	 * bs_* 则会获取到bs_1、bs_2<br/>
	 *
	 * 
	 * @param keys
	 * @return
	 * @see
	 */
	public Set<String> keys(String keys) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.keys(keys);
		} catch (Exception e) {
			LOGGER.error("JedisHelper keys method error:", e);
			return null;
		} finally {
			// 用完jedis 返还到连接池
			returnResource(jedis);
		}
	}

	/**
	 * 
	 * 删除key
	 * 
	 * @param key
	 * @param expire
	 * @see
	 */
	public long del(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.del(key);
		} catch (Exception e) {
			LOGGER.error("JedisHelper del method error:", e);
			return 0;
		} finally {
			// 用完jedis 返还到连接池
			returnResource(jedis);
		}
	}

	/**
	 * 自增key方法
	 * 
	 * @param key
	 * @return
	 * @see
	 */
	public long incr(String key) {
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.incr(key);
		} catch (Exception e) {
			LOGGER.error("JedisHelper del method error:", e);
			return 0;
		} finally {
			// 用完jedis 返还到连接池
			returnResource(jedis);
		}
	}
}
