/*
 * 文件名：Application.java
 * 版权：Copyright by Layne
 * 描述：
 * 修改人：Layne
 * 修改时间：2019年5月17日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.access;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.bicycle")
public class Application {

	static {
//		System.setProperty("java.security.auth.login.config", "d:/kafka_ssl/kafka_client_jaas.conf");
		System.setProperty("java.security.auth.login.config", "/app/kafka/config/kafka_client_jaas.conf");
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
