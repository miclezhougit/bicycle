/*
 * 文件名：KafkaProducer.java
 * 版权：Copyright by Layne
 * 描述：
 * 修改人：Layne
 * 修改时间：2019年5月17日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.access.producer;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import javax.annotation.Resource;

@Component
public class KafkaProducer {
	@Resource
	private KafkaTemplate<Object, Object> kafkaTemplate;

	public void send(String topic, String key, Object value) {
		System.out.println(">>>>>>Producer:topic:" + topic + ",key:" + key + ",value:" + value);
    ListenableFuture<SendResult<Object, Object>> send = kafkaTemplate.send(topic, key, value);
    System.out.println(send.toString());
  }

}
