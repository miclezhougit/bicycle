package com.bicycle.access.service;


/**
 * 单车企业原始数据转业务数据service
 *
 * @see BicycleDataBusinessService
 * @since
 */
public interface BicycleDataBusinessService {

    /**
     * 车辆位置上报业务处理
     *
     * @param jsonStr
     * @see
     */
    public void processBicycleSite(String jsonStr);

    /**
     * 订单上报业务处理
     *
     * @param jsonStr
     * @see
     */
    public void processOrderInfo(String jsonStr);

}
