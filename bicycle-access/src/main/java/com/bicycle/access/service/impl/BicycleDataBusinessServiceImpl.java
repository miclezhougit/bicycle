package com.bicycle.access.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.access.service.BicycleDataBusinessService;
import com.bicycle.common.enums.*;
import com.bicycle.common.helper.CoordinateHelper;
import com.bicycle.common.helper.GenratorNoHelper;
import com.bicycle.common.helper.RedisStringHelper;
import com.bicycle.service.cache.DataCacheOprator;
import com.bicycle.service.cache.ReginDataCache;
import com.bicycle.service.cache.RegionRelation;
import com.bicycle.service.dto.BicycleSite;
import com.bicycle.service.dto.OrderInfo;
import com.bicycle.service.entity.*;
import com.bicycle.service.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 单车企业原始数据转业务数据service实现类
 *
 * @see BicycleDataBusinessServiceImpl
 * @since
 */
@Slf4j
@Service("dataBusinessService")
public class BicycleDataBusinessServiceImpl implements BicycleDataBusinessService {

    @Autowired
    private IBicycleSequenceReportDetailService reportDetailService;
    @Autowired
    private IBicycleBasicsAreaVehicleService areaVehicleService;
    @Autowired
    private IBicycleBasicsBicycleService bicycleBasicsBicycleService;
    @Autowired
    private IBicycleBasicsNoRecordService bicycleBasicsNoRecordService;
    @Autowired
    private Environment environment;
    @Autowired
    private IBicycleFacilitiesGovAreaService facilitiesGovAreaService;
    @Autowired
    private RedisStringHelper redisStringHelper;

    @Override
    public void processBicycleSite(String jsonStr) {
        long beginTime = System.currentTimeMillis();
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        BicycleSite bicycleSite = JSON.parseObject(jsonStr, BicycleSite.class);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>当前是更新车辆位置信息，车锁状态是：" + bicycleSite.getLockState() + ">>>>>>>>>>>>>>>>>>>>>>>>");
        this.commonLogicProcess(bicycleSite, bicycleSite);
        if (log.isDebugEnabled()) {
            log.debug("processBicycleSite exec time {} ms", (System.currentTimeMillis() - beginTime));
        }
    }

    @Override
    public void processOrderInfo(String jsonStr) {
        long beginTime = System.currentTimeMillis();
        DataCacheOprator.getInstance().setRedisStringHelper(redisStringHelper);
        OrderInfo orderInfo = JSON.parseObject(jsonStr, OrderInfo.class);
        // 获取所在行政区
        BicycleSite bicycleSite = BicycleSite.builder().lat(orderInfo.getEndLat()).lng(orderInfo.getEndLng()).bicycleNo(orderInfo.getBicycleNo()).comId(orderInfo.getComId()).build();
        bicycleSite.setLockState(1);//订单已结束，表名单车已锁车
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>当前是更新订单信息，车锁状态是：" + bicycleSite.getLockState() + ">>>>>>>>>>>>>>>>>>>>>>>>");
        this.commonLogicProcess(bicycleSite, orderInfo);
        if (log.isDebugEnabled()) {
            log.debug("processOrderInfo exec time {} ms", (System.currentTimeMillis() - beginTime));
        }
    }


    /**
     * 车辆位置和订单公共业务逻辑处理
     *
     * @param bicycleSite
     * @see
     */
    private void commonLogicProcess(BicycleSite bicycleSite, Object obj) {
        if (bicycleSite == null || StringUtils.isBlank(bicycleSite.getLat()) || StringUtils.isBlank(bicycleSite.getLng()) || StringUtils.isBlank(bicycleSite.getComId())
                || StringUtils.isBlank(bicycleSite.getBicycleNo())) {
            log.warn("传输信息异常：json：{}", JSON.toJSONString(bicycleSite));
            return;
        }

        // 高德地图坐标系GCJ转百度BD-2[point[0]=lat point[1]=lng]
        double[] baiduCoord = CoordinateHelper.gcj2bd(Double.parseDouble(bicycleSite.getLat()), Double.parseDouble(bicycleSite.getLng()));
        bicycleSite.setLat(String.valueOf(baiduCoord[0]));
        bicycleSite.setLng(String.valueOf(baiduCoord[1]));

        // 获取所在行政区
        RegionRelation globalRelation = getRegionRelation(bicycleSite);
        if (globalRelation == null) {
            log.info("processBicycleSite() 当前车辆位置没有对应的行政区,json:{}", JSON.toJSONString(bicycleSite));
        }
        // 获取所在街道
        BicycleCommonRegion globalStreet = null;
        if (globalRelation != null) {
            globalStreet = getRegionStreet(bicycleSite, globalRelation.getRegion());
            if (globalStreet == null) {
                log.info("processBicycleSite() 当前车辆位置没有对应的街道,json:{}", JSON.toJSONString(bicycleSite));
            }
        }


        // 获取当前的企业
        BicycleCommonEnterprise enterprise = DataCacheOprator.getInstance().getAllEnterprise(bicycleSite.getComId());

        if (globalRelation == null) {
            log.info("processBicycleSite() 没有命中深圳范围内的区域,json:{}", JSON.toJSONString(bicycleSite));
            return;
        }

        // 删除车辆区域关联关系
        deleteAreaVehicle(bicycleSite, enterprise);

        // 判断是否禁停区  是则产生乱停乱放案件
        /*BicycleFacilitiesGovArea forbidArea = getGovAreaByAreaType(bicycleSite, GovAreaAttributeEnum.AREA_ATTRI_ILLEGAL_PARK.getKey());
        if (forbidArea == null) {
            log.info("processBicycleSite() 当前车辆位置没有命中禁停区,json:{}", JSON.toJSONString(bicycleSite));
        } else {
            log.info("processBicycleSite() 当前车辆位置命中禁停区,id:{},区域名称：{},json:{}", forbidArea.getId(), forbidArea.getAreaName(), JSON.toJSONString(bicycleSite));
            insertIllegalParkCase(bicycleSite, globalRelation, globalStreet, enterprise);
            insertAreaVehicle(forbidArea, bicycleSite, globalStreet, enterprise);
        }*/


        // 判断是否监控区
        BicycleFacilitiesGovArea monitorArea = getGovAreaByAreaType(bicycleSite, GovAreaAttributeEnum.AREA_ATTRI_MONITOR.getKey());
        if (monitorArea == null) {
            log.info("processBicycleSite() 当前车辆位置没有命中监控区,json:{}", JSON.toJSONString(bicycleSite));
        } else {
            log.info("processBicycleSite() 当前车辆位置命中监控区,id:{},区域名称：{},json:{}", monitorArea.getId(), monitorArea.getAreaName(), JSON.toJSONString(bicycleSite));
            insertAreaVehicle(monitorArea, bicycleSite, enterprise);

            //监控区车辆超量产生案件
            BicycleBasicsAreaVehicle areaVehicle = new BicycleBasicsAreaVehicle();
            areaVehicle.setAreaId(monitorArea.getId());
            areaVehicle.setAreaAttribute(monitorArea.getAreaAttribute());
            int count = areaVehicleService.count(new QueryWrapper<>(areaVehicle));
            if (count > monitorArea.getParkValue()) {
                insertExcessCase(bicycleSite, globalRelation, globalStreet, enterprise);
            }
        }


        // 判断是否停放区,车锁必须是已锁状态
        if(bicycleSite.getLockState() == 1) {
            System.out.println("进入车辆违停状态判断，当前车辆行政区=" + globalRelation.getParentRegion().getId() + "，街道=" + globalStreet.getId());
            BicycleFacilitiesGovArea parkArea = getGovAreaByAreaType(bicycleSite, GovAreaAttributeEnum.AREA_ATTRI_PARK.getKey());
            if (parkArea == null) {
                log.info("processBicycleSite() 当前车辆位置没有命中停放区,json:{}", JSON.toJSONString(bicycleSite));
                insertIllegalParkCase(bicycleSite, globalRelation, globalStreet, enterprise);
                bicycleSite.setHasIllegalPark(1);
            } else {
                log.info("processBicycleSite() 当前车辆位置命中停放区,id:{},区域名称：{},json:{}", parkArea.getId(), parkArea.getAreaName(), JSON.toJSONString(bicycleSite));
                insertAreaVehicle(parkArea, bicycleSite, enterprise);
                bicycleSite.setHasIllegalPark(0);
            }
        }


        // 判断是否违规投放
        boolean flag = getBicycleBasicsBicycle(bicycleSite);
        if (flag) {
            log.info("processBicycleSite() 当前车辆已在所属行政区备案,json:{}", JSON.toJSONString(bicycleSite));
        } else {
            log.info("processBicycleSite() 当前车辆未在所属行政区备案,json:{}", JSON.toJSONString(bicycleSite));
            //上报违规投放案件
            insertIllegalDeliveryCase(bicycleSite, globalRelation, globalStreet, enterprise);
            bicycleSite.setPutOnState(0);
        }

        if (flag) {//已备案：更新违停状态及位置信息
            updateBicycleBasicsBicycle(bicycleSite, globalRelation, globalStreet);
        } else {//未备案：新增违投车辆
            insertBicycleBasicsBicycle(bicycleSite, globalRelation, globalStreet, enterprise);
        }

    }

    /**
     * 乱停乱放新增案件
     *
     * @param bicycleSite
     * @param globalRelation
     * @param enterprise
     * @see
     */
    private void insertIllegalParkCase(BicycleSite bicycleSite, RegionRelation globalRelation, BicycleCommonRegion globalStreet, BicycleCommonEnterprise enterprise) {
        BicycleSequenceReportDetail reportDetail = new BicycleSequenceReportDetail();
        reportDetail.setReportNo(GenratorNoHelper.generateReportNo());
        reportDetail.setReportTime(new Date());
        reportDetail.setReportType(ReportTypeEnum.REPORT_TYPE_1.getKey());
        reportDetail.setReportContent("平台检测违规停放");
        reportDetail.setReportResource(3); // 1：政府巡查，2：群众举报，3：监管平台
        reportDetail.setReportStatus(0); // 0：未处理 1：已退回，2：已处理，3：已结案
        reportDetail.setOrderType(1); // 1：普通订单 2：紧急订单
        reportDetail.setEnterpriseNo(enterprise.getEnterpriseNo());
        reportDetail.setEnterpriseName(enterprise.getEnterpriseAlias());
        if (globalRelation != null) {
            reportDetail.setDistrictId(globalRelation.getParentRegion().getId());
            reportDetail.setDistrictName(globalRelation.getParentRegion().getRegionName());
        }
        if (globalStreet != null) {
            reportDetail.setStreetId(globalStreet.getId());
            reportDetail.setStreetName(globalStreet.getRegionName());
        }
        reportDetail.setHasTrue(3); // 0：待核实，1：不属实 2：重复案件，3：属实
        reportDetail.setHasViolation(0); // 0：否 1：是
        reportDetail.setHasIntelligence(1); // reportDetail.setHasIntelligence(1);//
        reportDetail.setLng(bicycleSite.getLng());
        reportDetail.setLat(bicycleSite.getLat());
        reportDetail.setDetailAddress(null);
        reportDetail.setVehicleNo(bicycleSite.getBicycleNo());
        reportDetail.setVehicleCount(1); // 车辆数量
        reportDetail.setCreateTime(new Date());
        reportDetailService.save(reportDetail);
    }


    /**
     * 新增超量案件
     *
     * @param bicycleSite
     * @param globalRelation
     * @param globalStreet
     * @param enterprise
     */
    private void insertExcessCase(BicycleSite bicycleSite, RegionRelation globalRelation, BicycleCommonRegion globalStreet, BicycleCommonEnterprise enterprise) {
        BicycleSequenceReportDetail reportDetail = new BicycleSequenceReportDetail();
        reportDetail.setReportNo(GenratorNoHelper.generateReportNo());
        reportDetail.setReportTime(new Date());
        reportDetail.setReportType(ReportTypeEnum.REPORT_TYPE_5.getKey());
        reportDetail.setReportContent("平台检测区域超量");
        reportDetail.setReportResource(3); // 1：政府巡查，2：群众举报，3：监管平台
        reportDetail.setReportStatus(0); // 0：未处理 1：已退回，2：已处理，3：已结案
        reportDetail.setOrderType(1); // 1：普通订单 2：紧急订单
        reportDetail.setEnterpriseNo(enterprise.getEnterpriseNo());
        reportDetail.setEnterpriseName(enterprise.getEnterpriseAlias());
        if (globalRelation != null) {
            reportDetail.setDistrictId(globalRelation.getParentRegion().getId());
            reportDetail.setDistrictName(globalRelation.getParentRegion().getRegionName());
        }
        if (globalStreet != null) {
            reportDetail.setStreetId(globalStreet.getId());
            reportDetail.setStreetName(globalStreet.getRegionName());
        }
        reportDetail.setHasTrue(3); // 0：待核实，1：不属实 2：重复案件，3：属实
        reportDetail.setHasViolation(0); // 0：否 1：是
        reportDetail.setHasIntelligence(1); // reportDetail.setHasIntelligence(1);//
        reportDetail.setLng(bicycleSite.getLng());
        reportDetail.setLat(bicycleSite.getLat());
        reportDetail.setDetailAddress(null);
        reportDetail.setVehicleNo(bicycleSite.getBicycleNo());
        reportDetail.setVehicleCount(1); // 车辆数量
        reportDetail.setCreateTime(new Date());
        reportDetailService.save(reportDetail);
    }

    /**
     * 新增或修改备案表信息
     *
     * @param bicycleSite
     * @param globalRelation
     * @param enterprise
     * @see
     */
    private void insertBicycleBasicsBicycle(BicycleSite bicycleSite, RegionRelation globalRelation,
                                            BicycleCommonRegion globalStreet, BicycleCommonEnterprise enterprise) {
        BicycleBasicsBicycle basicsBicycle = new BicycleBasicsBicycle();
        basicsBicycle.setEnterpriseNo(enterprise.getEnterpriseNo());
        basicsBicycle.setEnterpriseName(enterprise.getEnterpriseAlias());
        basicsBicycle.setVehicleNo(bicycleSite.getBicycleNo());
        basicsBicycle.setVehicleType(1);
        basicsBicycle.setVehicleStatus(1);
        basicsBicycle.setLockStatus(bicycleSite.getLockState());
        basicsBicycle.setLng(bicycleSite.getLng());
        basicsBicycle.setLat(bicycleSite.getLat());
        if (globalRelation != null) {
            basicsBicycle.setDistrict(globalRelation.getParentRegion().getId());
        }
        if (globalStreet != null) {
            basicsBicycle.setStreet(globalStreet.getId());
        }
        basicsBicycle.setPutOnStatus(bicycleSite.getPutOnState());// 车辆备案状态： 0：未备案，1：已备案，2：已回收
        basicsBicycle.setHasIllegalPark(bicycleSite.getHasIllegalPark());// 车辆是否违停：0：否，1：是
        basicsBicycle.setGnssTime(bicycleSite.getGnssTime());
        basicsBicycle.setCreateTime(new Date());
        bicycleBasicsBicycleService.save(basicsBicycle);
    }

    /**
     * @Description: 更新备案表数据
     * @Author: zhouzm
     * @Date: 2021/7/31 22:40
     * @param bicycleSite:
     * @param globalRelation:
     * @param globalStreet:
     * @return: void
     **/
    private void updateBicycleBasicsBicycle(BicycleSite bicycleSite, RegionRelation globalRelation,BicycleCommonRegion globalStreet) {
        BicycleBasicsBicycle basicsBicycle = new BicycleBasicsBicycle();
        basicsBicycle.setLockStatus(bicycleSite.getLockState());
        basicsBicycle.setLng(bicycleSite.getLng());
        basicsBicycle.setLat(bicycleSite.getLat());
        if (globalRelation != null) {
            basicsBicycle.setDistrict(globalRelation.getParentRegion().getId());
        }
        if (globalStreet != null) {
            basicsBicycle.setStreet(globalStreet.getId());
        }
        basicsBicycle.setHasIllegalPark(bicycleSite.getHasIllegalPark());// 车辆是否违停：0：否，1：是
        basicsBicycle.setGnssTime(bicycleSite.getGnssTime());
        basicsBicycle.setUpdateTime(new Date());
        BicycleBasicsBicycle qbasicsBicycle = new BicycleBasicsBicycle();
        qbasicsBicycle.setVehicleNo(bicycleSite.getBicycleNo());
        bicycleBasicsBicycleService.update(basicsBicycle, new QueryWrapper<BicycleBasicsBicycle>(qbasicsBicycle));
    }


    /**
     * 违规投放新增案件
     *
     * @param bicycleSite
     * @param globalRelation
     * @param enterprise
     * @see
     */
    private void insertIllegalDeliveryCase(BicycleSite bicycleSite, RegionRelation globalRelation, BicycleCommonRegion globalStreet, BicycleCommonEnterprise enterprise) {
        BicycleSequenceReportDetail reportDetail = new BicycleSequenceReportDetail();
        reportDetail.setReportNo(GenratorNoHelper.generateReportNo());
        reportDetail.setReportTime(new Date());
        reportDetail.setReportType(ReportTypeEnum.REPORT_TYPE_4.getKey());
        reportDetail.setReportContent("平台检测违规投放");
        reportDetail.setReportResource(3); // 1：政府巡查，2：群众举报，3：监管平台
        reportDetail.setReportStatus(0); // 0：未处理 1：已退回，2：已处理，3：已结案
        reportDetail.setOrderType(1); // 1：普通订单 2：紧急订单
        reportDetail.setEnterpriseNo(enterprise.getEnterpriseNo());
        reportDetail.setEnterpriseName(enterprise.getEnterpriseAlias());
        if (globalRelation != null) {
            reportDetail.setDistrictId(globalRelation.getParentRegion().getId());
            reportDetail.setDistrictName(globalRelation.getParentRegion().getRegionName());
        }
        if (globalStreet != null) {
            reportDetail.setStreetId(globalStreet.getId());
            reportDetail.setStreetName(globalStreet.getRegionName());
        }
        reportDetail.setHasTrue(3); // 0：待核实，1：不属实 2：重复案件，3：属实
        reportDetail.setHasViolation(1); // 0：否 1：是
        reportDetail.setHasIntelligence(1); // 是否智能案件：0：否，1：是
        reportDetail.setLng(bicycleSite.getLng());
        reportDetail.setLat(bicycleSite.getLat());
        reportDetail.setDetailAddress(null);
        reportDetail.setVehicleNo(bicycleSite.getBicycleNo());
        reportDetail.setVehicleCount(1); // 车辆数量
        reportDetail.setCreateTime(new Date());
        reportDetailService.save(reportDetail);
    }


    /**
     * 根据经纬度，获取当前车辆所在行政区
     *
     * @param bicycleSite
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    private RegionRelation getRegionRelation(BicycleSite bicycleSite) {
        RegionRelation globalRelation = null;
        Point2D.Double point = new Point2D.Double(Double.parseDouble(bicycleSite.getLat()), Double.parseDouble(bicycleSite.getLng()));
        List<RegionRelation> regionRelationList = ReginDataCache.getRegionRelationList();
        if(regionRelationList != null) {
            for (RegionRelation relation : regionRelationList) {
                if (StringUtils.isNotEmpty(relation.getParentRegion().getRegionPoints())) {
                    String pointStr = relation.getParentRegion().getRegionPoints();
                    List<JSONObject> objectList = JSON.parseObject(pointStr, ArrayList.class);
                    List<Point2D.Double> pts = new ArrayList<Point2D.Double>();
                    for (JSONObject object : objectList) {
                        Point2D.Double p = new Point2D.Double(object.getDoubleValue("lat"), object.getDoubleValue("lng"));
                        pts.add(p);
                    }
                    boolean hasArea = CoordinateHelper.IsPtInPoly(point, pts);
                    if (hasArea) {
                        globalRelation = relation;
                        log.info("getRegionRelation() 当前车辆停放的行政区ID:{},行政区名称:{}", relation.getParentRegion().getId(), relation.getParentRegion().getRegionName());
                        break;
                    }
                }
            }
        }
        return globalRelation;
    }

    /**
     * 获取所在街道
     *
     * @param bicycleSite
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    private BicycleCommonRegion getRegionStreet(BicycleSite bicycleSite, List<BicycleCommonRegion> regionList) {
        BicycleCommonRegion globalRegion = null;
        Point2D.Double point = new Point2D.Double(Double.parseDouble(bicycleSite.getLat()), Double.parseDouble(bicycleSite.getLng()));
        if(regionList != null) {
            for (BicycleCommonRegion region : regionList) {
                if (StringUtils.isNotEmpty(region.getRegionPoints())) {
                    String pointStr = region.getRegionPoints();
                    List<JSONObject> objectList = JSON.parseObject(pointStr, ArrayList.class);
                    List<Point2D.Double> pts = new ArrayList<Point2D.Double>();
                    for (JSONObject object : objectList) {
                        Point2D.Double p = new Point2D.Double(object.getDoubleValue("lat"), object.getDoubleValue("lng"));
                        pts.add(p);
                    }
                    boolean hasArea = CoordinateHelper.IsPtInPoly(point, pts);
                    if (hasArea) {
                        globalRegion = region;
                        log.info("getRegionStreet() 当前车辆停放的街道ID:{},街道名称:{}", region.getId(), region.getRegionName());
                        break;
                    }
                }
            }
        }
        return globalRegion;
    }

    /**
     * 获取是否停在禁停区
     *
     * @param bicycleSite
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    private BicycleFacilitiesGovArea getGovAreaByAreaType(BicycleSite bicycleSite, int areaAttribute) {
        BicycleFacilitiesGovArea globalForbidArea = null;
        // 获取当前行政区域内的禁停区
//        List<BicycleFacilitiesGovArea> govAreaList = DataCacheOprator.getInstance().getGovAreaList(BicycleFacilitiesGovArea.builder().areaAttribute(areaAttribute).build());
        List<BicycleFacilitiesGovArea> govAreaList = facilitiesGovAreaService.list(new QueryWrapper<BicycleFacilitiesGovArea>(BicycleFacilitiesGovArea.builder().hasDel(HasDelEnum.HAS_DEL_FALSE.getKey()).areaStatus(AreaStatusEnum.AREA_STATUS_0.getKey()).areaType(areaAttribute).build()));
        Point2D.Double point = new Point2D.Double(Double.parseDouble(bicycleSite.getLat()), Double.parseDouble(bicycleSite.getLng()));
        for (BicycleFacilitiesGovArea govArea : govAreaList) {
            if (StringUtils.isNotEmpty(govArea.getAreaPoints())) {
                String pointStr = govArea.getAreaPoints();
                List<JSONObject> objectList = JSON.parseObject(pointStr, ArrayList.class);
                List<Point2D.Double> pts = new ArrayList<Point2D.Double>();
                boolean distanceTrue = true;
                for (JSONObject object : objectList) {
                    Point2D.Double p = new Point2D.Double(object.getDoubleValue("lat"), object.getDoubleValue("lng"));
                    pts.add(p);
                    if(areaAttribute == 0 && distanceTrue) {//停放区，在原先停放区基础上，以车辆位置信息距离车框最近点有没有超过5米，超过则违停
                        double distance = CoordinateHelper.getDistance(new Double(bicycleSite.getLat()),new Double(bicycleSite.getLng())
                                ,object.getDoubleValue("lat"),object.getDoubleValue("lng"));
                        if (distance <= Double.parseDouble(environment.getProperty("region.distance"))) {//两点距离小于5米，则不算违停
                            distanceTrue = false;
                        }
                    }
                }
                boolean hasArea = CoordinateHelper.IsPtInPoly(point, pts);
                if (hasArea || !distanceTrue) {//在停放区或者距离停放区最近点距离小于5米
                    globalForbidArea = govArea;
                    log.info("getGovAreaByAreaType() 命中政府区域,区域ID：{},区域名称：{}", govArea.getId(), govArea.getAreaName());
                    break;
                }
            }
        }
        return globalForbidArea;
    }


    /**
     * 根据企业编号和车辆编号删除所有的车辆区域关联关系
     *
     * @param bicycleSite
     * @param enterprise
     * @see
     */
    private void deleteAreaVehicle(BicycleSite bicycleSite, BicycleCommonEnterprise enterprise) {
        BicycleBasicsAreaVehicle areaVehicle = new BicycleBasicsAreaVehicle();
        areaVehicle.setEnterpriseNo(enterprise.getEnterpriseNo());
        areaVehicle.setVehicleNo(bicycleSite.getBicycleNo());
        areaVehicleService.remove(new QueryWrapper<>(areaVehicle));
    }

    /**
     * 新增区域和车辆的关联关系
     *
     * @param govArea
     * @param bicycleSite
     * @param enterprise
     * @see
     */
    private void insertAreaVehicle(BicycleFacilitiesGovArea govArea, BicycleSite bicycleSite, BicycleCommonEnterprise enterprise) {
        BicycleBasicsAreaVehicle areaVehicle = new BicycleBasicsAreaVehicle();
        areaVehicle.setEnterpriseNo(enterprise.getEnterpriseNo());
        areaVehicle.setEnterpriseName(enterprise.getEnterpriseAlias());
        areaVehicle.setVehicleNo(bicycleSite.getBicycleNo());
        areaVehicle.setAreaId(govArea.getId());
        areaVehicle.setAreaAttribute(govArea.getAreaAttribute());
        areaVehicleService.save(areaVehicle);
    }

    /**
     * 获取车辆是否已备案
     *
     * @param bicycleSite
     * @return
     * @see
     */
    @SuppressWarnings("unchecked")
    private boolean getBicycleBasicsBicycle(BicycleSite bicycleSite) {
        boolean flag = false;
        BicycleBasicsBicycle bicycleBasicsBicycle = new BicycleBasicsBicycle();
        bicycleBasicsBicycle.setVehicleNo(bicycleSite.getBicycleNo());
        List<BicycleBasicsBicycle> qBicycleBasicsBicycle = bicycleBasicsBicycleService.list(new QueryWrapper<BicycleBasicsBicycle>(bicycleBasicsBicycle));
        if(qBicycleBasicsBicycle != null && qBicycleBasicsBicycle.size() > 0) {//备案表存在，则无需同步
            flag = true;
        }
        return flag;
    }

    /**
     * 新增/更新未备案车联信息
     * @param bicycleSite
     * @param globalRelation
     * @param globalStreet
     * @see
     */
    private void insertBicycleBasicsNoRecord(BicycleSite bicycleSite, RegionRelation globalRelation,BicycleCommonRegion globalStreet) {
        BicycleBasicsNoRecord noRecord = new BicycleBasicsNoRecord();
        noRecord.setComId(bicycleSite.getComId());
        noRecord.setBicycleNo(bicycleSite.getBicycleNo());
        noRecord.setLng(bicycleSite.getLng());
        noRecord.setLat(bicycleSite.getLat());
        noRecord.setDistrict(globalRelation.getParentRegion().getId());
        noRecord.setStreet(globalStreet.getId());
        noRecord.setCreateDate(new Date());
        bicycleBasicsNoRecordService.saveOrUpdate(noRecord);
    }

    /**
     * 删除未备案表中，已备案车辆信息
     * @param bicycleSite
     * @see
     */
    private void deleteBicycleBasicsNoRecord(BicycleSite bicycleSite) {
        BicycleBasicsNoRecord noRecord = new BicycleBasicsNoRecord();
        noRecord.setComId(bicycleSite.getComId());
        noRecord.setBicycleNo(bicycleSite.getBicycleNo());
        bicycleBasicsNoRecordService.remove(new QueryWrapper<BicycleBasicsNoRecord>(noRecord));
    }

}
