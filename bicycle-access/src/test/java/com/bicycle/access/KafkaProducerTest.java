/*
 * 文件名：KafkaProducerTest.java
 * 版权：Copyright by Layne
 * 描述：
 * 修改人：Layne
 * 修改时间：2019年5月17日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.access;

import com.alibaba.fastjson.JSON;
import com.bicycle.access.producer.KafkaProducer;
import com.bicycle.common.enums.MsgEnums;
import com.bicycle.common.helper.CoordinateHelper;
import com.bicycle.service.dto.BaseJsonMsg;
import com.bicycle.service.dto.BicycleSite;
import com.bicycle.service.entity.BicycleBasicsBicycle;
import com.bicycle.service.entity.BicycleDataOrderInfo;
import com.bicycle.service.service.IBicycleBasicsBicycleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class KafkaProducerTest {
	@Resource
	private KafkaProducer kafkaProducer;
	@Resource
	private IBicycleBasicsBicycleService basicsBicycleService;

	@Test
	public void send() {
		BicycleSite bicycleSite = new BicycleSite();
		bicycleSite.setBicycleNo("1111111111111");
		bicycleSite.setComId("HL");
		bicycleSite.setGnssTime(new Date());
		bicycleSite.setLat("22.55212551227694");
		bicycleSite.setLng("114.1219808971019");
		bicycleSite.setLockState(1);

		BaseJsonMsg baseJsonMsg = new BaseJsonMsg();
		baseJsonMsg.setJsonStr(bicycleSite);
		baseJsonMsg.setType(MsgEnums.MSG_BICYCLE_SITE.getTopicKey());
		kafkaProducer.send("JR_HL",MsgEnums.MSG_BICYCLE_SITE.getTopicKey(), JSON.toJSONString(baseJsonMsg));

    BicycleDataOrderInfo orderInfo = new BicycleDataOrderInfo();
    orderInfo.setComId("HL");
    orderInfo.setOrderNo("9999999999");
    orderInfo.setBicycleNo("66666666");
    orderInfo.setUserId("1239876443");
    orderInfo.setStartTime(new Date());
    orderInfo.setStartLat("22.55212551227694");
    orderInfo.setStartLng("114.1219808971019");
    orderInfo.setEndTime(new Date());
    orderInfo.setEndLat("22.55212551227694");
    orderInfo.setEndLng("114.1219808971019");
    orderInfo.setOrderMile(12.09);
    orderInfo.setUploadTime(new Date());

    BaseJsonMsg baseJsonMsg1 = new BaseJsonMsg();
    baseJsonMsg1.setJsonStr(orderInfo);
    baseJsonMsg1.setType(MsgEnums.MSG_ORDER.getTopicKey());
    kafkaProducer.send("JR_HL",MsgEnums.MSG_ORDER.getTopicKey(), JSON.toJSONString(baseJsonMsg1));
	}
	@Test
	public void jwd() {
		List<BicycleBasicsBicycle> list = basicsBicycleService.list();
		list.forEach(bicycle ->{
			// 高德地图坐标系GCJ转百度BD-2[point[0]=lat point[1]=lng]
			double[] baiduCoord = CoordinateHelper.bd2gcj(Double.parseDouble(bicycle.getLat()), Double.parseDouble(bicycle.getLng()));
			bicycle.setLat(String.valueOf(baiduCoord[0]));
			bicycle.setLng(String.valueOf(baiduCoord[1]));
			basicsBicycleService.saveOrUpdate(bicycle);
		});



	}

}
