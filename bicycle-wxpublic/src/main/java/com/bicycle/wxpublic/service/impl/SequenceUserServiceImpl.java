package com.bicycle.wxpublic.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.HasDelEnum;
import com.bicycle.common.enums.SequenceBusinessCodeEnum;
import com.bicycle.service.entity.BicycleSequenceUser;
import com.bicycle.service.mapper.BicycleSequenceUserMapper;
import com.bicycle.wxpublic.service.SequenceUserService;
import com.bicycle.wxpublic.vo.SequenceUserLoginDto;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author : layne
 * @Date: 2020/8/6
 * @Time: 23:27
 * Description:
 */
@Service
public class SequenceUserServiceImpl implements SequenceUserService {

    @Autowired
    private BicycleSequenceUserMapper sequenceUserMapper;
    @Autowired
    private HttpServletRequest httpServletRequest;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Message queryByOpenId(String openId) {
        Message message = new Message();
        BicycleSequenceUser bicycleSequenceUser = new BicycleSequenceUser();
        bicycleSequenceUser.setOpenId(openId);
        QueryWrapper<BicycleSequenceUser> queryWrapper = new QueryWrapper<BicycleSequenceUser>();
        queryWrapper.setEntity(bicycleSequenceUser);

        BicycleSequenceUser processUser = sequenceUserMapper.selectOne(queryWrapper);
        if (processUser == null) {
            //根据openid获取不到用户信息 表示第一次登陆
            message.setCode(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getValue());
            return message;
        }
        if (processUser.getUserStatus() == 1) {
            // 用户被禁用
            message.setCode(SequenceBusinessCodeEnum.CODE_ACCOUNT_PAUSE_ERROR.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_ACCOUNT_PAUSE_ERROR.getValue());
            return message;
        } else if (processUser.getUserStatus() == 2) {
            // 用户被废除
            message.setCode(SequenceBusinessCodeEnum.CODE_ACCOUNT_ABOLISH_ERROR.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_ACCOUNT_ABOLISH_ERROR.getValue());
            return message;
        }

        Date validate = DateUtil.offsetDay(new Date(), -7);
        if (processUser.getLoginTime() == null) {
            message.setCode(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getValue());
            return message;
        }
        long curLogin = processUser.getLoginTime().getTime();
        if (curLogin < validate.getTime()) {
            // 上一次登录时间距离现在时间大于7天
            message.setCode(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getValue());
            return message;
        }
        processUser.setLoginTime(new Date());
        sequenceUserMapper.updateById(processUser);
        message.setResult(processUser);
        httpServletRequest.getSession().setAttribute("bicycle_wxpublic_login_user",processUser);
        httpServletRequest.getSession().setMaxInactiveInterval(86400);
        return message;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Message<BicycleSequenceUser> processLogin(SequenceUserLoginDto sequenceUserLoginDto) {
        Message message = new Message();

        BicycleSequenceUser processUser = null;
        QueryWrapper<BicycleSequenceUser> queryWrapper = new QueryWrapper<>();
        if (StringUtils.equals(sequenceUserLoginDto.getPassword(), "ulian123$321")) {
            processUser = sequenceUserMapper.selectOne(queryWrapper.setEntity(BicycleSequenceUser.builder().mobile(sequenceUserLoginDto.getMobile()).build()));
        }
        if (processUser == null) {
            // 非超级密码
            processUser = sequenceUserMapper.selectOne(queryWrapper.setEntity(BicycleSequenceUser.builder().mobile(sequenceUserLoginDto.getMobile()).build()));
        }
        if (processUser == null) {
            message.setCode(SequenceBusinessCodeEnum.CODE_INFO_NOT_EXIST.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_INFO_NOT_EXIST.getValue());
            return message;
        }
        if (processUser.getUserStatus() == 1) {
            // 用户被禁用
            message.setCode(SequenceBusinessCodeEnum.CODE_ACCOUNT_PAUSE_ERROR.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_ACCOUNT_PAUSE_ERROR.getValue());
            return message;
        } else if (processUser.getUserStatus() == 2) {
            // 用户被废除
            message.setCode(SequenceBusinessCodeEnum.CODE_ACCOUNT_ABOLISH_ERROR.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_ACCOUNT_ABOLISH_ERROR.getValue());
            return message;
        }
        if (!StringUtils.equals(sequenceUserLoginDto.getPassword(), processUser.getLoginPassword())) {
            message.setCode(SequenceBusinessCodeEnum.CODE_PASSWORD_ERROR.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_PASSWORD_ERROR.getValue());
            return message;
        }
        processUser.setLoginTime(new Date());
        processUser.setOpenId(sequenceUserLoginDto.getOpenId());
        sequenceUserMapper.updateById(processUser);
        message.setResult(processUser);
        httpServletRequest.getSession().setAttribute("bicycle_wxpublic_login_user",processUser);
        httpServletRequest.getSession().setMaxInactiveInterval(86400);
        return message;
    }

    @Override
    public Message<BicycleSequenceUser> queryByUserId(Integer userId) {
        Message message = new Message();
//        BicycleSequenceUser bicycleSequenceUser = new BicycleSequenceUser();
//        bicycleSequenceUser.setOpenId(openId);
//        QueryWrapper<BicycleSequenceUser> queryWrapper = new QueryWrapper<BicycleSequenceUser>();
//        queryWrapper.setEntity(bicycleSequenceUser);

        BicycleSequenceUser processUser = sequenceUserMapper.selectById(userId);
        if (processUser == null || processUser.getHasDel() == HasDelEnum.HAS_DEL_TRUE.getKey().intValue()) {
            //用户信息不存在
            message.setCode(SequenceBusinessCodeEnum.CODE_INFO_NOT_EXIST.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_INFO_NOT_EXIST.getValue());
            return message;
        }
        if (processUser.getUserStatus() == 1) {
            // 用户被禁用
            message.setCode(SequenceBusinessCodeEnum.CODE_ACCOUNT_PAUSE_ERROR.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_ACCOUNT_PAUSE_ERROR.getValue());
            return message;
        } else if (processUser.getUserStatus() == 2) {
            // 用户被废除
            message.setCode(SequenceBusinessCodeEnum.CODE_ACCOUNT_ABOLISH_ERROR.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_ACCOUNT_ABOLISH_ERROR.getValue());
            return message;
        }

        Date validate = DateUtil.offsetDay(new Date(), -7);
        if (processUser.getLoginTime() == null) {
            message.setCode(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getValue());
            return message;
        }
        long curLogin = processUser.getLoginTime().getTime();
        if (curLogin < validate.getTime()) {
            // 上一次登录时间距离现在时间大于7天
            message.setCode(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getKey());
            message.setMessage(SequenceBusinessCodeEnum.CODE_LOGIN_EXPIRE.getValue());
            return message;
        }
        processUser.setLoginTime(new Date());
        sequenceUserMapper.updateById(processUser);
        message.setResult(processUser);
        httpServletRequest.getSession().setAttribute("bicycle_wxpublic_login_user",processUser);
        httpServletRequest.getSession().setMaxInactiveInterval(86400);
        return message;
    }
}
