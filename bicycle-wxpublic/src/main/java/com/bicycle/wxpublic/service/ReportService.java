package com.bicycle.wxpublic.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.wxpublic.vo.ReportPhotoDto;
import com.bicycle.wxpublic.vo.ReportRecordDto;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 22:40
 * Description:
 */
public interface ReportService {

    /**
     * 拍照举报
     *
     * @param reportPhotoDto
     * @return
     */
    Message photoReport(ReportPhotoDto reportPhotoDto);

    /**
     * 获取举报信息列表
     *
     * @param reportRecordDto
     * @return
     */
    Message record(ReportRecordDto reportRecordDto);

    /**
     * 获取举报记录详情
     *
     * @param reportId
     * @return
     */
    Message recordDetail(Integer reportId);
}
