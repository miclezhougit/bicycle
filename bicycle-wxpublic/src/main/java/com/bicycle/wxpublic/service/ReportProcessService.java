package com.bicycle.wxpublic.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.wxpublic.vo.ReportProcessDetailDto;
import com.bicycle.wxpublic.vo.ReportProcessDto;
import com.bicycle.wxpublic.vo.ReportProcessListDto;
import com.bicycle.wxpublic.vo.ReportRepeatCaseDto;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 9:20
 * Description:
 */
public interface ReportProcessService {
    /**
     * 企业端获取举报信息列表
     *
     * @param reportProcessListDto
     * @return
     */
    Message queryReportDetailList(ReportProcessListDto reportProcessListDto);

    /**
     * 获取举报信息详情
     *
     * @param detailId
     * @return
     */
    Message queryWxReportDetail(Integer detailId);

    /**
     * 处理举报
     *
     * @param reportProcessDto
     * @return
     */
    Message updateReportDetail(ReportProcessDto reportProcessDto);

    /**
     * 获取重复案件
     *
     * @param reportRepeatCaseDto
     * @return
     */
    Message queryRepeatCase(ReportRepeatCaseDto reportRepeatCaseDto);
}
