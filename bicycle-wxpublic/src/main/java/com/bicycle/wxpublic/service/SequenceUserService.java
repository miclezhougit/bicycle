package com.bicycle.wxpublic.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleSequenceUser;
import com.bicycle.wxpublic.vo.SequenceUserLoginDto;

/**
 * @author : layne
 * @Date: 2020/8/6
 * @Time: 23:26
 * Description:
 */
public interface SequenceUserService {
    /**
     * 根据openId 获取用户信息
     *
     * @param openId
     * @return
     */
    Message queryByOpenId(String openId);

    /**
     * 用户登陆
     *
     * @param sequenceUserLoginDto
     * @return
     */
    Message<BicycleSequenceUser> processLogin(SequenceUserLoginDto sequenceUserLoginDto);

    /**
     * 根据用户主键userId获取信息
     *
     * @param userId
     * @return
     */
    Message<BicycleSequenceUser> queryByUserId(Integer userId);
}
