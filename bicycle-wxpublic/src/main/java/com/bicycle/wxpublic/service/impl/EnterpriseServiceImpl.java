package com.bicycle.wxpublic.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.service.mapper.BicycleCommonEnterpriseMapper;
import com.bicycle.wxpublic.service.EnterpriseService;
import com.bicycle.wxpublic.vo.EnterpriseListQueryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 0:15
 * Description:
 */
@Service
public class EnterpriseServiceImpl implements EnterpriseService {

    @Autowired
    private BicycleCommonEnterpriseMapper enterpriseMapper;

    @Override
    public Message<List<BicycleCommonEnterprise>> selectEnterpriseList(EnterpriseListQueryDto enterpriseListQueryDto) {
        Message message = new Message();
        QueryWrapper<BicycleCommonEnterprise> queryWrapper = new QueryWrapper<BicycleCommonEnterprise>();
        queryWrapper.setEntity(BicycleCommonEnterprise.builder().enterpriseType(enterpriseListQueryDto.getEnterpriseType()).enterpriseStatus(enterpriseListQueryDto.getEnterpriseStatus()).build());
        List<BicycleCommonEnterprise> enterpriseList = enterpriseMapper.selectList(queryWrapper);
        message.setResult(enterpriseList);
        return message;
    }

    @Override
    public BicycleCommonEnterprise getEnterprise(String enterpriseNo) {
        QueryWrapper<BicycleCommonEnterprise> queryWrapper = new QueryWrapper<BicycleCommonEnterprise>();
        queryWrapper.setEntity(BicycleCommonEnterprise.builder().enterpriseNo(enterpriseNo).build());
        return enterpriseMapper.selectOne(queryWrapper);
    }
}
