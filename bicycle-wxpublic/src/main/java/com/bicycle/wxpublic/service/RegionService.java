package com.bicycle.wxpublic.service;

import com.bicycle.service.entity.BicycleCommonRegion;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 23:04
 * Description:
 */
public interface RegionService {

    /**
     * 根据区域主键获取区域信息
     *
     * @param regionId
     * @return
     */
    BicycleCommonRegion getRegion(Integer regionId);
}
