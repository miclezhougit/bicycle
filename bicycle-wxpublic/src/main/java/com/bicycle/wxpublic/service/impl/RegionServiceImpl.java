package com.bicycle.wxpublic.service.impl;

import com.bicycle.service.entity.BicycleCommonRegion;
import com.bicycle.service.mapper.BicycleCommonRegionMapper;
import com.bicycle.wxpublic.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 23:05
 * Description:
 */
@Service
public class RegionServiceImpl implements RegionService {

    @Autowired
    private BicycleCommonRegionMapper regionMapper;


    @Override
    public BicycleCommonRegion getRegion(Integer regionId) {
        return regionMapper.selectById(regionId);
    }
}
