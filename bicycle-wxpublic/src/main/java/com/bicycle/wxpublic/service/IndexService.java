package com.bicycle.wxpublic.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.wxpublic.vo.IndexStatisVo;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 23:06
 * Description:
 */
public interface IndexService {

    /**
     * 获取首页统计数据信息
     *
     * @return
     * @param userId
     */
    Message<IndexStatisVo> statis(Integer userId);

    /**
     * 获取违停车辆位置信息
     * @return
     */
    Message illegalBicyclePosition();
}
