package com.bicycle.wxpublic.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.ReportResourceEnum;
import com.bicycle.service.config.MyBatisPlusConfig;
import com.bicycle.service.entity.BicycleBasicsBicycle;
import com.bicycle.service.entity.BicycleDataBicycleSite;
import com.bicycle.service.entity.BicycleSequenceReportDetail;
import com.bicycle.service.mapper.BicycleBasicsBicycleMapper;
import com.bicycle.service.mapper.BicycleDataBicycleSiteMapper;
import com.bicycle.service.mapper.BicycleSequenceReportDetailMapper;
import com.bicycle.wxpublic.service.IndexService;
import com.bicycle.wxpublic.vo.IllegalBicyclePositionVo;
import com.bicycle.wxpublic.vo.IndexStatisVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 23:06
 * Description:
 */
@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    private BicycleBasicsBicycleMapper bicycleMapper;

    @Autowired
    private BicycleSequenceReportDetailMapper reportDetailMapper;
    @Autowired
    private HttpServletRequest httpServletRequest;
    @Autowired
    private BicycleDataBicycleSiteMapper dataBicycleSiteMapper;

    @Override
    public Message<IndexStatisVo> statis(Integer userId) {
        Message message = new Message();
        IndexStatisVo indexStatisVo = new IndexStatisVo();
        QueryWrapper<BicycleDataBicycleSite> queryWrapper = new QueryWrapper<BicycleDataBicycleSite>();
        queryWrapper.groupBy("bicycle_no");
        MyBatisPlusConfig.dynamicTableName.set(DateUtil.format(new Date(), "yyyyMMdd"));
//        int bicycleCount = bicycleSiteService.count(queryWrapper);
        List<BicycleDataBicycleSite> bicycleSites = dataBicycleSiteMapper.selectList(queryWrapper);
        long bicycleCount = bicycleSites == null ? 0L : bicycleSites.size();

//        long totalBicycle = bicycleMapper.selectCount(new QueryWrapper<BicycleBasicsBicycle>());
        indexStatisVo.setTotalBicycle(bicycleCount);

        QueryWrapper<BicycleSequenceReportDetail> sequenceReportDetailQueryWrapper = new QueryWrapper<BicycleSequenceReportDetail>();
        sequenceReportDetailQueryWrapper.like("report_type", 1);
        long illegalParkCount = reportDetailMapper.selectCount(sequenceReportDetailQueryWrapper);

//        long totalIllegalBicycle = bicycleMapper.selectCount(new QueryWrapper<BicycleBasicsBicycle>(BicycleBasicsBicycle.builder().hasIllegalPark(1).build()));
        indexStatisVo.setTotalIllegalBicycle( illegalParkCount);
        long totalReport = reportDetailMapper.selectCount(new QueryWrapper<BicycleSequenceReportDetail>(BicycleSequenceReportDetail.builder().reportResource(ReportResourceEnum.GOV.getKey()).build()));
        indexStatisVo.setTotalReport(totalReport);

        long totalMyReport = reportDetailMapper.selectCount(new QueryWrapper<BicycleSequenceReportDetail>(BicycleSequenceReportDetail.builder().reportResource(ReportResourceEnum.GOV.getKey()).userId(userId).build()));
        indexStatisVo.setTotalMyReport(totalMyReport);
        message.setResult(indexStatisVo);
        return message;
    }

    @Override
    public Message illegalBicyclePosition() {
        Message message = new Message();
        List<IllegalBicyclePositionVo> bicyclePositionVoList = new LinkedList<IllegalBicyclePositionVo>();
        List<BicycleBasicsBicycle> bicycleBasicsBicycles = bicycleMapper.selectList(new QueryWrapper<BicycleBasicsBicycle>(BicycleBasicsBicycle.builder().hasIllegalPark(1).build()));
        bicycleBasicsBicycles.forEach(bicycleBasicsBicycle -> {
            IllegalBicyclePositionVo bicyclePositionVo = new IllegalBicyclePositionVo();
            BeanUtil.copyProperties(bicycleBasicsBicycle, bicyclePositionVo);
            bicyclePositionVoList.add(bicyclePositionVo);
        });
        message.setResult(bicyclePositionVoList);
        return message;
    }


}
