package com.bicycle.wxpublic.service;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.wxpublic.vo.EnterpriseListQueryDto;

import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 0:15
 * Description:
 */
public interface EnterpriseService {
    /**
     * 获取企业信息列表
     *
     * @param enterpriseListQueryDto
     * @return
     */
    Message<List<BicycleCommonEnterprise>> selectEnterpriseList(EnterpriseListQueryDto enterpriseListQueryDto);

    /**
     * 根据企业编号获取企业信息
     *
     * @param enterpriseNo
     * @return
     */
    BicycleCommonEnterprise getEnterprise(String enterpriseNo);
}
