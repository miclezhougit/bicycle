package com.bicycle.wxpublic.service;

import com.bicycle.wxpublic.vo.WeChatJsapiSignatureVo;
import com.bicycle.wxpublic.vo.WeChatAccessTokenVo;

/**
 * @author : layne
 * @Date: 2020/8/6
 * @Time: 21:59
 * Description: 微信配置service
 */
public interface WeChatService {

    /**
     * 获取微信jsapi签名配置信息
     *
     * @return
     */
    WeChatJsapiSignatureVo jsapiSignature();


    /**
     * 根据微信code获取accesstoken
     *
     * @param code
     * @return
     */
    WeChatAccessTokenVo getWxAccessToken(String code);
}
