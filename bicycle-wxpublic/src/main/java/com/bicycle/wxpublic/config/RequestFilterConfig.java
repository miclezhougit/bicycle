package com.bicycle.wxpublic.config;

import com.bicycle.wxpublic.interceptor.RequestFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : layne
 * @Date: 2020/8/9
 * @Time: 23:52
 * Description:
 */
@Configuration
public class RequestFilterConfig {

    @Bean
    public FilterRegistrationBean registFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new RequestFilter());
        registration.addUrlPatterns("/*");
        registration.setName("RequestFilter");
        registration.setOrder(1);
        return registration;
    }

}
