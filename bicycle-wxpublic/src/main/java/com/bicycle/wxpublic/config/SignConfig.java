package com.bicycle.wxpublic.config;

import com.bicycle.wxpublic.interceptor.SignInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/9
 * @Time: 23:25
 * Description:
 */
@Configuration
//@ConfigurationProperties(prefix = "interceptor.sign")
public class SignConfig implements WebMvcConfigurer {
    @Autowired
    private SignInterceptor signInterceptor;

    private List<String> excludePathPatterns;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration registration = registry.addInterceptor(signInterceptor);
        registration.addPathPatterns("/**");
        registration.excludePathPatterns(
                "/captch/vehicleQueryKaptcha",
                "/static/**",
                "/index.html",
                "/swagger-resources/**",
                "/webjars/**",
                "/v2/**",
                "/swagger-ui.html/**"
        );
    }
}
