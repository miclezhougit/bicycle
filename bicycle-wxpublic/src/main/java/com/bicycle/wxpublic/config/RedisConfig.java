package com.bicycle.wxpublic.config;

import com.bicycle.common.helper.RedisStringHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author : layne
 * @Date: 2020/8/11
 * @Time: 14:25
 * Description:
 */
@Configuration
public class RedisConfig {

    @Bean
    public RedisStringHelper redisStringHelper() {
        return new RedisStringHelper();
    }
}
