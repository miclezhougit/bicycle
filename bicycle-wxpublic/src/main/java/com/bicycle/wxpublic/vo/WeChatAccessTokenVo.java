package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 16:59
 * Description:
 */
@Data
public class WeChatAccessTokenVo {

    @ApiModelProperty(value = "微信accessToken")
    String accessToken;

    @ApiModelProperty(value = "微信用户openId")
    String openId;
}
