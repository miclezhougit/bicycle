package com.bicycle.wxpublic.vo;

import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/6
 * @Time: 22:05
 * Description:
 */
@Data
public class WeChatJsapiSignatureVo {

    private String accessToken;

    private Long secondTimestamp;

    private JsapiSignatureVo wxJsapiSignature;

    @Data
    public static class JsapiSignatureVo {
        private String appId;

        private String nonceStr;

        private String signature;

        private Long timestamp;

        private String url;
    }
}
