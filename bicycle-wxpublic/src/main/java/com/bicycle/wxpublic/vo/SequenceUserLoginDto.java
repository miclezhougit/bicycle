package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/6
 * @Time: 23:44
 * Description:
 */
@Data
public class SequenceUserLoginDto {

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "登录密码")
    private String password;

    @ApiModelProperty(value = "微信openid")
    private String openId;
}
