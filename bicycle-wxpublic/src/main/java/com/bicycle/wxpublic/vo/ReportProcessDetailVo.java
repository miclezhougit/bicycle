package com.bicycle.wxpublic.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.bicycle.service.entity.BicycleSequenceReportPhoto;
import com.bicycle.service.entity.BicycleSequenceReportProcess;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 10:51
 * Description:
 */
@Data
public class ReportProcessDetailVo {

    @ApiModelProperty("处理记录")
    private List<ReportProcessDetailRecordVo> processList;

    @ApiModelProperty("详情图片")
    private List<BicycleSequenceReportPhoto> detailPhotoList;

    @ApiModelProperty("举报详情")
    private ReportProcessListVo.ReportPRocessDetailListVo reportDetailList;


    @Data
    public static class ReportProcessDetailRecordVo {

        // 处理前的照片
        @ApiModelProperty(value = "处理前的照片")
        private List<BicycleSequenceReportPhoto> preDetailPhotoList = new ArrayList<BicycleSequenceReportPhoto>();

        // 处理后的照片
        @ApiModelProperty(value = "处理后的照片")
        private List<BicycleSequenceReportPhoto> afterDetailPhotoList = new ArrayList<BicycleSequenceReportPhoto>();

        @ApiModelProperty(value = "主键ID")
        private Integer id;

        @ApiModelProperty(value = "记录ID")
        private Integer detailId;

        @ApiModelProperty(value = "手机号码")
        private String mobile;

        @ApiModelProperty(value = "是否属实 0：待核实，1：不属实 2：重复案件，3：属实")
        private Integer hasTrue;

        @ApiModelProperty(value = "处理时间")
        private Date processTime;

        @ApiModelProperty(value = "处理人员主键")
        private Integer processUserId;

        @ApiModelProperty(value = "处理人员姓名")
        private String processUserName;

        @ApiModelProperty(value = "处理企业编号")
        private String processEnterpriseNo;

        @ApiModelProperty(value = "处理企业名称")
        private String processEnterpriseName;

        @ApiModelProperty(value = "处理备注")
        private String remark;

        @ApiModelProperty(value = "推送企业编号")
        private String pushEnterpriseNo;

        @ApiModelProperty(value = "推送企业名称")
        private String pushEnterpriseName;

        @ApiModelProperty(value = "操作类型 1：处理 2：推送企业 3：结案")
        private Integer operateType;

        @ApiModelProperty(value = "创建时间")
        private Date createTime;

    }


}
