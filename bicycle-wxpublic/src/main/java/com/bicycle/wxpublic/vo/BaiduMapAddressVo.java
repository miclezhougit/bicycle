package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 1:33
 * Description:
 */
@Data
public class BaiduMapAddressVo {

    @ApiModelProperty("详细地址")
    private String address;

    @ApiModelProperty("行政区主键")
    private Integer districtId;


}
