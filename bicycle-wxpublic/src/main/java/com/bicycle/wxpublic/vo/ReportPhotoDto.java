package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 22:42
 * Description:
 */
@Data
public class ReportPhotoDto {

    /**
     * 用户主键
     */
    @ApiModelProperty("用户主键")
    private Integer userId;
    /**
     * 举报类型 1：乱停乱放 2：损坏车辆 3：其他
     */
    @ApiModelProperty("举报类型 1：乱停乱放 2：损坏车辆 3：其他")
    private String[] reportType;
    /**
     * 举报内容
     */
    @ApiModelProperty("举报内容")
    private String reportContent;
    /**
     * 区域主键
     */
    @ApiModelProperty("区域主键")
    private Integer districtId;
    /**
     * 经度
     */
    @ApiModelProperty("经度")
    private String lng;
    /**
     * 纬度
     */
    @ApiModelProperty("纬度")
    private String lat;
    /**
     * 详细地址
     */
    @ApiModelProperty("详细地址")
    private String detailAddress;
    /**
     * 企业数组
     */
    @ApiModelProperty("企业数组")
    private String[] enterpriseNos;

    @ApiModelProperty(value = "accessToken",hidden = true)
    private String accessToken;

    @ApiModelProperty("举报的图片mediaId")
    private String[] reportMediaId;
    /**
     * 验证码
     */
    @ApiModelProperty("验证码")
    private String code;
    /**
     * 订单类型：1：普通订单，2：紧急订单
     */
    @ApiModelProperty("订单类型：1：普通订单，2：紧急订单")
    private Integer orderType;

}
