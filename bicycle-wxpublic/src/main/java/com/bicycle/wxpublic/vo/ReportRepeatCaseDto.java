package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 15:28
 * Description:
 */
@Data
public class ReportRepeatCaseDto {

    @ApiModelProperty(value = "重复案件编号")
    private String repeatCaseNo;

    @ApiModelProperty(value = "企业编号")
    private String enterpriseNo;

    @ApiModelProperty(value = "区域主键")
    private Integer districtId;
}
