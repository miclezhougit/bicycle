package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 23:25
 * Description:
 */
@Data
public class IllegalBicyclePositionVo {

    @ApiModelProperty(value = "企业编号")
    private String enterpriseNo;

    @ApiModelProperty(value = "车辆经度")
    private String lng;

    @ApiModelProperty(value = "车辆纬度")
    private String lat;
}
