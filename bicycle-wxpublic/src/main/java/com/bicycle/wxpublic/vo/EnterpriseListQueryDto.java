package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 0:12
 * Description:
 */
@Data
public class EnterpriseListQueryDto {

    @ApiModelProperty(value = "企业类型 1：共享单车 2：设备企业")
    private Integer enterpriseType;

    @ApiModelProperty(value = "企业状态：0：正常，1：已退出")
    private Integer enterpriseStatus;
}
