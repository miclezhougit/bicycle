package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 9:23
 * Description:
 */
@Data
public class ReportProcessListDto {

    @ApiModelProperty(value = "举报来源：1：政府巡查，2：群众举报，3：监管平台")
    private Integer reportResource;

    @ApiModelProperty(value = "状态  0：所有，2：已处理，3：已结案")
    private Integer reportStatus;

    @ApiModelProperty(value = "企业编号")
    private String enterpriseNo;

    @ApiModelProperty(value = "区域主键")
    private Integer districtId;
}
