package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 23:40
 * Description:
 */
@Data
public class ReportRecordDto {

    /**
     * 用户主键
     */
    @ApiModelProperty("用户主键")
    private Integer userId;

    // 1：所有 2：七天 3：一个月 4：三个月
    @ApiModelProperty("1：所有 2：七天 3：一个月 4：三个月")
    private int dayCriteria = 1;

}
