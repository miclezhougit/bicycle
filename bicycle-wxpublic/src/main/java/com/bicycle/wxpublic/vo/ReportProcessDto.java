package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 15:15
 * Description:
 */
@Data
public class ReportProcessDto {

    // 详情ID
    @ApiModelProperty("详情ID")
    private Integer detailId;
    // 处理前的图片
    @ApiModelProperty("处理前的图片")
    private String[] preMediaId;
    // 处理后的图片
    @ApiModelProperty("处理后的图片")
    private String[] afterMediaId;
    // 是否属实 0：待核实，1：不属实 2：重复案件，3：属实
    @ApiModelProperty("是否属实 0：待核实，1：不属实 2：重复案件，3：属实")
    private Integer hasTrue;
    // 重复案件编号
    @ApiModelProperty("重复案件编号")
    private String repeatCaseNo;
    // 处理备注
    @ApiModelProperty("处理备注")
    private String remark;
    // 处理人员ID
    @ApiModelProperty("处理人员ID")
    private Integer processUserId;
    // 处理人员名字
    @ApiModelProperty("处理人员名字")
    private String processUserName;
    //    // token
//    @ApiModelProperty("详情ID")
//    private String accessToken;
    // 处理人手机号
    @ApiModelProperty("处理人手机号")
    private String mobile;
    // 区域ID
    @ApiModelProperty("区域ID")
    private Integer districtId;
    // 企业编号
    @ApiModelProperty("企业编号")
    private String enterpriseNo;
    // 企业名称
    @ApiModelProperty("企业名称")
    private String enterpriseName;
}
