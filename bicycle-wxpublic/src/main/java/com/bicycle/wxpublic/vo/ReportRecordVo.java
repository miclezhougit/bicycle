package com.bicycle.wxpublic.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 23:43
 * Description:
 */
@Data
public class ReportRecordVo {

    /**
     * 主键ID
     */
    @ApiModelProperty("主键ID")
    private Integer reportId;
    /**
     * 举报编号
     */
    @ApiModelProperty("举报编号")
    private String reportNo;
    /**
     * 举报时间
     */
    @ApiModelProperty("举报时间")
    private Date reportTime;
    /**
     * 举报类型 1：乱停乱放 2：损坏车辆 3：其他
     */
    @ApiModelProperty("举报类型 1：乱停乱放 2：损坏车辆 3：其他")
    private String[] reportType;
    /**
     * 是否违规投放 0：否 1：是
     */
    @ApiModelProperty("是否违规投放 0：否 1：是")
    private Integer isViolation;
    /**
     * 状态 0：未处理 1：已处理
     */
    @ApiModelProperty("状态 0：未处理 1：已处理")
    private Integer reportStatus;
    /**
     * 区域主键
     */
    @ApiModelProperty("区域主键")
    private Integer districtId;
    /**
     * 区域名称
     */
    @ApiModelProperty("区域名称")
    private String districtName;
    /**
     * 用户主键
     */
    @ApiModelProperty("用户主键")
    private Integer userId;
    /**
     * 举报内容
     */
    @ApiModelProperty("举报内容")
    private String reportContent;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
     * 订单类型：1：普通订单 2：紧急订单
     */
    @ApiModelProperty("订单类型：1：普通订单 2：紧急订单")
    private Integer orderType;
}
