package com.bicycle.wxpublic.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 23:08
 * Description:
 */
@Data
public class IndexStatisVo {

    @ApiModelProperty("车辆总数")
    private Long totalBicycle;

    @ApiModelProperty("违停总数")
    private Long totalIllegalBicycle;

    @ApiModelProperty("执法总数")
    private Long totalReport;

    @ApiModelProperty("我的执法总数")
    private Long totalMyReport;
}
