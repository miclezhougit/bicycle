package com.bicycle.wxpublic.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 0:14
 * Description:
 */
@Data
public class ReportRecordDetailVo {

    @ApiModelProperty(value = "主键ID")
    private Integer reportId;

    @ApiModelProperty(value = "举报编号")
    private String reportNo;

    @ApiModelProperty(value = "举报时间")
    private Date reportTime;

    @ApiModelProperty(value = "举报类型 1：乱停乱放 2：损坏车辆 3：其他")
    private String[] reportType;

    @ApiModelProperty(value = "举报来源：1：政府巡查，2：群众举报")
    private Integer reportResource;

    @ApiModelProperty(value = "是否违规投放 0：否 1：是")
    private Integer hasViolation;

    @ApiModelProperty(value = "状态  0：未处理 1：已处理")
    private Integer reportStatus;

    @ApiModelProperty(value = "订单类型：1：普通订单 2：紧急订单")
    private Integer orderType;

    @ApiModelProperty(value = "区域主键")
    private Integer districtId;

    @ApiModelProperty(value = "区域名称")
    private String districtName;

    @ApiModelProperty(value = "用户主键")
    private Integer userId;

    @ApiModelProperty(value = "非法企业车辆数")
    private Integer illegalVehicle;

    @ApiModelProperty(value = "举报内容")
    private String reportContent;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "各企业举报详情")
    private List<ReportRecordReportDetailVo> reportDetail;

    @ApiModelProperty(value = "非法车辆编号")
    private List<String> illegal;

    @ApiModelProperty(value = "合法车辆编号")
    private List<String> legal;

    @ApiModelProperty(value = "结案时间")
    private Date closeTime;

    @ApiModelProperty(value = "详细地址")
    private String detailAddress;

    @ApiModelProperty(value = "经度")
    private String lng;

    @ApiModelProperty(value = "纬度")
    private String lat;

    @ApiModelProperty(value = "举报图片地址")
    private List<String> imageUrl;

    @ApiModelProperty(value = "处理图片地址")
    private List<String> afterImages;

    @ApiModelProperty(value = "处理备注")
    private String processRemark;


    @Data
    public static class ReportRecordReportDetailVo {
        @ApiModelProperty(value = "状态  0：未处理 1：已退回，2：已处理，3：已结案")
        private Integer reportStatus;

        @ApiModelProperty(value = "企业编号")
        private String enterpriseNo;

        @ApiModelProperty(value = "企业名称")
        private String enterpriseName;

        @ApiModelProperty(value = "非法车辆数")
        private Integer illegal;

        @ApiModelProperty(value = "合法车辆数")
        private Integer legal;

        @ApiModelProperty(value = "处理时间")
        private Date processTime;
    }
}
