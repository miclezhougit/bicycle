package com.bicycle.wxpublic.exception;

import lombok.Getter;

/**
 * @author : layne
 * @Date: 2020/8/9
 * @Time: 16:52
 * Description:
 */
public class BusinessException extends RuntimeException {

    @Getter
    private int errorCode;
    @Getter
    private String errorMsg;

    public BusinessException() {
        super();
    }

    public BusinessException(int errorCode, String errorMsg) {
        super(errorMsg);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }
}
