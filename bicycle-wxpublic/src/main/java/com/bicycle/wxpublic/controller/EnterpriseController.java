package com.bicycle.wxpublic.controller;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.service.entity.BicycleSequenceUser;
import com.bicycle.wxpublic.service.EnterpriseService;
import com.bicycle.wxpublic.vo.EnterpriseListQueryDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 0:07
 * Description:
 */
@Slf4j
@RestController
@RequestMapping("/enterprise")
@Api(tags = "企业信息相关功能")
public class EnterpriseController {

    @Autowired
    private EnterpriseService enterpriseService;


    @ApiOperation(value = "获取企业信息列表")
    @RequestMapping(value = "selectEnterpriseList", method = {RequestMethod.POST})
    public Message<List<BicycleCommonEnterprise>> selectEnterpriseList(@RequestBody EnterpriseListQueryDto enterpriseListQueryDto) {
        return enterpriseService.selectEnterpriseList(enterpriseListQueryDto);
    }
}
