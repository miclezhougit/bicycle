package com.bicycle.wxpublic.controller;

import javax.annotation.Resource;

import com.bicycle.service.entity.BicycleSequenceUser;
import com.bicycle.service.service.IBicycleSequenceUserService;
import com.bicycle.wxpublic.service.SequenceUserService;
import com.bicycle.wxpublic.vo.SequenceUserLoginDto;
import io.swagger.annotations.ApiImplicitParam;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bicycle.common.entity.base.DataGrid;
import com.bicycle.common.entity.base.Message;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 移动端用户信息表 前端控制器
 * </p>
 *
 * @author layne
 * @since 2020-08-06
 */
@Slf4j
@RestController
@RequestMapping("/user")
@Api(tags = "用户信息相关功能")
public class SequenceUserController {

    @Resource
    private SequenceUserService sequenceUserService;


    @ApiOperation(value = "获取用户信息")
    @RequestMapping(value = "getUser", method = {RequestMethod.GET})
    @ApiImplicitParam(name = "userId", value = "用户主键", required = true)
    public Message<BicycleSequenceUser> queryByOpenId(Integer userId) {
        return sequenceUserService.queryByUserId(userId);
    }

    /**
     * 用户登陆
     *
     * @param sequenceUserLoginDto
     * @return
     */
    @ApiOperation(value = "用户登陆")
    @RequestMapping(value = "processLogin", method = {RequestMethod.POST})
    public Message<BicycleSequenceUser> processLogin(@RequestBody SequenceUserLoginDto sequenceUserLoginDto) {
        return sequenceUserService.processLogin(sequenceUserLoginDto);
    }


}
