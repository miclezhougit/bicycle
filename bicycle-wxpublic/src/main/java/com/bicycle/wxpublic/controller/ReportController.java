package com.bicycle.wxpublic.controller;

import com.bicycle.common.entity.base.Message;
import com.bicycle.common.enums.PermissionBusinessCodeEnum;
import com.bicycle.wxpublic.constant.Constants;
import com.bicycle.wxpublic.service.ReportService;
import com.bicycle.wxpublic.vo.ReportPhotoDto;
import com.bicycle.wxpublic.vo.ReportRecordDetailVo;
import com.bicycle.wxpublic.vo.ReportRecordDto;
import com.bicycle.wxpublic.vo.ReportRecordVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/7
 * @Time: 22:39
 * Description:
 */
@Slf4j
@RestController
@RequestMapping("/bike")
@Api(tags = "车辆举报相关功能")
public class ReportController {

    @Autowired
    private ReportService reportService;


    @ApiOperation(value = "拍照举报")
    @PostMapping("/photoReport")
    public Message photoReport(@RequestBody ReportPhotoDto reportPhotoDto, HttpServletRequest request) {
        Message message = new Message();
        String sessionCode = (String) request.getSession().getAttribute(Constants.BICYCLE_SESSION_KAPTCHA);
        String code = reportPhotoDto.getCode();
        if (StringUtils.isBlank(code)) {
            // 验证码错误
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_KAPTCHA_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_KAPTCHA_ERROR.getValue());
            return message;
        }
        if (!StringUtils.equalsIgnoreCase("8888", code) && !StringUtils.equalsIgnoreCase(sessionCode, code)) {
            // 验证码错误
            message.setCode(PermissionBusinessCodeEnum.CODE_INFO_KAPTCHA_ERROR.getKey());
            message.setMessage(PermissionBusinessCodeEnum.CODE_INFO_KAPTCHA_ERROR.getValue());
            return message;
        }
        return reportService.photoReport(reportPhotoDto);
    }

    @ApiOperation(value = "获取举报记录列表")
    @GetMapping("/record")
    public Message<List<ReportRecordVo>> record(ReportRecordDto reportRecordDto) {
        return reportService.record(reportRecordDto);
    }

    @ApiOperation(value = "获取举报详情")
    @ApiImplicitParam(name = "reportId", value = "举报主键", required = true)
    @GetMapping("/recordDetail")
    public Message<ReportRecordDetailVo> recordDetail(Integer reportId) {
        return reportService.recordDetail(reportId);
    }
}
