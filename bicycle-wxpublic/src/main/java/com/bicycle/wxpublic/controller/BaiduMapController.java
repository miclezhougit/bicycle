
package com.bicycle.wxpublic.controller;

import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bicycle.common.entity.base.Message;
import com.bicycle.common.helper.CoordinateHelper;
import com.bicycle.service.entity.BicycleCommonRegion;
import com.bicycle.service.mapper.BicycleCommonRegionMapper;
import com.bicycle.wxpublic.vo.BaiduMapAddressVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.geom.Point2D;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(tags = "地址转换相关功能")
@RestController
@RequestMapping("/baidu")
public class BaiduMapController {

    @Autowired
    private BicycleCommonRegionMapper regionMapper;
    @Autowired
    private Environment environment;

    /**
     * @param lng 经度
     * @param lat 纬度
     * @return
     * @see
     */
    @ApiOperation("根据经纬度转换地址")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "lat", value = "纬度", required = true),
            @ApiImplicitParam(name = "lng", value = "经度", required = true)
    })
    @GetMapping("/address")
    public Message<BaiduMapAddressVo> address(String lng, String lat) {
        Message message = new Message();
        String url = MessageFormat.format(environment.getProperty("baidu.geocoder"), lat, lng);
        String address = null;
        Integer districtId = null;

        String result = HttpRequest.get(url).execute().body();
        JSONObject object = JSON.parseObject(result);
        object = JSON.parseObject(object.getString("result"));
        object = JSON.parseObject(object.getString("addressComponent"));
        address = object.getString("street") + object.getString("street_number");// + sematic_description;


        // DataCacheOprator.getInstance().setJedisHelper(redisStringHelper);
        List<BicycleCommonRegion> commonRegions = regionMapper.selectList(null);

        boolean isIn = false;

        Point2D.Double position = new Point2D.Double(Double.parseDouble(lng), Double.parseDouble(lat));
        for (BicycleCommonRegion district : commonRegions) {
            if (StringUtils.isBlank(district.getRegionPoints()) || position == null) {
                continue;
            }
            JSONArray areaArray = JSON.parseArray(district.getRegionPoints());
            if (areaArray == null || areaArray.isEmpty()) {
                continue;
            }
            List<Point2D.Double> pointList = new ArrayList<Point2D.Double>();
            for (int j = 0; j < areaArray.size(); j++) {
                double lngd = Double.valueOf(areaArray.getJSONObject(j).getString("lng"));
                double latd = Double.valueOf(areaArray.getJSONObject(j).getString("lat"));
                pointList.add(new Point2D.Double(lngd, latd));
            }
            isIn = CoordinateHelper.IsPtInPoly(position, pointList);
            if (isIn) {
                districtId = district.getId();
                break;
            }

        }
        BaiduMapAddressVo baiduMapAddressVo = new BaiduMapAddressVo();
        baiduMapAddressVo.setAddress(address);
        baiduMapAddressVo.setDistrictId(districtId);
        message.setResult(baiduMapAddressVo);
        return message;
    }
}
