package com.bicycle.wxpublic.controller;

import com.bicycle.common.entity.base.Message;
import com.bicycle.service.entity.BicycleCommonEnterprise;
import com.bicycle.wxpublic.service.IndexService;
import com.bicycle.wxpublic.vo.EnterpriseListQueryDto;
import com.bicycle.wxpublic.vo.IllegalBicyclePositionVo;
import com.bicycle.wxpublic.vo.IndexStatisVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 23:04
 * Description:
 */
@Slf4j
@Api(tags = "首页相关功能")
@RestController
@RequestMapping("/index")
public class IndexController {

    @Autowired
    private IndexService indexService;


    @ApiOperation(value = "获取统计数据信息")
    @GetMapping(value = "statis")
    @ApiImplicitParam(name = "userId", value = "用户主键", required = true)
    public Message<IndexStatisVo> statis(Integer userId) {
        return indexService.statis(userId);
    }


    @ApiOperation(value = "获取违停车辆定位")
    @GetMapping(value = "illegalBicyclePosition")
    public Message<List<IllegalBicyclePositionVo>> illegalBicyclePosition() {
        return indexService.illegalBicyclePosition();

    }
}
