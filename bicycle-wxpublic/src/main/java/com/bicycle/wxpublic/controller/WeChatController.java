package com.bicycle.wxpublic.controller;

import com.bicycle.common.entity.base.Message;
import com.bicycle.wxpublic.service.WeChatService;
import com.bicycle.wxpublic.vo.WeChatJsapiSignatureVo;
import com.bicycle.wxpublic.vo.WeChatAccessTokenVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : layne
 * @Date: 2020/8/6
 * @Time: 21:57
 * Description:
 */

@Slf4j
@RestController
@RequestMapping("/wechat")
@Api(tags = "微信公众号配置")
public class WeChatController {

    @Autowired
    private WeChatService weChatService;


    @ApiOperation(value = "获取微信公众号签名信息")
    @RequestMapping(value = "jsapiSignature", method = {RequestMethod.GET})
    public Message<WeChatJsapiSignatureVo> jsapiSignature() {
        Message message = new Message();
        WeChatJsapiSignatureVo weChatJsapiSignatureVo = weChatService.jsapiSignature();
        message.setResult(weChatJsapiSignatureVo);
        return message;
    }

    @ApiOperation(value = "根据code获取用户openid信息")
    @RequestMapping(value = "getWxAccessToken", method = {RequestMethod.GET})
    @ApiImplicitParam(name = "code", value = "微信code", required = true)
    public Message<WeChatAccessTokenVo> getWxAccessToken(String code) {
        Message message = new Message();
        WeChatAccessTokenVo weChatAccessTokenVo = weChatService.getWxAccessToken(code);
        message.setResult(weChatAccessTokenVo);
        return message;
    }


}
