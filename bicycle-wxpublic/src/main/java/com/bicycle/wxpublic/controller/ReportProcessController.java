package com.bicycle.wxpublic.controller;

import com.bicycle.common.entity.base.Message;
import com.bicycle.wxpublic.service.ReportProcessService;
import com.bicycle.wxpublic.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author : layne
 * @Date: 2020/8/8
 * @Time: 9:15
 * Description:
 */
@Slf4j
@Api(tags = "企业处理相关功能")
@RestController
@RequestMapping("/report")
public class ReportProcessController {

    @Autowired
    private ReportProcessService reportProcessService;


    /**
     * @param reportProcessListDto
     * @return
     */
    @ApiOperation("获取举报信息列表")
    @GetMapping("/getReportDetailList")
    public Message<List<ReportProcessListVo>> getReportDetailList(ReportProcessListDto reportProcessListDto) {
        Message message = new Message();
        message = reportProcessService.queryReportDetailList(reportProcessListDto);
        return message;
    }

    @ApiOperation("获取举报信息详情")
    @GetMapping("/getReportDetail")
    @ApiImplicitParam(name = "detailId", value = "举报信息主键", required = true)
    public Message<ReportProcessDetailVo> getReportDetail(Integer detailId) {
        return reportProcessService.queryWxReportDetail(detailId);
    }

    @ApiOperation("举报处理")
    @PostMapping("/processReportDetail")
    public Message processReportDetail(@RequestBody ReportProcessDto reportProcessDto) {
        return reportProcessService.updateReportDetail(reportProcessDto);
    }

    @ApiOperation("获取重复案件")
    @GetMapping("/getRepeatCase")
    public Message<ReportProcessDetailVo.ReportProcessDetailRecordVo> getRepeatCase(ReportRepeatCaseDto reportRepeatCaseDto) {
        return reportProcessService.queryRepeatCase(reportRepeatCaseDto);
    }
}
