/*
 * 文件名：RequestFilter.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月24日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.wxpublic.interceptor;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class RequestFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		ServletRequest requestWrapper = null;
		if (request instanceof HttpServletRequest) {
			if (request.getContentType() != null) {
				String contentType = request.getContentType().toLowerCase();
				if (contentType.contains("multipart/form-data")) {
					chain.doFilter(request, response);
					return;
				}
			}
			requestWrapper = new RequestWrapper((HttpServletRequest) request);
		}
		if (null == requestWrapper) {
			chain.doFilter(request, response);
		} else {
			chain.doFilter(requestWrapper, response);
		}

	}

	@Override
	public void destroy() {

	}

}
