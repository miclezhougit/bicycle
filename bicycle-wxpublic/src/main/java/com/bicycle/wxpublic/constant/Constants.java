/*
 * 文件名：Constants.java
 * 版权：Copyright by 联通系统集成有限公司
 * 描述：
 * 修改人：焦凯旋
 * 修改时间：2019年6月22日
 * 跟踪单号：
 * 修改单号：
 * 修改内容：
 */

package com.bicycle.wxpublic.constant;

public class Constants {

	/**
	 * 登录用户session
	 */
	public static String BICYCLE_SESSION_LOGIN_USER = "bicycle_session_login_user";
	/**
	 * 验证码session
	 */
	public static String BICYCLE_SESSION_KAPTCHA = "bicycle_session_kaptcha";
	/**
	 * 微信公众号api签名
	 */
	public static String BICYCLE_KEY_JSAPISIGNATURE = "bicycle_key_jsapisignature";
}
